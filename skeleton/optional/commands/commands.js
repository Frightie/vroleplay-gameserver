/**
 * @title commands.js
 * @overview Implements Package Commands
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

/**
  * @namespace
  */
const Commands = {};
export default Commands;
