/**
 * @title events.js
 * @overview Package Events File
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

export default class Events {
  /**
   * Registers all Events
   *
   */
  static register() {

  }
}
