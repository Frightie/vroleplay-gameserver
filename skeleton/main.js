/**
 * @title main.js
 * @overview Main Package File
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

// Predefined V:RP Header to install our own error types & handler
import error from '../../shared/error';
import errorHandler from '../../shared/errorHandler';
error.install(global);
process.on('uncaughtException', errorHandler);

import Package from '../../shared/classes/package';

class MyPackage extends Package {
  /**
   * Constructs the Package
   * @constructor
   */
  constructor() {
    super('myPackage');
  }

  /**
   * Starts the Package
   */
  start() {
    super.start();
  }

  /**
   * Registers all Permissions. Static Permissions are returned as string array,
   * dynamic permissions can be created using vrp.permission.create.
   */
  registerPermissions() {
    return [];
  }

  // Do not remove this. Otherwise Jan will slowly sneak up to you and rip your anus apart.
  receiveDependencies(deps) { super.receiveDependencies(global, deps); }
  receiveVRPNamespace(namespace) { super.receiveVRPNamespace(global, namespace); }
}

// Register globals
global.package = new MyPackage();
global[global.package.name] = global.package;
