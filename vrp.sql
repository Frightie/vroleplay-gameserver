/*
Navicat MySQL Data Transfer

Source Server         : vrp
Source Server Version : 50624
Source Host           : localhost:3306
Source Database       : vrp

Target Server Type    : MYSQL
Target Server Version : 50624
File Encoding         : 65001

Date: 2015-09-10 18:36:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `accounts`
-- ----------------------------
DROP TABLE IF EXISTS `accounts`;
CREATE TABLE `accounts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `active` int(11) NOT NULL DEFAULT '0',
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ipb_pass_salt` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `access_level` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `remember_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of accounts
-- ----------------------------
INSERT INTO `accounts` VALUES ('1', '1', 'Jan', '$2y$10$jmFuCGh2qWLLszutpp19HOn31S5O58zeNEAo0DBAeGZkJ0DYR20lm', 'lvZ]\'', 'tempp@gmail.com', '42', '0000-00-00 00:00:00', '2015-09-09 15:53:56', '5uLwZbmfxXhnfd3BSaQ7E8bpygtYDNTzXbvWKIhwD8eFry5LNZHBGZKdOOG4');
INSERT INTO `accounts` VALUES ('2', '1', 'Fright', '$2y$10$jmFuCGh2qWLLszutpp19HOn31S5O58zeNEAo0DBAeGZkJ0DYR20lm', 'lvZ]\'', 'tempp@gmail.com', '1337', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'nNLWVNPrmsoYE317utjb63g6i9FFPkvV5P97o3zbQ7nexw4nLl6y36eXLfRK');

-- ----------------------------
-- Table structure for `applications`
-- ----------------------------
DROP TABLE IF EXISTS `applications`;
CREATE TABLE `applications` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL DEFAULT '0',
  `account` int(11) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `age` int(11) NOT NULL,
  `question_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `question_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `question_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handler` int(11) NOT NULL,
  `reason` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notified` int(11) NOT NULL DEFAULT '0',
  `ip_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `account` (`account`),
  KEY `account_2` (`account`),
  CONSTRAINT `fk_applications_accounts_id` FOREIGN KEY (`account`) REFERENCES `accounts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of applications
-- ----------------------------
INSERT INTO `applications` VALUES ('1', '1', '1', 'Ariel_Wein', '$2y$10$sJK8pZkw9eRFBfILZC3dKuJzadafQAIYZfac3BGEXlWpIXqbmQ0v6', '18', '18', '18', '181', '0', '', '0', '127.0.0.1', '2015-03-19 15:17:30', '2015-03-19 15:17:36');
INSERT INTO `applications` VALUES ('2', '1', '1', 'Test_Test', '$2y$10$4uZ7GNDQul34ffxXBgJr..XIKFbEKFcxaqVeyx21KCktpUaSoDgie', '18', '18181818', '18181818', '18181818', '0', '', '0', '127.0.0.1', '2015-03-24 12:53:06', '2015-03-24 18:25:49');
INSERT INTO `applications` VALUES ('3', '1', '1', 'Test_Formeeting', '$2y$10$o1AGvQzJtFL1H.FNqv.diutoesidwGxKPNB2lnNrUBcHHJJ54pVRS', '18', 'fuck you mates', 'i eat', 'pillows', '0', '', '0', '127.0.0.1', '2015-03-24 18:24:30', '2015-03-24 20:54:43');
INSERT INTO `applications` VALUES ('4', '1', '1', 'Tommy_Davis', '$2y$10$6lkhHD3pNRYeLq5/LiOAcO3NRRzHxyrBbP5Lvwayi2ihzgHKnmj6O', '18', 'aasdada', '18dasda', '1dasdad', '0', '', '0', '127.0.0.1', '2015-04-09 23:10:31', '2015-04-09 23:11:28');
INSERT INTO `applications` VALUES ('5', '-1', '1', 'Tommy_Davis', '$2y$10$bXYdQV4eAZ2taN8EL/xCheLrLnumew.n9FvaoRQAJ5m1N17aFqvtG', '18', '15151', '151515', '15', '1', 'Please write some more about the background of your character.', '1', '127.0.0.1', '2015-04-09 23:14:09', '2015-04-09 23:14:40');
INSERT INTO `applications` VALUES ('6', '1', '1', 'Moshe_Barsheset', '$2y$10$MiLF/h4HWTaP24lJzV6WCeTOpuzXpNEJ42Xw3Zv0D8lYGf764OwRe', '18', 'O a, a a mgier', '1213', '12313', '0', '', '0', '::1', '2015-04-17 21:55:33', '2015-04-17 21:55:56');
INSERT INTO `applications` VALUES ('7', '0', '1', 'Test_App', '$2y$10$o4j/cTj6npKC.rz3BWLW6OOfpnWdxjQUbF7e8FPcMVlHNo9DPhxqK', '18', 'asd', 'dsa', 'asd', '0', '', '0', '::1', '2015-09-09 15:38:36', '2015-09-09 15:38:36');
INSERT INTO `applications` VALUES ('8', '0', '1', 'Test_Appp', '$2y$10$of1g3REbDgb4L8fg48oOpuQECDRoBkWb834e3bqwis5RJ/2QS4S3m', '12', 'sdfsf', 'dfsfsd', 'fsdfsf', '0', '', '0', '::1', '2015-09-09 16:06:41', '2015-09-09 16:06:41');

-- ----------------------------
-- Table structure for `articles`
-- ----------------------------
DROP TABLE IF EXISTS `articles`;
CREATE TABLE `articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author` int(11) unsigned NOT NULL,
  `tempname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(2400) COLLATE utf8_unicode_ci NOT NULL,
  `date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `author` (`author`),
  CONSTRAINT `fk_articles_accounts_id` FOREIGN KEY (`author`) REFERENCES `accounts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of articles
-- ----------------------------
INSERT INTO `articles` VALUES ('1', '2', 'Fright', 'Steam Group & Rockstar Games Social Club Crew ', 'Hello.<br/><br/>Grand Theft Auto V is out and we are all excited to enjoy this PC masterpiece.<br/> As we patiently wait for a GTA-Multiplayer initial release, why not play together?<br/><br/><h4>Why a Steam Group/Rockstar Games Social Club Crew?</h4>\r\n<ul>\r\n<li>Play online together.</li>\r\n<li>Join the V Role Play official group/crew, help spread the word and show your support.</li>\r\n<li>Receive event reminders and announcements as a notice on Steam/Rockstar Games Social Club.</li>\r\n<li>Meet other V Role Play community members.</li></ul>\r\n<h5><a href=\"http://steamcommunity.com/groups/v-rp\" target=\"_blank\">Official V Role Play Steam Group</a></h5>\r\n<h5><a href=\"http://socialclub.rockstargames.com/crew/v_role_play\" target=\"_blank\">Official V Role Play Rockstar Games Social Club Crew</a></h5>\r\n<br/>\r\nIt is important to note that this will in no way, shape, or form, slow down the development of V-RP.  This is simply here for us to interact and play with you guys before the release of V Role Play.  We will be scheduling group events for everyone to participate in on GTA Online and V-RP once released.  So invite your friends!\r\n<br/><br/>\r\n<strong>Best Regards, <br/>The V: Role Play Staff</strong>', 'Wednesday, 15/04/15, at 00:42', '2015-07-12 13:23:00', '0000-00-00 00:00:00');
INSERT INTO `articles` VALUES ('2', '2', 'Fright', 'V: Role Play and GTA-Multiplayer Progress Update', 'Hello dear members of V: Role Play.<br/>\nBoth V: Role Play and GTA-Multiplayer have progressed a lot lately.<br/>\nWe would like to present some of the major additions in this article.<br/>\n<br/>\n<h4>V: Role Play Progress</h4>\nAll of V: Role Play\'s services received many additions lately (Game Mode, User Control Panel, User Interfaces and Community Forums).<br/>\nYou can find a detailed list of some of the major additions in the official <a href=\"http://forum.v-rp.com/index.php?/topic/220-v-role-play-progress-update/#entry1514\">V: Role Play Progress forum announcement</a>.<br/>\nThe topic includes a more detailed list of features, staff changes and some statistics you might be interested in.<br/>\nAlso, it\'s needless to remark that our User Control Panel was re-design, and now is fully responsible and fits all screen sizes. We hope that you like it!\n<br/>\n<br/>\n<h4>GTA-Multiplayer Progress</h4>\nEvery weekend, an official development blog is released by the GTA-Multiplayer team.<br/>\nThe recent development blogs are amazing, showing amazing progression.<br/>\n<br/>\nRecent development blog: <a href=\"https://forum.gta-mp.net/index.php?/topic/855-development-blog-week-17/\">Development Blog Week #17</a><br/>\nFor a full list of all past development blogs, you can browse through the official <a href=\"https://forum.gta-mp.net/index.php?/forum/2-development-news/\">Development News</a> section within GTA-Multiplayer.\n<br/>\n<br/>\nBeside the good news, we would like to say a big thank you to all our dedicated members. We can\'t wait to see you all in-game, and provide you with the best services possible.\n<br/>\n<br/>\nRegards,<br/>\n<strong>The V: Role Play Management Team</strong>', 'Tuesday, 11/08/15, at 17:45', '2015-08-11 17:45:00', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `bans`
-- ----------------------------
DROP TABLE IF EXISTS `bans`;
CREATE TABLE `bans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` int(11) unsigned DEFAULT NULL,
  `ip` varchar(64) NOT NULL,
  `issuer` varchar(24) NOT NULL,
  `expiration` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `issued` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reason` varchar(256) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ban_account_id` (`account`),
  KEY `account` (`account`),
  CONSTRAINT `fk_bans_accounts_id` FOREIGN KEY (`account`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bans
-- ----------------------------

-- ----------------------------
-- Table structure for `boosts`
-- ----------------------------
DROP TABLE IF EXISTS `boosts`;
CREATE TABLE `boosts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applied_to` int(11) unsigned NOT NULL,
  `stat` varchar(32) NOT NULL,
  `remaining` int(11) NOT NULL,
  `value` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `applied_to_id` (`applied_to`),
  KEY `id` (`id`),
  CONSTRAINT `fk_boosts_characters_id` FOREIGN KEY (`applied_to`) REFERENCES `characters` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of boosts
-- ----------------------------

-- ----------------------------
-- Table structure for `businesses`
-- ----------------------------
DROP TABLE IF EXISTS `businesses`;
CREATE TABLE `businesses` (
  `id` int(11) unsigned NOT NULL,
  `owner` int(11) unsigned NOT NULL DEFAULT '0',
  `name` varchar(124) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `level` int(11) NOT NULL DEFAULT '0',
  `price` int(11) NOT NULL,
  `fee` int(11) NOT NULL DEFAULT '0',
  KEY `owner` (`owner`),
  CONSTRAINT `fk_businesses_characters_id` FOREIGN KEY (`owner`) REFERENCES `characters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of businesses
-- ----------------------------

-- ----------------------------
-- Table structure for `characters`
-- ----------------------------
DROP TABLE IF EXISTS `characters`;
CREATE TABLE `characters` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `app` int(11) NOT NULL,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `account` int(11) unsigned NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_pos` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '{"x": 0, "y": 0, "z": 0}',
  `playtime` int(11) NOT NULL,
  `duty_time` int(11) NOT NULL,
  `last_paycheck` int(11) NOT NULL,
  `experience` int(11) NOT NULL DEFAULT '0',
  `skin` int(11) NOT NULL DEFAULT '0',
  `cash` int(11) NOT NULL DEFAULT '100',
  `bank` int(11) NOT NULL DEFAULT '500',
  `phone` varchar(26) COLLATE utf8_unicode_ci NOT NULL,
  `faction` int(11) NOT NULL DEFAULT '0',
  `faction_rank` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `stats` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '{}',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_online` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `account_id` (`account`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of characters
-- ----------------------------
INSERT INTO `characters` VALUES ('1', '1', 'Jan', '1', '$2y$10$sJK8pZkw9eRFBfILZC3dKuJzadafQAIYZfac3BGEXlWpIXqbmQ0v6', '{\"x\":-67.51580047607422,\"y\":-769.1283569335938,\"z\":43.16450119018555}', '254', '0', '198', '0', '0', '100', '500', '', '1', 'Faction Leader', '{\"hunger\":90,\"stealthAbilityMaxed\":0,\"shootingAbilityMaxed\":0,\"flyingAbilityMaxed\":0,\"wheelieAbilityMaxed\":0,\"lungCapacityMaxed\":0,\"strengthMaxed\":0,\"staminaMaxed\":0,\"stealthAbility\":0,\"shootingAbility\":0,\"flyingAbility\":0,\"wheelieAbility\":0,\"lungCapacity\":0,\"strength\":0,\"stamina\":0}', '2015-03-19 15:17:36', '2015-03-19 15:17:36', '2015-09-10 16:35:18');
INSERT INTO `characters` VALUES ('2', '2', 'Ariel_Wein', '2', '$2y$10$sJK8pZkw9eRFBfILZC3dKuJzadafQAIYZfac3BGEXlWpIXqbmQ0v6', '{\"x\":1254.750244140625,\"y\":-1595.9150390625,\"z\":52.16277313232422}', '0', '0', '0', '0', '0', '100', '500', '', '0', '', '{}', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `factions`
-- ----------------------------
DROP TABLE IF EXISTS `factions`;
CREATE TABLE `factions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `acronym` varchar(12) NOT NULL,
  `leader` int(11) unsigned DEFAULT NULL,
  `db_ranks` varchar(1024) NOT NULL DEFAULT '[{"name":"Faction Leader","destroyable":false,"permissions":["IS_FACTION_LEADER", "CAN_KICK_PLAYER_FROM_FACTION", "CAN_INVITE_PLAYER_TO_FACTION"]}, {"name":"Member","destroyable":false,"permissions":[]}]',
  PRIMARY KEY (`id`),
  KEY `leader_id` (`leader`),
  CONSTRAINT `fk_factions_characters_id` FOREIGN KEY (`leader`) REFERENCES `characters` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of factions
-- ----------------------------
INSERT INTO `factions` VALUES ('1', 'San Andreas Police Department', 'SAPD', '1', '[{\"name\":\"Faction Leader\",\"destroyable\":false,\"permissions\":[\"IS_FACTION_LEADER\", \"CAN_KICK_PLAYER_FROM_FACTION\", \"CAN_INVITE_PLAYER_TO_FACTION\"]}, {\"name\":\"Member\",\"destroyable\":false,\"permissions\":[]}]');
INSERT INTO `factions` VALUES ('2', 'San Andreas Emergency Services', 'SAES', '1', '[{\"name\":\"Faction Leader\",\"destroyable\":false,\"permissions\":[\"IS_FACTION_LEADER\", \"CAN_KICK_PLAYER_FROM_FACTION\", \"CAN_INVITE_PLAYER_TO_FACTION\"]}, {\"name\":\"Member\",\"destroyable\":false,\"permissions\":[]}]');

-- ----------------------------
-- Table structure for `gs_login_attempts`
-- ----------------------------
DROP TABLE IF EXISTS `gs_login_attempts`;
CREATE TABLE `gs_login_attempts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` int(11) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `result` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `account` (`account`),
  CONSTRAINT `fk_login_attempts_accounts_id` FOREIGN KEY (`account`) REFERENCES `accounts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=807 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gs_login_attempts
-- ----------------------------
INSERT INTO `gs_login_attempts` VALUES ('782', '1', '127.0.0.1', '2015-09-07 15:17:00', 'success');
INSERT INTO `gs_login_attempts` VALUES ('783', '1', '127.0.0.1', '2015-09-08 13:14:44', 'success');
INSERT INTO `gs_login_attempts` VALUES ('784', '1', '127.0.0.1', '2015-09-08 13:18:23', 'success');
INSERT INTO `gs_login_attempts` VALUES ('785', '1', '127.0.0.1', '2015-09-08 13:24:38', 'fail');
INSERT INTO `gs_login_attempts` VALUES ('786', '1', '127.0.0.1', '2015-09-08 13:24:49', 'success');
INSERT INTO `gs_login_attempts` VALUES ('787', '1', '127.0.0.1', '2015-09-08 13:30:43', 'fail');
INSERT INTO `gs_login_attempts` VALUES ('788', '1', '127.0.0.1', '2015-09-08 13:30:49', 'success');
INSERT INTO `gs_login_attempts` VALUES ('789', '1', '127.0.0.1', '2015-09-08 13:43:29', 'success');
INSERT INTO `gs_login_attempts` VALUES ('790', '1', '127.0.0.1', '2015-09-08 15:06:29', 'success');
INSERT INTO `gs_login_attempts` VALUES ('791', '1', '127.0.0.1', '2015-09-08 15:26:25', 'success');
INSERT INTO `gs_login_attempts` VALUES ('792', '1', '127.0.0.1', '2015-09-08 15:41:30', 'success');
INSERT INTO `gs_login_attempts` VALUES ('793', '1', '127.0.0.1', '2015-09-08 15:46:01', 'success');
INSERT INTO `gs_login_attempts` VALUES ('794', '1', '127.0.0.1', '2015-09-08 15:48:00', 'success');
INSERT INTO `gs_login_attempts` VALUES ('795', '1', '127.0.0.1', '2015-09-08 15:50:49', 'success');
INSERT INTO `gs_login_attempts` VALUES ('796', '1', '127.0.0.1', '2015-09-10 16:03:14', 'success');
INSERT INTO `gs_login_attempts` VALUES ('797', '1', '127.0.0.1', '2015-09-10 16:06:38', 'success');
INSERT INTO `gs_login_attempts` VALUES ('798', '1', '127.0.0.1', '2015-09-10 16:10:01', 'success');
INSERT INTO `gs_login_attempts` VALUES ('799', '1', '127.0.0.1', '2015-09-10 16:10:54', 'success');
INSERT INTO `gs_login_attempts` VALUES ('800', '1', '127.0.0.1', '2015-09-10 16:11:50', 'success');
INSERT INTO `gs_login_attempts` VALUES ('801', '1', '127.0.0.1', '2015-09-10 16:13:13', 'success');
INSERT INTO `gs_login_attempts` VALUES ('802', '1', '127.0.0.1', '2015-09-10 16:13:37', 'success');
INSERT INTO `gs_login_attempts` VALUES ('803', '1', '127.0.0.1', '2015-09-10 16:28:15', 'success');
INSERT INTO `gs_login_attempts` VALUES ('804', '1', '127.0.0.1', '2015-09-10 16:30:34', 'success');
INSERT INTO `gs_login_attempts` VALUES ('805', '1', '127.0.0.1', '2015-09-10 16:35:15', 'success');
INSERT INTO `gs_login_attempts` VALUES ('806', '1', '127.0.0.1', '2015-09-10 16:35:36', 'success');

-- ----------------------------
-- Table structure for `houses`
-- ----------------------------
DROP TABLE IF EXISTS `houses`;
CREATE TABLE `houses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner` int(11) unsigned DEFAULT '0',
  `entrance` varchar(256) NOT NULL,
  `price` int(11) NOT NULL,
  `level` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `owner_id` (`owner`),
  CONSTRAINT `fk_houses_characters_id` FOREIGN KEY (`owner`) REFERENCES `characters` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of houses
-- ----------------------------

-- ----------------------------
-- Table structure for `items`
-- ----------------------------
DROP TABLE IF EXISTS `items`;
CREATE TABLE `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descriptor` int(10) NOT NULL,
  `storage` int(10) unsigned NOT NULL DEFAULT '0',
  `quantity` int(11) NOT NULL,
  `attributes` varchar(255) NOT NULL DEFAULT '{"dropPosition": null}',
  PRIMARY KEY (`id`),
  KEY `descriptor_id` (`descriptor`),
  KEY `storage_id` (`storage`),
  CONSTRAINT `descriptor_id` FOREIGN KEY (`descriptor`) REFERENCES `item_descriptors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `storage_id` FOREIGN KEY (`storage`) REFERENCES `storages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=187 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of items
-- ----------------------------
INSERT INTO `items` VALUES ('182', '20', '1', '1', '{\"equipped\":false,\"components\":[],\"tint\":0,\"dropPosition\":null}');
INSERT INTO `items` VALUES ('183', '42', '1', '2', '{\"bullets\":0,\"dropPosition\":null}');
INSERT INTO `items` VALUES ('184', '7', '1', '1', '{\"equipped\":true,\"components\":[],\"tint\":0,\"dropPosition\":null}');
INSERT INTO `items` VALUES ('185', '41', '1', '2', '{\"bullets\":0,\"dropPosition\":null}');
INSERT INTO `items` VALUES ('186', '44', '1', '1', '{\"dropPosition\":null}');

-- ----------------------------
-- Table structure for `item_descriptors`
-- ----------------------------
DROP TABLE IF EXISTS `item_descriptors`;
CREATE TABLE `item_descriptors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `type` varchar(24) NOT NULL,
  `weight` float(11,1) NOT NULL DEFAULT '1.0',
  `volume` float(11,6) NOT NULL DEFAULT '0.100000' COMMENT 'in Liters',
  `attributes` varchar(255) NOT NULL DEFAULT '{}',
  `other` varchar(255) NOT NULL DEFAULT '{"modifiers": []}',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of item_descriptors
-- ----------------------------
INSERT INTO `item_descriptors` VALUES ('1', 'Backpack', 'A nice backpack that holds up to 20kg of weight.', 'storage', '1500.0', '0.100000', '{}', '{\"modifiers\": [], \"capacity\": 38.0, \"weightRating\": 20000.0}');
INSERT INTO `item_descriptors` VALUES ('2', 'Teaspoon of Salt', 'The finest salt you can find.', 'item', '5.7', '0.005000', '{}', '{\"modifiers\": []}');
INSERT INTO `item_descriptors` VALUES ('3', 'Radio', 'Radio device.', 'item', '500.0', '0.100000', '{}', '{\"modifiers\": []}');
INSERT INTO `item_descriptors` VALUES ('4', 'Energy Drink', 'The ingredients are a well kept secrets. Some say that it increases your stamina for a short amount of time.', 'consumable', '320.5', '0.100000', '{}', '{\"modifiers\": [{\"stat\": \"stamina\", \"data\": {\"type\": \"value\", \"value\": 60}, \"boost\": 180}]}');
INSERT INTO `item_descriptors` VALUES ('5', '+STA', 'Increases your stamina (Cheat Item).', 'consumable', '1.0', '0.100000', '{}', '{\"modifiers\": [{\"stat\": \"stamina\", \"data\": {\"type\": \"value\", \"value\": 60}}]}');
INSERT INTO `item_descriptors` VALUES ('6', 'Placebo', 'Food Placebo. Holds 10 seconds and is only supposed to be used for testing.', 'consumable', '1.0', '0.100000', '{}', '{\"modifiers\": [{\"stat\": \"hunger\", \"data\": {\"type\": \"value\", \"value\": 30}, \"boost\": 10}]}');
INSERT INTO `item_descriptors` VALUES ('7', 'Combat Pistol', 'Combat Pistol. Uses 9mm ammo.', 'weapon', '600.0', '0.100000', '{\"equipped\": false, \"components\": [], \"tint\": 0}', '{\"maxStackSize\": 1, \"identifier\": \"WEAPON_COMBATPISTOL\", \"magazine\": 41, \"modifiers\": []}');
INSERT INTO `item_descriptors` VALUES ('8', 'Pistol', 'Pistol. Uses 9mm ammo.', 'weapon', '900.0', '0.100000', '{\"equipped\": false, \"components\": [], \"tint\": 0}', '{\"maxStackSize\": 1, \"identifier\": \"WEAPON_PISTOL\", \"magazine\": 41, \"modifiers\": []}');
INSERT INTO `item_descriptors` VALUES ('9', 'AP Pistol', 'AP Pistol. Uses 9mm ammo.', 'weapon', '1000.0', '0.100000', '{\"equipped\": false, \"components\": [], \"tint\": 0}', '{\"maxStackSize\": 1, \"identifier\": \"WEAPON_APPISTOL\", \"magazine\": 41, \"modifiers\": []}');
INSERT INTO `item_descriptors` VALUES ('10', 'Taser', 'Taser. Used by law enforcement agencies to stun.', 'weapon', '800.0', '0.100000', '{\"equipped\": false, \"components\": [], \"tint\": 0}', '{\"maxStackSize\": 1, \"identifier\": \"WEAPON_STUNGUN\", \"noAmmo\": true, \"modifiers\": []}');
INSERT INTO `item_descriptors` VALUES ('11', 'Desert Eagle', 'Desert Eagle. Uses .50 ammo.', 'weapon', '1700.0', '0.100000', '{\"equipped\": false, \"components\": [], \"tint\": 0}', '{\"maxStackSize\": 1, \"identifier\": \"WEAPON_PISTOL50\", \"magazine\": 50, \"modifiers\": []}');
INSERT INTO `item_descriptors` VALUES ('12', 'SNS Pistol', 'SNS Pistol. Uses 9mm ammo.', 'weapon', '1000.0', '0.100000', '{\"equipped\": false, \"components\": [], \"tint\": 0}', '{\"maxStackSize\": 1, \"identifier\": \"WEAPON_SNSPISTOL\", \"magazine\": 41, \"modifiers\": []}');
INSERT INTO `item_descriptors` VALUES ('13', 'Heavy Pistol', 'Heavy Pistol. Uses .45 ACP ammo.', 'weapon', '1200.0', '0.100000', '{\"equipped\": false, \"components\": [], \"tint\": 0}', '{\"maxStackSize\": 1, \"identifier\": \"WEAPON_HEAVYPISTOL\", \"magazine\": 51,  \"modifiers\": []}');
INSERT INTO `item_descriptors` VALUES ('14', 'Vintage Pistol', 'Vintage Pistol. Uses 9mm ammo.', 'weapon', '600.0', '0.100000', '{\"equipped\": false, \"components\": [], \"tint\": 0}', '{\"maxStackSize\": 1, \"identifier\": \"WEAPON_VINTAGEPISTOL\", \"magazine\": 41, \"modifiers\": []}');
INSERT INTO `item_descriptors` VALUES ('15', 'Hand Flare', 'Hand Flare.', 'weapon', '800.0', '0.100000', '{\"equipped\": false, \"components\": [], \"tint\": 0}', '{\"identifier\": \"WEAPON_FLARE\", \"throwable\": true, \"modifiers\": [], \"noAmmo\": true}');
INSERT INTO `item_descriptors` VALUES ('16', 'Micro SMG', 'Micro SMG. Uses 9x19mm ammo.', 'weapon', '1700.0', '0.100000', '{\"equipped\": false, \"components\": [], \"tint\": 0}', '{\"maxStackSize\": 1, \"identifier\": \"WEAPON_MICROSMG\", \"magazine\": 41, \"modifiers\": []}');
INSERT INTO `item_descriptors` VALUES ('17', 'MP5', 'MP5. Uses 9x19mm ammo.', 'weapon', '2600.0', '0.100000', '{\"equipped\": false, \"components\": [], \"tint\": 0}', '{\"maxStackSize\": 1, \"identifier\": \"WEAPON_SMG\", \"magazine\": 41, \"modifiers\": []}');
INSERT INTO `item_descriptors` VALUES ('18', 'Assault SMG', 'Assault SMG. Uses 5.56mm ammo.', 'weapon', '2100.0', '0.100000', '{\"equipped\": false, \"components\": [], \"tint\": 0}', '{\"maxStackSize\": 1, \"identifier\": \"WEAPON_ASSAULTSMG\", \"magazine\": 42, \"modifiers\": []}');
INSERT INTO `item_descriptors` VALUES ('19', 'Combat PDW', 'Combat PDW. Uses 9mm ammo.', 'weapon', '2100.0', '0.100000', '{\"equipped\": false, \"components\": [], \"tint\": 0}', '{\"maxStackSize\": 1, \"identifier\": \"WEAPON_COMBATMG\", \"magazine\": 41, \"modifiers\": []}');
INSERT INTO `item_descriptors` VALUES ('20', 'Assault Rifle', 'Assault Rifle. Uses 5.56mm ammo.', 'weapon', '4000.0', '0.100000', '{\"equipped\": false, \"components\": [], \"tint\": 0}', '{\"maxStackSize\": 1, \"identifier\": \"WEAPON_ASSAULTRIFLE\", \"magazine\": 42, \"modifiers\": []}');
INSERT INTO `item_descriptors` VALUES ('21', 'M4A1 Carbine', 'M4A1 Carbine. Uses 5.56mm ammo.', 'weapon', '5000.0', '0.100000', '{\"equipped\": false, \"components\": [], \"tint\": 0}', '{\"maxStackSize\": 1, \"identifier\": \"WEAPON_CARBINERIFLE\", \"magazine\": 42, \"modifiers\": []}');
INSERT INTO `item_descriptors` VALUES ('22', 'TAR-21 Rifle', 'TAR-21 Rifle. Uses 5.56mm ammo.', 'weapon', '3500.0', '0.100000', '{\"equipped\": false, \"components\": [], \"tint\": 0}', '{\"maxStackSize\": 1, \"identifier\": \"WEAPON_ADVANCEDRIFLE\", \"magazine\": 42, \"modifiers\": []}');
INSERT INTO `item_descriptors` VALUES ('23', 'G36C Rifle', 'G36C Rifle. Uses 5.56mm ammo.', 'weapon', '3600.0', '0.100000', '{\"equipped\": false, \"components\": [], \"tint\": 0}', '{\"maxStackSize\": 1, \"identifier\": \"WEAPON_SPECIALCARBINE\", \"magazine\": 42, \"modifiers\": []}');
INSERT INTO `item_descriptors` VALUES ('24', 'Bullpup Rifle', 'Bullpup Rifle. Uses 5.56mm ammo.', 'weapon', '3000.0', '0.100000', '{\"equipped\": false, \"components\": [], \"tint\": 0}', '{\"maxStackSize\": 1, \"identifier\": \"WEAPON_BULLPUPRIFLE\", \"magazine\": 42, \"modifiers\": []}');
INSERT INTO `item_descriptors` VALUES ('25', 'Sniper Rifle', 'Sniper Rifle. Uses 7.62mm ammo.', 'weapon', '6500.0', '0.100000', '{\"equipped\": false, \"components\": [], \"tint\": 0}', '{\"maxStackSize\": 1, \"identifier\": \"WEAPON_SNIPERRIFLE\", \"magazine\": 52, \"modifiers\": []}');
INSERT INTO `item_descriptors` VALUES ('26', 'Heavy Sniper', 'Heavy Sniper. Uses 12.7x99mm ammo.', 'weapon', '13500.0', '0.100000', '{\"equipped\": false, \"components\": [], \"tint\": 0}', '{\"maxStackSize\": 1, \"identifier\": \"WEAPON_HEAVYSNIPER\", \"magazine\": 49, \"modifiers\": []}');
INSERT INTO `item_descriptors` VALUES ('27', 'RPG Launcher', 'RPG. Uses 40mm ammo.', 'weapon', '7000.0', '0.100000', '{\"equipped\": false, \"components\": [], \"tint\": 0}', '{\"maxStackSize\": 1, \"identifier\": \"WEAPON_RPG\", \"launcher\": true, \"magazine\": 45, \"modifiers\": []}');
INSERT INTO `item_descriptors` VALUES ('28', 'Grenade', 'Hand grenade.', 'weapon', '400.0', '0.100000', '{\"equipped\": false, \"components\": [], \"tint\": 0}', '{\"identifier\": \"WEAPON_GRENADE\", \"throwable\": true, \"modifiers\": [], \"noAmmo\": true}');
INSERT INTO `item_descriptors` VALUES ('29', 'Tear Gas', 'Tear gas.', 'weapon', '300.0', '0.100000', '{\"equipped\": false, \"components\": [], \"tint\": 0}', '{\"identifier\": \"WEAPON_SMOKEGRENADE\", \"throwable\": true, \"modifiers\": [], \"noAmmo\": true}');
INSERT INTO `item_descriptors` VALUES ('30', 'Sticky Bomb', 'Sticky Bomb.', 'weapon', '500.0', '0.100000', '{\"equipped\": false, \"components\": [], \"tint\": 0}', '{\"identifier\": \"WEAPON_STICKYBOMB\", \"throwable\": true, \"modifiers\": [], \"noAmmo\": true}');
INSERT INTO `item_descriptors` VALUES ('31', 'Molotov', 'Molotov.', 'weapon', '400.0', '0.100000', '{\"equipped\": false, \"components\": [], \"tint\": 0}', '{\"identifier\": \"WEAPON_MOLOTOV\", \"throwable\": true, \"modifiers\": [], \"noAmmo\": true}');
INSERT INTO `item_descriptors` VALUES ('32', 'Baseball Bat', 'Baseball Bat.', 'weapon', '1000.0', '0.100000', '{\"equipped\": false, \"components\": [], \"tint\": 0}', '{\"maxStackSize\": 1, \"identifier\": \"WEAPON_BAT\", \"melee\": true, \"modifiers\": [], \"noAmmo\": true}');
INSERT INTO `item_descriptors` VALUES ('33', 'Knife', 'Knife.', 'weapon', '400.0', '0.100000', '{\"equipped\": false, \"components\": [], \"tint\": 0}', '{\"maxStackSize\": 1, \"identifier\": \"WEAPON_KNIFE\", \"melee\": true, \"modifiers\": [], \"noAmmo\": true}');
INSERT INTO `item_descriptors` VALUES ('34', 'Nightstick', 'Nightstick', 'weapon', '1000.0', '0.100000', '{\"equipped\": false, \"components\": [], \"tint\": 0}', '{\"maxStackSize\": 1, \"identifier\": \"WEAPON_NIGHTSTICK\", \"melee\": true, \"modifiers\": [], \"noAmmo\": true}');
INSERT INTO `item_descriptors` VALUES ('35', 'Crowbar', 'Crowbar.', 'weapon', '2000.0', '0.100000', '{\"equipped\": false, \"components\": [], \"tint\": 0}', '{\"maxStackSize\": 1, \"identifier\": \"WEAPON_CROWBAR\", \"melee\": true, \"modifiers\": [], \"noAmmo\": true}');
INSERT INTO `item_descriptors` VALUES ('36', 'Golf Club', 'Golf Club', 'weapon', '750.0', '0.100000', '{\"equipped\": false, \"components\": [], \"tint\": 0}', '{\"maxStackSize\": 1, \"identifier\": \"WEAPON_GOLFCLUB\", \"melee\": true, \"modifiers\": [], \"noAmmo\": true}');
INSERT INTO `item_descriptors` VALUES ('37', 'Hammer', 'Hammer.', 'weapon', '1500.0', '0.100000', '{\"equipped\": false, \"components\": [], \"tint\": 0}', '{\"maxStackSize\": 1, \"identifier\": \"WEAPON_HAMMER\", \"melee\": true, \"modifiers\": [], \"noAmmo\": true}');
INSERT INTO `item_descriptors` VALUES ('38', 'Broken Bottle', 'Broken Bottle.', 'weapon', '250.0', '0.100000', '{\"equipped\": false, \"components\": [], \"tint\": 0}', '{\"maxStackSize\": 1, \"identifier\": \"WEAPON_BOTTLE\", \"melee\": true, \"modifiers\": [], \"noAmmo\": true}');
INSERT INTO `item_descriptors` VALUES ('39', 'Cavalry Dagger', 'Cavalry Dagger.', 'weapon', '400.0', '0.100000', '{\"equipped\": false, \"components\": [], \"tint\": 0}', '{\"maxStackSize\": 1, \"identifier\": \"WEAPON_DAGGER\", \"melee\": true, \"modifiers\": [], \"noAmmo\": true}');
INSERT INTO `item_descriptors` VALUES ('40', 'Hatchet', 'Hatchet.', 'weapon', '3000.0', '0.100000', '{\"equipped\": false, \"components\": [], \"tint\": 0}', '{\"maxStackSize\": 1, \"identifier\": \"WEAPON_HATCHET\", \"melee\": true, \"modifiers\": [], \"noAmmo\": true}');
INSERT INTO `item_descriptors` VALUES ('41', '9mm Magazine', 'Magazine of 9x19mm bullets.', 'item', '250.0', '0.100000', '{\"bullets\": 12}', '{\"magazine\": true, \"bulletWeight\": 7.45, \"modifiers\": []}');
INSERT INTO `item_descriptors` VALUES ('42', '5.56mm Magazine', 'Magazine of 5.56x45mm bullets.', 'item', '250.0', '0.100000', '{\"bullets\": 30}', '{\"magazine\": true, \"bulletWeight\": 6.0, \"modifiers\": []}');
INSERT INTO `item_descriptors` VALUES ('43', 'Global Storage', 'If you own this as a player, you shouldnt.', 'storage', '0.0', '0.100000', '{}', '{\"modifiers\": [], \"capacity\": 2000000.0, \"weightRating\": -1.0}');
INSERT INTO `item_descriptors` VALUES ('44', 'Sprunk', 'A delicious sprunk can.', 'consumable', '320.5', '0.282000', '{}', '{\"modifiers\": [{\"stat\": \"thirst\", \"data\": {\"type\": \"value\", \"value\": 10}}, {\"stat\": \"hunger\", \"data\": {\"type\": \"value\", \"value\": 5}}]}');
INSERT INTO `item_descriptors` VALUES ('45', 'RPG Rocket', 'RPG Rocket', 'item', '0.0', '0.100000', '{\"bullets\": 1}', '{\"magazine\": true, \"bulletWeight\": 1500.0, \"deleteEmpty\": true, \"modifiers\": []}');
INSERT INTO `item_descriptors` VALUES ('46', 'Flashlight', 'Flashlight for Pistol, Combat Pistol, AP Pistol, Heavy Pistol, .50 Pistol and Micro SMG.', 'item', '300.0', '0.100000', '{}', '{\"component\": true, \"hashes\": [{\"weapons\": [7, 8, 9, 11, 13, 16], \"hash\": 899381934}, {\"weapons\": [17, 18, 19,  20, 21, 22, 23, 24, 25, 26], \"hash\": 2076495324}], \"modifiers\": []}');
INSERT INTO `item_descriptors` VALUES ('47', 'Weapon Tint', 'Changes the color of your weapon', 'item', '1.0', '0.100000', '{\"tintIndex\": 1}', '{\"modifiers\": []}');
INSERT INTO `item_descriptors` VALUES ('49', '12.7mm Magazine', 'Magazine of 12.7x99mm bullets.', 'item', '250.0', '0.100000', '{\"bullets\": 10}', '{\"magazine\": true, \"bulletWeight\": 45, \"modifiers\": []}');
INSERT INTO `item_descriptors` VALUES ('50', '.50 Magazine', 'Magazine of .50 (12.7x33mm) bullets.', 'item', '250.0', '0.100000', '{\"bullets\": 9}', '{\"magazine\": true, \"bulletWeight\": 19, \"modifiers\": []}');
INSERT INTO `item_descriptors` VALUES ('51', '.45 ACP Magazine', 'Magazine of 11.43×23mm bullets.', 'item', '250.0', '0.100000', '{\"bullets\": 13}', '{\"magazine\": true, \"bulletWeight\": 12, \"modifiers\": []}');
INSERT INTO `item_descriptors` VALUES ('52', '7.62mm Magazine', 'Magazine of 7.62x51mm bullets.', 'item', '250.0', '0.100000', '{\"bullets\": 16}', '{\"magazine\": true, \"bulletWeight\": 10, \"modifiers\": []}');
INSERT INTO `item_descriptors` VALUES ('53', 'Flare', 'Flare. Used by Flare Gun.', 'item', '0.0', '0.100000', '{\"bullets\": 1}', '{\"magazine\": true, \"bulletWeight\": 100, \"deleteEmpty\": true, \"modifiers\": []}');
INSERT INTO `item_descriptors` VALUES ('54', 'Car Key', 'A key for a vehicle.', 'item', '60.0', '0.030000', '{\"vehicle\": 0, \"vehicleDescriptor\": 0}', '{\"modifiers\": []}');

-- ----------------------------
-- Table structure for `migrations`
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('2014_10_27_053727_create_accounts_table', '1');
INSERT INTO `migrations` VALUES ('2014_11_03_163756_create_news_table', '2');
INSERT INTO `migrations` VALUES ('2014_12_10_003530_create_characters_table', '2');
INSERT INTO `migrations` VALUES ('2015_03_19_163333_create_applications_table', '2');

-- ----------------------------
-- Table structure for `reports`
-- ----------------------------
DROP TABLE IF EXISTS `reports`;
CREATE TABLE `reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` int(11) NOT NULL,
  `reporter` int(11) NOT NULL,
  `text` varchar(512) NOT NULL,
  `handler` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of reports
-- ----------------------------

-- ----------------------------
-- Table structure for `slack_auth`
-- ----------------------------
DROP TABLE IF EXISTS `slack_auth`;
CREATE TABLE `slack_auth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slack_name` varchar(64) NOT NULL,
  `account` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `slack_account_id` (`account`),
  CONSTRAINT `slack_account_id` FOREIGN KEY (`account`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of slack_auth
-- ----------------------------
INSERT INTO `slack_auth` VALUES ('1', 'jan', '1');

-- ----------------------------
-- Table structure for `slides`
-- ----------------------------
DROP TABLE IF EXISTS `slides`;
CREATE TABLE `slides` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order` int(11) NOT NULL,
  `title` varchar(124) NOT NULL,
  `desc` varchar(256) NOT NULL,
  `img_src` varchar(256) DEFAULT NULL,
  `url` varchar(256) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of slides
-- ----------------------------
INSERT INTO `slides` VALUES ('1', '2', 'Steam Group & Rockstar Social Club Crew ', 'Grand Theft Auto V is out and playable.<br/> We have set up several social services (Rockstar Games Social Club Crew, Steam Group and more) that you might want to check out!', 'steam_and_socialclub.jpg', '/articles/read/1', '2015-08-11 20:40:05', '0000-00-00 00:00:00');
INSERT INTO `slides` VALUES ('2', '3', 'Official TeamSpeak 3 Server Release', 'Test', 'ts3_release.jpg', 'http://forum.v-rp.com/index.php?/topic/30-teamspeak-release-and-information/', '2015-08-11 18:03:37', '0000-00-00 00:00:00');
INSERT INTO `slides` VALUES ('3', '1', 'V: Role Play and GTA-Multiplayer Progress Update', 'Both V: Role Play and GTA-Multiplayer have progressed a lot lately. We have published an article summing up the latest additions to the progress.', 'progress.jpg', '/articles/read/2', '2015-08-11 20:41:00', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `storages`
-- ----------------------------
DROP TABLE IF EXISTS `storages`;
CREATE TABLE `storages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descriptor` int(11) NOT NULL,
  `owner` int(11) unsigned NOT NULL,
  `owner_type` varchar(1) NOT NULL DEFAULT 'c',
  `attributes` varchar(255) NOT NULL DEFAULT '{"dropPosition": null}',
  PRIMARY KEY (`id`),
  KEY `owner` (`owner`),
  CONSTRAINT `fk_storages_characters_id` FOREIGN KEY (`owner`) REFERENCES `characters` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of storages
-- ----------------------------
INSERT INTO `storages` VALUES ('0', '43', '1', 'i', '{\"dropPosition\": null}');
INSERT INTO `storages` VALUES ('1', '1', '1', 'c', '{\"dropPosition\": null}');

-- ----------------------------
-- Table structure for `tokens`
-- ----------------------------
DROP TABLE IF EXISTS `tokens`;
CREATE TABLE `tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `hash` varchar(32) NOT NULL,
  `type` varchar(32) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tokens
-- ----------------------------

-- ----------------------------
-- Table structure for `vehicles`
-- ----------------------------
DROP TABLE IF EXISTS `vehicles`;
CREATE TABLE `vehicles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `owner` int(11) unsigned NOT NULL,
  `owner_type` char(1) NOT NULL DEFAULT 'c',
  `model_name` varchar(32) NOT NULL,
  `primary_color` varchar(64) NOT NULL DEFAULT '{"r": 0, "g": 0, "b": 0, "a": 255}',
  `secondary_color` varchar(64) NOT NULL DEFAULT '{"r": 0, "g": 0, "b": 0, "a": 255}',
  `position` varchar(128) NOT NULL DEFAULT '{"x": 0, "y": 0, "z": 0}',
  `rotation` varchar(128) NOT NULL DEFAULT '{"x": 0, "y": 0, "z": 0}',
  `customization` varchar(256) NOT NULL DEFAULT '[]',
  `destroyed` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `vehicle_owner_id` (`owner`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of vehicles
-- ----------------------------
INSERT INTO `vehicles` VALUES ('2', '1', 'c', 'police', '{\"r\": 255, \"g\": 255, \"b\": 255, \"a\": 255}', '{\"r\": 0, \"g\": 255, \"b\": 0, \"a\": 255}', '{\"x\":-67.51580047607422,\"y\":-769.1283569335938,\"z\":43.16450119018555}', '{\"x\": 0, \"y\": 0, \"z\": 0}', '[]', '0');
INSERT INTO `vehicles` VALUES ('3', '1', 'f', 'police', '{\"r\": 0, \"g\": 0, \"b\": 0, \"a\": 255}', '{\"r\": 0, \"g\": 0, \"b\": 0, \"a\": 255}', '{\"x\": 0, \"y\": 0, \"z\": 0}', '{\"x\": 0, \"y\": 0, \"z\": 0}', '[]', '0');
INSERT INTO `vehicles` VALUES ('7', '0', 's', 'police', '{\"r\": 0, \"g\": 0, \"b\": 0, \"a\": 255}', '{\"r\": 0, \"g\": 0, \"b\": 0, \"a\": 255}', '{\"x\": 0, \"y\": 0, \"z\": 0}', '{\"x\": 0, \"y\": 0, \"z\": 0}', '[]', '0');

-- ----------------------------
-- Table structure for `vehicles_list`
-- ----------------------------
DROP TABLE IF EXISTS `vehicles_list`;
CREATE TABLE `vehicles_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_id` int(11) NOT NULL DEFAULT '0',
  `model_name` varchar(18) NOT NULL,
  `manufacturer` varchar(18) NOT NULL,
  `price` int(11) NOT NULL,
  `category` varchar(18) NOT NULL DEFAULT 'others',
  `performance` varchar(1024) NOT NULL,
  `reallife` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of vehicles_list
-- ----------------------------
INSERT INTO `vehicles_list` VALUES ('1', '0', '9F', 'Obey', '141000', 'sports', '{\n  \"acceleration\": \"4.6\",\n  \"topspeed\": \"250\",\n  \"engine\": \"M4 V10\",\n  \"drivetrain\": \"AWD\",\n  \"gearbox\": \"6\"\n}', 'Audi R8');
INSERT INTO `vehicles_list` VALUES ('2', '0', '9F Cabrio', 'Obey', '151000', 'sports', '{\n  \"acceleration\": \"4.6\",\n  \"topspeed\": \"250\",\n  \"engine\": \"M4 V10\",\n  \"drivetrain\": \"AWD\",\n  \"gearbox\": \"6\"\n}', 'Audi R8 Spyder');
INSERT INTO `vehicles_list` VALUES ('3', '0', 'Furore GT', 'Lampadati', '221000', 'sports', '{\n  \"acceleration\": \"5.0\",\n  \"topspeed\": \"250\",\n  \"engine\": \"FR Supercharged\",\n  \"drivetrain\": \"RWD\",\n  \"gearbox\": \"6\",\n  \"mass\": \"none\"\n}', 'Maserati Alfieri');
INSERT INTO `vehicles_list` VALUES ('4', '0', 'Massacro', 'Dewbauchee', '301000', 'sports', '{\n  \"acceleration\": \"3.2\"\n  \"topspeed\": \"310\",\n  \"engine\": \"FR V6\",\n  \"drivetrain\": \"RWD\",\n  \"gearbox\": \"6\",\n  \"mass\": \"1700\"\n}\n', 'Ferrari F12 Berlinetta, Aston Martin Vanquish');
INSERT INTO `vehicles_list` VALUES ('5', '0', 'Alpha', 'Albany', '155000', 'sports', '{\r\n  \"acceleration\": \"5.9\",\r\n  \"topspeed\": \"252\",\r\n  \"engine\": \"FR V8\",\r\n  \"drivetrain\": \"RWD\",\r\n  \"gearbox\": \"6\",\r\n  \"mass\": \"1700\"\r\n}', 'Cadillac Elmiraj');
INSERT INTO `vehicles_list` VALUES ('6', '0', 'Banshee', 'Bravado', '110000', 'sports', '{\r\n  \"acceleration\": \"3.7\",\r\n  \"topspeed\": \"240\",\r\n  \"engine\": \"FR Twin-Turbo V8\",\r\n  \"drivetrain\": \"RWD\",\r\n  \"gearbox\": \"5\",\r\n  \"mass\": \"1200\"\r\n}', 'Dodge Viper SR');
INSERT INTO `vehicles_list` VALUES ('7', '0', 'Blista Compact', 'Dinka', '12000', 'sports', '{\r\n  \"acceleration\": \"7.4\",\r\n  \"topspeed\": \"241\",\r\n  \"engine\": \"FF Twin-Cam Inline-4\",\r\n  \"drivetrain\": \"FWD\",\r\n  \"gearbox\": \"5\",\r\n  \"mass\": \"1400\"\r\n}', 'Honda CR-X');
INSERT INTO `vehicles_list` VALUES ('8', '0', 'Buffalo', 'Bravado', '58000', 'sports', '{\r\n  \"acceleration\": \"6.9\",\r\n  \"topspeed\": \"233\",\r\n  \"engine\": \"FR Supercharged V8\",\r\n  \"drivetrain\": \"RWD\",\r\n  \"gearbox\": \"5\",\r\n  \"mass\": \"1650\"\r\n}', 'Dodge Charger');
INSERT INTO `vehicles_list` VALUES ('9', '0', 'Buffalo S', 'Bravado', '68000', 'sports', '{\r\n  \"acceleration\": \"6.9\",\r\n  \"topspeed\": \"233\",\r\n  \"engine\": \"FR Supercharged V8\",\r\n  \"drivetrain\": \"RWD\",\r\n  \"gearbox\": \"5\",\r\n  \"mass\": \"1650\"\r\n}', 'Dodge Charger SRT 8');
INSERT INTO `vehicles_list` VALUES ('10', '0', 'Carbonizzare', 'Grotti', '341000', 'sports', '{\r\n  \"acceleration\": \"3.5\",\r\n  \"topspeed\": \"318\",\r\n  \"engine\": \"FR Naturally Aspirated V8\",\r\n  \"drivetrain\": \"RWD\",\r\n  \"gearbox\": \"6\",\r\n  \"mass\": \"1600\"\r\n}', 'Aston Martin Zagato, Ferrari F12');
INSERT INTO `vehicles_list` VALUES ('11', '0', 'Comet', 'Pfister', '101000', 'sports', '{\n  \"acceleration\": \"3.8\",\n  \"topspeed\": \"326\",\n  \"engine\": \"R4 Flat 6\",\n  \"drivetrain\": \"RWD\",\n  \"gearbox\": \"5\",\n  \"mass\": \"1400\"\n}', 'Porsche 911');
INSERT INTO `vehicles_list` VALUES ('12', '0', 'Coquette', 'Invetero', '138000', 'sports', '{\r\n  \"acceleration\": \"3.9\",\r\n  \"topspeed\": \"325\",\r\n  \"engine\": \"FR V8\",\r\n  \"drivetrain\": \"RWD\",\r\n  \"gearbox\": \"6\",\r\n  \"mass\": \"1400\"\r\n}', 'Chevrolet Corvette C5');
