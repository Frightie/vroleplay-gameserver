/**
 * @title logger.js
 * @overview Logging Class (implements multiple functions)
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

require('colors');

import LogFile from './classes/log';

/**
 * Log Class for V:RP
 */
export default class Logger {
  /**
   * Constructs a new Logger.
   * @constructor
   * @param {string} basePath the base path to the log files.
   */
  constructor(basePath) {
    /** @type {string} */
    this.basePath = basePath;

    /** @type {Array<string>} */
    this.functions = ['debug', 'error', 'fatal', 'implement', 'info', 'log', 'print', 'success', 'warning'];

    /** @type {LogFile} */
    this.logFile = new LogFile(this.getLogName(), false);

    /** @type {number} */
    this.currentLogLevel = require('../config/config.js').environment.logLevel;

    events.Add('config logLevel update', level => {
      this.currentLogLevel = level;
    });

    setInterval(() => {
      // Check if the log file has to be changed
      if (this.getLogName() !== this.logFile.path) {
        this.logFile = new LogFile(this.getLogName(), false);
        this.print('Log File switched.');
      }
    }, 5000);
  }

  /**
   * Prints a debug message
   *
   * @param {*} data the data to log
   * @param {int} [level=1] optional log level. corresponds to config.debugLevel. The higher it is, the more verbose the output will be
   */
  debug(data, level = 1) {
    if (level <= this.currentLogLevel) {
      console.log(`[${'DEBUG'.magenta}${(level > 1 ? ' ('.magenta + level + ')'.magenta : '')}] ${data}`);
    }
  }

  /**
   * Prints (and logs) an error to the console.
   *
   * @param {*} data The data to log
   * @param {boolean} [slack = true] send the message to slack
   */
  error(data, slack = true) {
    this.logFile.write(`[ERROR] ${data}`);
    console.log(`[${'ERROR'.red}] ${data}`);

    if (slack) {
      shared.integration.slack.sendError(data);
    }
  }

  /**
   * Prints (and logs) an fatal error to the console. Then it throws a message.
   *
   * @param {*} data The data to throw/print
   */
  fatal(data) {
    console.log(`[${'FATAL'.red}] ${data}`);
    throw new Error(data);
  }

  /**
   * Gets the supposed log file name (including the date).
   *
   * @returns {string} the log file name.
   */
  getLogName() {
    const date = new Date();
    return `${this.basePath}${date.getFullYear()}_${('0' + (date.getMonth() + 1)).slice(-2)}_${('0' + date.getDate()).slice(-2)}.log`;
  }

  /**
   * Prints an implementation (TO DO) note.
   *
   * @param {*} data data to log.
   */
  implement(data) {
    console.log(`>       ${'IMPLEMENTATION NOTE'.italic.cyan}: "${data}"`);
  }

  /**
   * Prints a string and logs it. (alias: info)
   *
   * @param {*} data The data to log
   */
  info(data) {
    return this.print(data);
  }

  /**
   * Logs a string into the server's log file
   *
   * @param {*} data data to log.
   */
  log(data) {
    this.logFile.write(data);
    console.log(`[LOG]   ${data}`);
  }

  /**
   * Prints a string and logs it. (alias: info)
   *
   * @param {*} data The data to log
   */
  print(data) {
    this.logFile.write(`[INFO] ${data}`);
    console.log(`[${'INFO'.yellow}]  ${data}`);
  }

  /**
   * Prints (and logs) a success message
   *
   * @param {*} data The data to log
   * @param {boolean} [slack = false] send the message to slack
   */
  success(data, slack = false) {
    this.logFile.write(`[GOOD]  ${data}`);
    console.log(`[${'GOOD'.green}]  ${data}`);

    if (slack) {
      shared.integration.slack.sendInfo(data);
    }
  }

  /**
   * Prints (and logs) a warning to the console.
   *
   * @param {*} data The data to log
   */
  warn(data) {
    this.logFile.write(`[WARN]  ${data}`);
    console.log(`[WARN] ${data}`.black.bgWhite);
    shared.integration.slack.sendWarning(data);
  }
}
