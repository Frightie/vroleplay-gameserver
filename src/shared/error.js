/**
 * @title error.js
 * @overview Custom Error Implementation
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

/**
 * @namespace
 */
const Errors = {};

/**
 * ErrorTypes by V:RP
 */
export default Errors;

Errors.install = scope => {
  for (const key in Errors.errorTypes) {
    if (Errors.errorTypes.hasOwnProperty(key)) {
      scope[key] = Errors.errorTypes[key];
    }
  }
};

class FatalError extends Error {
  constructor(message) {
    super(message);
    this.name = 'FatalError';
    this.message = message || 'Fatal Error raised';
    this.stack = new Error().stack;
  }
}

class DatabaseError extends Error {
  constructor(message) {
    super(message);
    this.name = 'DatabaseError';
    this.message = message || 'Database Error raised';
    this.stack = new Error().stack;
  }
}

class ScriptError extends Error {
  constructor(message) {
    super(message);
    this.name = 'ScriptError';
    this.message = message || 'Script Error raised';
    this.stack = new Error().stack;
  }
}

Errors.errorTypes = {
  FatalError: FatalError,
  DatabaseError: DatabaseError,
  ScriptError: ScriptError,
};
