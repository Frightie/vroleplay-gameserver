/**
 * @title shared.js
 * @overview Main Shared File
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

// Predefined V:RP Header to install our own error types & handler
import error from './error';
import errorHandler from './errorHandler';
error.install(global);
process.on('uncaughtException', errorHandler);

import Logger from './logger';
import Utility from './utility';
import LogFile from './classes/log';
import Command from './classes/command';
import CommandGroup from './classes/commandGroup';
import Enums from './enums';
import Slack from './integration/slack';
import Stat from './classes/stat';
import VitalStat from './classes/vitalStat';
import StatModifier from './classes/statModifier';
import Boost from './classes/boost';
import Timeout from './classes/timeout';

const integration = {
  slack: Slack,
};

global.vrp = {};

global.shared = { //eslint-disable-line
  LogFile,
  Command,
  CommandGroup,
  integration,
  enum: Enums,
  Stat,
  VitalStat,
  StatModifier,
  Boost,
  Timeout,
  logger: new Logger('./vlogs/server'),
  utility: Utility,
  _setVRP(vrp) {
    global.vrp = vrp;
  },
};
global.log = global.shared.logger;

/**
 * Shared Namespace
 */
export default global.shared;
