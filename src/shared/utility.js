/**
* @file Implements utility functions
* @author Ariel Weinberger
*/

let weapons = [];

/**
 * Utility Class
 */
export default class Utility {
  /**
   * Concats an Object. First Parameter is dominant
   *
   * @param {Object} object1 First Object.
   * @param {Object} object2 Object to cat into object1.
   * @returns {Object}
   */
  static concat(object1, object2) {
    /*eslint-disable*/
    if (typeof object1 === 'undefined') {
      object1 = object2;
    }
    else {
      for (var idx in object2) {
        if (typeof object1[idx] === 'undefined') {
          object1[idx] = object2[idx];
        }
      }
    }
    /*eslint-enable*/
    return object1;
  }

  /**
   * Calculates the distance between two points
   *
   * @param {Vector3f} vec1 the first vector
   * @param {Vecotr3f} vec2 the second vector
   * @returns {float} the distance between the points
   */
  static distance(vec1, vec2) {
    if (!this.isVector3(vec1) || !this.isVector3(vec2)) {
      return 0.0;
    }

    return Math.sqrt(Math.pow(vec1.x - vec2.x, 2) +
           Math.pow(vec1.y - vec2.y, 2) +
           Math.pow(vec1.z - vec2.z, 2));
  }

   /**
    * Checks whether the given object has the same JSON signature
    *
    * @param {Object} object1
    * @param {Object} object2
    */
  static isSameObject(object1, object2) {
    return JSON.stringify(object1) === JSON.stringify(object2);
  }

   /**
    * Checks whether the given Object has Vector3-Like members
    *
    * @param {Object} vec the suspected Vector3
    * @returns {boolean} whether you can handle it as a Vector or not
    */
  static isVector3(vec) {
    return vec.hasOwnProperty('x') && vec.hasOwnProperty('y') &&
          vec.hasOwnProperty('z') && !isNaN(vec.x) && !isNaN(vec.y) &&
          !isNaN(vec.z);
  }

   /**
    * Calculates the gray tone based on the distance
    *
    * @param {float} distance distance between the points
    * @param {float} maxDistance maximum distance (=darkest color)
    * @param {float} [dropStart=0.0] drop offset
    * @param {number} [minTone=128] the darkest tone possible
    * @returns {RGB} color
    */
  static getColorFromDistance(distance, maxDistance, dropStart = 0.0, minTone = 128) {
    let colorValue = 255 - Math.round(((distance - dropStart) / (maxDistance - dropStart)) * (255 - minTone)); //eslint-disable-line
    if (colorValue > 255) {
      colorValue = 255;
    } else if (colorValue < minTone) {
      colorValue = minTone;
    }
    return new RGB(colorValue, colorValue, colorValue);
  }

   /**
    * Escapes a string in order to prevent MySQL injections
    *
    * @param {string} str the string to escape.
    * @returns {string} the escaped string
    */
    static mysqlEscape(str) {
      return str.replace(/[\0\x08\x09\x1a\n\r"'\\\%]/g, char => {
        switch (char) {
        case '\0':
          return '\\0';
        case '\x08':
          return '\\b';
        case '\x09':
          return '\\t';
        case '\x1a':
          return '\\z';
        case '\n':
          return '\\n';
        case '\r':
          return '\\r';
        case '\'':
        case '"':
        case '\\':
        case '%':
          return '\\' + char; // prepends a backslash to backslash, percent,
                            // and double/single quotes
        default:
          return char;
        }
      });
    }

    /**
     * Parses a date as DD.MM.YYYY HH:MM:SS
     *
     * @param {object} date The date that should be parsed
     * @param {string} [opt_dateSeparator = '.'] date separating character
     * @param {string} [opt_timeSeparator = ':'] time separating character
     * @returns {string} the parsed date
     */
    static parseDate(date, dateSeparator = '.', timeSeparator = ':') {
      let parsed = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
      parsed += dateSeparator;
      parsed += date.getMonth() < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1;
      parsed += dateSeparator;
      parsed += date.getFullYear();
      parsed += ' ';
      parsed += date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
      parsed += timeSeparator;
      parsed += date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
      parsed += timeSeparator;
      parsed += date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
      return parsed;
    }

    /**
     * @type {Array<Object>}
     */
    get weapons() {
      return weapons;
    }

    /**
     * Finds a Weapon by its name or hash
     *
     * @param {number|string} nameOrHash weapon name or its hash
     * @returns {Object} weapon object
     */
    static findWeapon(nameOrHash) {
      for (let i = 0; i < weapons.length; i++) {
        if (weapons[i].name === nameOrHash || weapons[i].hash === nameOrHash) {
          return weapons[i];
        }
      }
      return weapons[0];
    }
}

weapons = [
  { name: 'WEAPON_UNARMED', hash: 0xA2719263 },
  { name: 'WEAPON_ANIMAL', hash: 0xF9FBAEBE },
  { name: 'WEAPON_COUGAR', hash: 0x08D4BE52 },
  { name: 'WEAPON_KNIFE', hash: 0x99B507EA },
  { name: 'WEAPON_NIGHTSTICK', hash: 0x678B81B1 },
  { name: 'WEAPON_HAMMER', hash: 0x4E875F73 },
  { name: 'WEAPON_BAT', hash: 0x958A4A8F },
  { name: 'WEAPON_GOLFCLUB', hash: 0x440E4788 },
  { name: 'WEAPON_CROWBAR', hash: 0x84BD7BFD },
  { name: 'WEAPON_PISTOL', hash: 0x1B06D571 },
  { name: 'WEAPON_COMBATPISTOL', hash: 0x5EF9FEC4 },
  { name: 'WEAPON_APPISTOL', hash: 0x22D8FE39 },
  { name: 'WEAPON_PISTOL50', hash: 0x99AEEB3B },
  { name: 'WEAPON_MICROSMG', hash: 0x13532244 },
  { name: 'WEAPON_SMG', hash: 0x2BE6766B },
  { name: 'WEAPON_ASSAULTSMG', hash: 0xEFE7E2DF },
  { name: 'WEAPON_ASSAULTRIFLE', hash: 0xBFEFFF6D },
  { name: 'WEAPON_CARBINERIFLE', hash: 0x83BF0278 },
  { name: 'WEAPON_ADVANCEDRIFLE', hash: 0xAF113F99 },
  { name: 'WEAPON_MG', hash: 0x9D07F764 },
  { name: 'WEAPON_COMBATMG', hash: 0x7FD62962 },
  { name: 'WEAPON_PUMPSHOTGUN', hash: 0x1D073A89 },
  { name: 'WEAPON_SAWNOFFSHOTGUN', hash: 0x7846A318 },
  { name: 'WEAPON_ASSAULTSHOTGUN', hash: 0xE284C527 },
  { name: 'WEAPON_BULLPUPSHOTGUN', hash: 0x9D61E50F },
  { name: 'WEAPON_STUNGUN', hash: 0x3656C8C1 },
  { name: 'WEAPON_SNIPERRIFLE', hash: 0x05FC3C11 },
  { name: 'WEAPON_HEAVYSNIPER', hash: 0x0C472FE2 },
  { name: 'WEAPON_REMOTESNIPER', hash: 0x33058E22 },
  { name: 'WEAPON_GRENADELAUNCHER', hash: 0xA284510B },
  { name: 'WEAPON_GRENADELAUNCHER_SMOKE', hash: 0x4DD2DC56 },
  { name: 'WEAPON_RPG', hash: 0xB1CA77B1 },
  { name: 'WEAPON_PASSENGER_ROCKET', hash: 0x166218FF },
  { name: 'WEAPON_AIRSTRIKE_ROCKET', hash: 0x13579279 },
  { name: 'WEAPON_STINGER', hash: 0x687652CE },
  { name: 'WEAPON_MINIGUN', hash: 0x42BF8A85 },
  { name: 'WEAPON_GRENADE', hash: 0x93E220BD },
  { name: 'WEAPON_STICKYBOMB', hash: 0x2C3731D9 },
  { name: 'WEAPON_SMOKEGRENADE', hash: 0xFDBC8A50 },
  { name: 'WEAPON_BZGAS', hash: 0xA0973D5E },
  { name: 'WEAPON_MOLOTOV', hash: 0x24B17070 },
  { name: 'WEAPON_FIREEXTINGUISHER', hash: 0x060EC506 },
  { name: 'WEAPON_PETROLCAN', hash: 0x34A67B97 },
  { name: 'WEAPON_DIGISCANNER', hash: 0xFDBADCED },
  { name: 'WEAPON_BRIEFCASE', hash: 0x88C78EB7 },
  { name: 'WEAPON_BRIEFCASE_02', hash: 0x01B79F17 },
  { name: 'WEAPON_BALL', hash: 0x23C9F95C },
  { name: 'WEAPON_FLARE', hash: 0x497FACC3 },
  { name: 'WEAPON_VEHICLE_ROCKET', hash: 0xBEFDC581 },
  { name: 'WEAPON_BARBED_WIRE', hash: 0x48E7B178 },
  { name: 'WEAPON_DROWNING', hash: 0xFF58C4FB },
  { name: 'WEAPON_DROWNING_IN_VEHICLE', hash: 0x736F5990 },
  { name: 'WEAPON_BLEEDING', hash: 0x8B7333FB },
  { name: 'WEAPON_ELECTRIC_FENCE', hash: 0x92BD4EBB },
  { name: 'WEAPON_EXPLOSION', hash: 0x2024F4E8 },
  { name: 'WEAPON_FALL', hash: 0xCDC174B0 },
  { name: 'WEAPON_EXHAUSTION', hash: 0x364A29EC },
  { name: 'WEAPON_HIT_BY_WATER_CANNON', hash: 0xCC34325E },
  { name: 'WEAPON_RAMMED_BY_CAR', hash: 0x07FC7D7A },
  { name: 'WEAPON_RUN_OVER_BY_CAR', hash: 0xA36D413E },
  { name: 'WEAPON_HELI_CRASH', hash: 0x145F1012 },
  { name: 'WEAPON_FIRE', hash: 0xDF8E89EB },
  { name: 'WEAPON_BOTTLE', hash: 0xF9E6AA4B },
  { name: 'WEAPON_GUSENBERG', hash: 0x61012683 },
  { name: 'WEAPON_SPECIALCARBINE', hash: 0xC0A3098D },
  { name: 'WEAPON_HEAVYPISTOL', hash: 0xD205520E },
  { name: 'WEAPON_SNSPISTOL', hash: 0xBFD21232 },
  { name: 'WEAPON_BULLPUPRIFLE', hash: 0x7F229F94 },
  { name: 'WEAPON_DAGGER', hash: 0x92A27487 },
  { name: 'WEAPON_VINTAGEPISTOL', hash: 0x083839C4 },
  { name: 'WEAPON_FIREWORK', hash: 0x7F7497E5 },
  { name: 'WEAPON_MUSKET', hash: 0xA89CB99E },
  { name: 'WEAPON_HEAVYSHOTGUN', hash: 0x3AABBBAA },
  { name: 'WEAPON_MARKSMANRIFLE', hash: 0xC734385A },
  { name: 'WEAPON_HOMINGLAUNCHER', hash: 0x63AB0442 },
  { name: 'WEAPON_PROXMINE', hash: 0xAB564B93 },
  { name: 'WEAPON_SNOWBALL', hash: 0x787F0BB },
  { name: 'WEAPON_RAILGUN', hash: 0x6D544C99 },
  { name: 'WEAPON_HATCHET', hash: 0xF9DCBF2D },
];
