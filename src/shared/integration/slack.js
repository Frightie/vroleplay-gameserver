/**
 * @title slack.js
 * @overview Slack Integration
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

import querystring from 'querystring';
import request from '../node_modules/request';

/**
 * Slack Integration for V:RP. Provides the Ability to send Messages
 * to slack channels.
 */
class Slack {
  /**
   * @constructs {Slack}
   * @param {string} token Auth token for Slack
   */
  constructor(token) {
    /** @type {string} */
    this.token = token;

    /** @type {boolean} */
    this.enabled = false;
  }

  /**
   * Sends a simple message (only text)
   *
   * @param {string} message the message
   */
  send(message) {
    this.sendMessage(message, {
      simpleMessage: true,
    });
  }

  /**
   * Sends an error message to slack
   *
   * @param {string} message the message
   * @param {Object=} [options={}] message options
   */
  sendError(message, options = {}) {
    const defaultOptions = {
      color: 'danger',
      title: 'Server Error',
      username: '[ERROR] V:RP Server',
      channel: '#development',
    };
    this.sendMessage(message, shared.utility.concat(options, defaultOptions));
  }

  /**
   * Sends an fatal error message to slack
   *
   * @param {string} message the message
   * @param {Object=} [options={}] message options
   */
  sendFatal(message, options) {
    const defaultOptions = {
      color: 'danger',
      username: '[FATAL] V:RP Server',
      title: '!! Fatal Server Error !!',
      channel: '#development',
      symbol: 'warning',
    };
    this.sendMessage(message, shared.utility.concat(options, defaultOptions));
  }

  /**
   * Sends an info message to slack
   *
   * @param {string} message the message
   * @param {Object=} [options={}] message options
   */
  sendInfo(message, options) {
    const defaultOptions = {
      color: 'good',
      title: 'Server Info',
      username: '[INFO] V:RP Server',
      channel: '#general',
    };
    this.sendMessage(message, shared.utility.concat(options, defaultOptions));
  }

  /**
   * Sends a warning message to slack
   *
   * @param {string} message the message
   * @param {Object=} [options={}] message options
   */
  sendWarning(message, options) {
    const defaultOptions = {
      color: 'warning',
      title: 'Server Warning',
      username: '[WARNING] V:RP Server',
      channel: '#development',
    };
    this.sendMessage(message, shared.utility.concat(options, defaultOptions));
  }

  /**
   * Sends an error message to slack
   *
   * @param {string} message the message
   * @param {Object=} [options={}] message options
   */
  sendMessage(message, options = {}) {
    if (!this.enabled) {
      if (typeof options.callback === 'function') {
        options.callback();
      }
      return;
    }

    const defaultOptions = {
      color: '#2c3e50',
      channel: '#development',
      symbol: 'computer',
      title: 'Server Message',
      pretext: '',
      simpleMessage: false,
      username: 'V:RP Server',
      callback: () => {},

      fallback: message,
    };

    shared.utility.concat(options, defaultOptions);

    let postData;
    if (options.simpleMessage) {
      postData = querystring.stringify({
        'token': this.token,
        'channel': options.channel,
        'username': 'V:RP Server',
        'icon_emoji': `:${options.symbol}:`,
        'text': message,
      });
    } else {
      postData = querystring.stringify({
        'token': this.token,
        'channel': options.channel,
        'username': options.username,
        'icon_emoji': `:${options.symbol}:`,
        'attachments': JSON.stringify([
          {
            'pretext': options.pretext,
            'fallback': options.fallback,
            'title': options.title,
            'title_link': options.title_link,
            'text': message,
            'color': options.color,
          },
        ]),
      });
    }

    request.post({
      headers: { 'content-type': 'application/x-www-form-urlencoded' },
      url: 'https://slack.com/api/chat.postMessage',
      body: postData,
    }, error => {
      if (error) {
        console.log(error);
      }

      options.callback();
    });
  }

  /**
   * Sends a raw message (you need to define all options yourself)
   *
   * @param {string} data message data
   * @param {function ()} [callback=()=>{}] callback when sending finished
   */
  sendRaw(data, callback = () => {}) {
    if (!this.enabled) {
      callback();
      return;
    }

    request.post({
      headers: {'content-type': 'application/x-www-form-urlencoded'},
      url: 'https://slack.com/api/chat.postMessage',
      body: querystring.stringify(data),
    }, error => {
      if (error) {
        console.log(error);
      }

      callback();
    });
  }
}

/**
 * Slack Instance
 */
export default new Slack('xoxp-3134456568-3135052506-3531436889-66fc9c');
