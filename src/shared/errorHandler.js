/**
 * @title errorHandler.js
 * @overview Custom Error Handler Implementation
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

require('colors');

import { errorTypes as errors } from './error';
let lsterr = null;

/**
 * Handles an uncaught exception
 *
 * @param {Error} error the error
 */
export default function uncaughtException(err) {
  if (err === lsterr) {
    return;
  }

  lsterr = err;
  if (err instanceof Error || typeof err.stack !== 'undefined') {
    // Remove first callstack item
    const stackArray = err.stack.split(' at ');

    let bCustomType = false;
    for (const idx in errors) {
      if (idx === err.name) {
        bCustomType = true;
        break;
      }
    }

    let topLevelFunction;
    if (bCustomType) {
      topLevelFunction = stackArray[2];
      stackArray.splice(0, 2);
    } else {
      topLevelFunction = stackArray[1];
      stackArray.splice(0, 1);
    }
    try {
      console.log(`${err.name}: ${err.message}`);
      console.log(`Callstack: ${err.stack}`);
    } catch (e) {
      console.log(`${err.name}: ${err.message}`);
    }
    console.log('--------------------------------------------------------------------------------'.red);
    console.log('                           Uncaught Script Exception');
    console.log(`   ${'Error Type'.red}: ${err.name}`);
    console.log(`   ${'Error Description'.red}: ${err.message}`);

    const fileBegin = topLevelFunction.indexOf('(') + 1;
    const fileEnd = topLevelFunction.indexOf(':', fileBegin + 3); // Remove Windows Style ':'

    if (fileBegin !== -1 && fileEnd !== -1) {
      let fileName = topLevelFunction.substr(fileBegin, fileEnd - fileBegin);
      console.log(`   ${'File'.yellow}: ${fileName}`);

      const lineBegin = fileEnd + 1;
      const lineEnd = topLevelFunction.indexOf(':', lineBegin);

      if (lineBegin !== -1 && lineEnd !== -1) {
        const line = topLevelFunction.substr(lineBegin, lineEnd - lineBegin);
        console.log(`   ${'Line'.yellow}: ${line}`);
        console.log('');
        console.log(`                                    ${'CALLSTACK'.red}`);
        console.log(`                                    ${'---------'.red}`);
        console.log(stackArray.join(' at '));
        console.log('--------------------------------------------------------------------------------'.red);

        // Let's send some slack message

        if (typeof shared.integration.slack.sendMessage !== 'function') {
          console.log('Cannot send Slack notification: Could not find sendMessage function');
          return;
        }

        fileName = fileName.substr(fileName.indexOf('packages'));

        shared.integration.slack.sendRaw({
          'token': 'xoxp-3134456568-3135052506-3531436889-66fc9c',
          'channel': '#development',
          'username': '[ERROR] V:RP Server',
          'icon_emoji': ':exclamation:',
          'attachments': JSON.stringify([
            {
              'fallback': `${err.name}: ${err.message} in ${fileName}`,
              'text': `${err.name} - ${err.message}`,
              'fields': [
                {
                  'title': 'File',
                  'value': `${fileName}, Line ${line}`,
                  'short': true,
                },
                {
                  'title': 'Date',
                  'value': shared.utility.parseDate(new Date()),
                  'short': true,
                },
              ],
              'color': 'danger',
            },
          ]),
        });
      }
    }
  } else {
    log.error(err);
  }
}
