/**
 * @title enums.js
 * @overview This file contains various enumerations
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

/**
 * Enumerations
 */
export default {
  LoginResult: {
    ErrorBanned: -6,
    ErrorWrongAuthCode: -5,
    ErrorAuthName: -4,
    ErrorInternal: -3,
    ErrorUnknownAccount: -2,
    ErrorWrongPassword: -1,
    ErrorLoginDisabled: 0,
    SuccessWaitForAuth: 1,
    Success: 2,
  },
};
