/**
 * @title command.js
 * @overview Command Class Implementation
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

/** @class Command */
export default class Command {
  /**
  * Command Constructor
  *
  * @constructor
  * @this {Command}
  * @param {string|string[]} trigger - One or multiple triggers for the command (typed in the chat)
  * @param {Object} [options] - The options for that Command
  * @param {Array<Array<string>>} [options.parameters] - Command parameters
  * @param {function (player: Player): bool} [options.validator] - Command validator
  * @param {function(player: Player, ..args: *): boolean} handler - The command handler
  */
  constructor(trigger, options, handler) {
    /** @type {string} */
    this.usage = '';

    /** @type {Object} */
    this.options = {
      parameters: [],
      minimumParameters: 0,
      optionalParameters: 0,
      validator: player => {
        if (typeof player !== 'undefined') {
          return true;
        }
        return player.playing === true;
      },
      permissions: [],
      parent: undefined,
    };
    this.options = shared.utility.concat(options, this.options);

    let triggers = [];
    if (typeof trigger === 'string') {
      triggers.push(trigger);
    } else {
      triggers = trigger;
    }

    // Validate parameters, check if 'text' is not a last parameter
    for (let i = 0; i < this.options.parameters.length; i++) {
      if (options.parameters[i].length < 1 ) {
        throw new ScriptError(`Command ${triggers[0]}: Parameter mismatch, array length is < 1`);
      }
      if (options.parameters[i][1] === 'text' && i < options.parameters.length - 1) {
        throw new ScriptError(`Command ${triggers[0]}: Parameter mismatch, "text" always has to be the last parameter`);
      }

      if (this.options.parameters[i].length === 3) {
        this.options.optionalParameters++;
      } else {
        if (this.options.optionalParameters) {
          throw new ScriptError(`Required parameter after optional parameter in Command '${triggers[0]}'.`);
        }
        this.options.minimumParameters++;
      }
    }

    /** @type {Set} */
    this.trigger = new Set(triggers);

    /** @type {function(player: Player, args: ...*): boolean} */
    /** @private */ this._handler = handler;

    if (!this.options.skipUsage) {
      this.generateUsageMessage();
    }
  }

  /**
   * Unregisters (removes) the command
   *
   */
  unregister() {
    vrp.server.commandManager.remove(this);

    this._handler = () => { log.error('Invalidated function called'); };
  }

  /**
   * Executes the command
   *
   * @param {Player} player The player executing the function
   * @param {string} commandArguments Command Arguments.
   * @returns {boolean} result of the command handler
   */
  execute(player, commandArguments) {
    console.time('ExecuteStart');
    const validation = this.options.validator(player, commandArguments);
    if (validation === false) {
      return player.sendError('You are not allowed to use this command.');
    } else if (typeof validation === 'string') {
      return player.sendError(validation);
    }

    for (let i = 0; i < this.options.permissions.length; i++) {
      if (!player.permissions.has(this.options.permissions[i])) {
        player.debug(`Missing '${this.options.permissions[i]}'`);
        return player.sendError('You are not allowed to use this command.');
      }
    }

     // Check parameters
    let finished = 0;
    let executed = false;
    const fnDispatch = () => {
      if (executed) {
        return;
      }
      executed = true;
      commandArguments.unshift(player);
      console.timeEnd('ExecuteStart');
      this._handler.apply(this, commandArguments);
      return;
    };
    const fnConvert = (type, argIdx) => {
      events.Call('find player', commandArguments[argIdx], players => {
        if (!players.length) {
          return player.sendMessage('Could not find the target.');
        }
        if (players.length > 1) {
          return player.sendError('More than one target found.');
        }
        if (type === 'target' && players[0].account.id === player.account.id) {
          return player.sendError('Target (not self) required.');
        }

        commandArguments[argIdx] = players[0];

        finished++;
        if (finished >= this.options.parameters.length) {
          fnDispatch();
        }
      });
    };

    if (this.options.parameters.length > 0) {
      if (commandArguments.length < this.options.minimumParameters) {
        return player.sendMessage(this.usage);
      }


      // Convert parameters
      (() => {
        console.time('ConvertingParams');
        for (let i = 0; i < this.options.parameters.length; i++) {
          // Default Value for optional arguments
          if (i >= commandArguments.length) {
            commandArguments.push(this.options.parameters[i][2]);
            finished++;
            continue;
          }

          switch (this.options.parameters[i][1]) {
          case 'integer':
            commandArguments[i] = parseInt(commandArguments[i], 10);
            if (isNaN(commandArguments[i])) {
              return player.sendMessage(this.usage);
            }
            finished++;
            break;
          case 'float':
            commandArguments[i] = parseFloat(commandArguments[i]);
            if (isNaN(commandArguments[i])) {
              return player.sendMessage(this.usage);
            }
            finished++;
            break;
          case 'text':
            commandArguments[i] = commandArguments.splice(i, commandArguments.length - i).join(' ');
            finished++;
            return true;
          case 'player':
            fnConvert('player', i);
            break;
          case 'target':
            fnConvert('target', i);
            break;
          default:
            finished++;
            break;
          }
        }
        console.timeEnd('ConvertingParams');
      })();
    }

    if (finished >= this.options.parameters.length) {
      fnDispatch();
    }
  }

   /**
    * Generates the USAGE Message for the Command
    */
  generateUsageMessage() {
    let usage = '';
    if (this.options.parent) {
      usage = `${this.options.parent.usage} `;
    } else {
      // general usage message
      usage = 'USAGE: /';
    }

    if (this.trigger.size > 1) {
      usage = `${usage}[`;
    }

    this.trigger.forEach(strTrigger => {
      usage = `${usage}${strTrigger}|`;
    });
    usage = usage.substr(0, usage.length - 1);
    if (this.trigger.size > 1) {
      usage = `${usage}]`;
    }

    for (let i = 0; i < this.options.parameters.length; i++) {
      if (this.options.minimumParameters - i === 0 && this.options.optionalParameters) {
        usage = `${usage} ([${this.options.parameters[i][0]}]`;
      } else {
        usage = `${usage} [${this.options.parameters[i][0]}]`;
      }
    }

    if (this.options.optionalParameters) {
      usage = `${usage})`;
    }

    this.usage = usage;
  }
}
