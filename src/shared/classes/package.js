/**
 * @title package.js
 * @overview Package Class
 * @author Ariel Weinberger
 */

import fs from 'fs';

/**
 * Package Class. Base Class for all Packages.
 */
export default class Package {
  /**
   * Constructs the Package with the given name
   *
   * @constructor
   * @param {string} name the package name
   */
  constructor(name) {
    /** @type {string} */
    this.name = name;

    /** @type {boolean} */
    this.started = false;

    // Register Event Handler
    events.Add(`dear ${name}, please give yourself to me.`, () => {
      return this;
    });

    events.Add('registerPermissions', () => {
      return { data: this.registerPermissions.bind(this)() || [] };
    });
  }

  /**
   * Starts the package
   */
  start() {
    log.success(`Package "${this.name}" started.`);
    this.started = true;
  }

  /**
   * returns the package's commands, if there are any.
   *
   * @returns {Object} command object
   */
  get commands() {
    if (typeof this._commands === 'undefined') {
      if (fs.existsSync(`./packages/${this.name}/commands/commands.js`)) {
        this._commands = require(`../../packages/${this.name}/commands/commands.js`);
        log.debug(`package ${this.name} has commands.`, 2);
      } else {
        this._commands = {};
        log.debug(`package ${this.name} has no commands.`, 2);
      }
    }
    return this._commands;
  }

  /**
   * Returns the Package's dependencies
   *
   * @type {[string]}
   */
  get dependencies() {
    return [];
  }

  /**
   * Returns the package's events, if there are any.
   *
   * @type {Object} event object
   */
  get events() {
    if (typeof this._events === 'undefined') {
      if (fs.existsSync(`./packages/${this.name}/events.js`)) {
        this._events = require(`../../packages/${this.name}/events`);
      } else {
        this._events = {};
      }
    }
    log.debug(`package ${this.name} has ${(this._events === {} ? 'no ' : '')} events.`, 2);
    return this._events;
  }

  /**
   * Returns the package's interace.
   *
   * @type {Object}
   */
  get interface() {
    return this;
  }

  /**
   * Receives the Dependencies' interfaces from the package manager
   *
   * @param {Object} scope the global module scope
   * @param {Object} deps the dependencies
   */
  receiveDependencies(scope, deps) {
    if (typeof scope.vrp === 'undefined') {
      scope.vrp = {};
    }

    // Called before receiveVRPNamespace
    deps.forEach((value, idx) => {
      scope.vrp[idx] = value;

      if (idx === 'shared') {
        // Make shared namespace
        scope.shared = value;
        // Make Log Namespace
        scope.log = scope.shared.logger;
      }
    });
  }

  /**
   * Receives the whole V:RP Namespace
   * Warning: overrides the data received in 'receiveDependencies'
   *
   * @param {Object} scope the global module scope
   * @param {Object} vrpNamespace the VRP Namespace
   */
  receiveVRPNamespace(scope, vrpNamespace) {
    const fixedNamespace = {};

    for (const key in vrpNamespace) {
      if (key === this.name) {
        continue;
      }

      fixedNamespace[key] = vrpNamespace[key];
    }

    // Copy over the interfaces
    scope.vrp = vrpNamespace;

    // Copy shared
    if (typeof vrp.shared === 'undefined') {
      throw new Error('V:RP Namespace does not contain "shared"');
    }
    scope.shared = vrp.shared;
  }

  /**
   * Registers all Permissions. Static Permissions are returned as string array,
   * dynamic permissions can be created using vrp.permission.create.
   */
  registerPermissions() {
    return [];
  }
}
