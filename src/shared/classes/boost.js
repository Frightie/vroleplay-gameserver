/**
 * @title boost.js
 * @overview Boost Class (StatModifier over time)
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

import StatModifier from './statModifier';

/**
 * A Boost is a StatModifier that only runs for a certain
 * amount of time.
 */
export default class Boost extends StatModifier {
  /**
   * Creates a new Boost
   *
   * @param {string} stat - The name of the Stat.
   * @param {Object} modifier - The modifications that will be made
   * @param {string} modifier.type - The type (ratio = * modifier, absolute (default) = +/- modifier)
   * @param {float} modifier.value - The modification value
   * @param {Object} options - Other options
   * @param {function (player: Player)} options.beforeApply - Callback when the stat will be applied
   * @param {function (player: Player)} options.afterApply - Callback when the stat has been applied
   */
  constructor(stat, modifier, options = {}) {
    super(stat, modifier, options);

    /** @type {int} */
    this.duration = this.options.boost;
  }

  /**
   * Tries to find a Boost (to increase performance) or create a new one.
   *
   * @param {string} stat - The name of the Stat.
   * @param {Object} modifier - The modifications that will be made
   * @param {string} modifier.type - The type (ratio = * modifier, absolute (default) = +/- modifier)
   * @param {float} modifier.value - The modification value
   * @param {Object} options - Other options
   * @param {function (player: Player)} options.beforeApply - Callback when the stat will be applied
   * @param {function (player: Player)} options.afterApply - Callback when the stat has been applied
   *
   * @returns {Boost}
   */
  static findOrCreate(stat, modifier, options = {}) {
    return super.findOrCreate(stat, modifier, options, Boost);
  }

  /**
   * Creates a Boost from a Database Model.
   *
   * @param {database.Boost} dbModel - The Database Model.
   *
   * @returns {Boost}
   */
  static fromDBModel(dbModel) {
    const boost = StatModifier.findOrCreate(dbModel.stat, { type: 'value', value: dbModel.value }, { boost: dbModel.remaining }, Boost);

    for (const player of vrp.player.manager.players.values()) {
      if (player.character && player.character.id === dbModel.applied_to) {
        boost.apply(player, dbModel);
        return { boost, applied: true };
      }
    }
    return { boost, applied: false };
  }

  /**
   * Manages the active Boosts for a Stat (e.g. of a player).
   *
   * @param {Set<Boost>} boosts - The Set to manage.
   * @param {string} stat - The stat that will be modified.
   * @param {number} value - The - supposed - new value.
   */
  static manageBoosts(boosts, stat, value) {
    const del = [];
    let valueLeft = value;
    for (const boost of boosts.values()) {
      if (boost.stat !== stat) {
        continue;
      }

      if (valueLeft === boost.value) {
        valueLeft = 0;
        boost.value = 0;
        del.push(boost);
        break;
      } else if (valueLeft < 0) {
        if (valueLeft < boost.value) {
          valueLeft += boost.value;
          boost.value = 0;
          del.push(boost);
        } else {
          boost.value += valueLeft;
          if (boost.dbModel) {
            boost.dbModel.value = boost.value;
          }
          valueLeft = 0;
          break;
        }
      } else {
        if (boost.value < valueLeft) {
          valueLeft -= boost.value;
          if (boost.dbModel) {
            boost.dbModel.value = boost.value;
          }
          boost.value = 0;
          del.push(boost);
        } else {
          boost.value -= valueLeft;
          if (boost.dbModel) {
            boost.dbModel.value = boost.value;
          }
          valueLeft = 0;
          break;
        }
      }
    }

    for (let i = 0; i < del.length; i++) {
      if (del[i].dbModel) {
        del[i].dbModel.destroy();
      } else {
        del[i].deleteFlag = true;
      }
      del[i].timeout.clear();
      boosts.delete(del[i]);
      log.debug(`Boost has been deleted`, 2);
    }
  }

  /**
   * Applies the Boost to a Player.
   *
   * @param {Player} player - The player that should be affected.
   * @param {database.Boost} [model=undefined] - The Database Model for the boost.
   */
  apply(player, model = undefined) {
    player.applyBoost(this.stat, (stat, change, boosts) => {
      const obj = {
        stat: this.stat,
        dbModel: undefined,
        timer: undefined,
        deleteFlag: false,
      };

      const fnDone = () => {
        change(-1 * obj.value);
        boosts.delete(obj);

        if (obj.dbModel) {
          obj.dbModel.destroy();
          log.debug(`Boost ${obj.dbModel.id} has run out.`);
        } else {
          obj.deleteFlag = true;
          log.debug(`Boost (unknown) has run out.`);
        }
      };

      log.debug(`Applying Boost on ${player.character.name} (${this.stat})`);

      boosts.add(obj);
      if (typeof model !== 'undefined') {
        obj.dbModel = model;
        obj.dbModel.value = obj.value = change(obj.dbModel.value);
        obj.timeout = new shared.Timeout(fnDone, obj.dbModel.remaining);
        return;
      }

      if (this.modifier.type === 'ratio') {
        obj.value = stat - stat * this.modifier.value;
      } else {
        obj.value = this.modifier.value;
      }

      obj.timeout = new shared.Timeout(fnDone, this.options.boost * 1000);
      obj.value = change(obj.value);

      vrp.database.Boost.create({
        value: obj.value,
        applied_to: player.character.id,
        stat: this.stat,
        remaining: this.duration,
      }, boost => {
        if (typeof boost === 'undefined') {
          throw new DatabaseError(`Could not create Boost.`);
        }

        obj.dbModel = boost;

        if (obj.deleteFlag) {
          boost.destroy();
        }
      });
    });
  }
}
