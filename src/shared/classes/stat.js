/**
 * @title stat.js
 * @overview A stat is something that has a value. (Very precise, i know)
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

export default class Stat {
  /**
   * Creates a new Stat
   *
   * @param {string} name - Stat name
   * @param {string} description - Short description of the stat
   * @param {number} minValue - Minimum stat value
   * @param {number} maxValue - Maximum stat value
   * @param {Object} [options={}] - Options
   */
  constructor(identifier, name, description, minValue, maxValue, options = {}) {
    /** @type {string} */
    this.identifier = identifier;

    /** @type {string} */
    this.name = name;

    /** @type {string} */
    this.description = description;

    /** @type {number} */
    this.minValue = minValue;

    /** @type {number} */
    this.maxValue = maxValue;

    /** @type {number} */
    this._value = this.minValue;

    /** @type {Object} */
    this.options = shared.utility.concat(options, { hooks: {} });
  }

  /**
   * Resets the Stat's value to the minValue.
   */
  reset() {
    this._value = this.minValue;
  }

  /**
   * @type {number}
   */
  get value() {
    return this._value;
  }

  /**
   * @type {number}
   */
  set value(value) {
    let newValue;
    if (value > this.maxValue) {
      newValue = this.maxValue;
    } else if (value < this.minValue) {
      newValue = this.minValue;
    } else {
      newValue = value;
    }

    const oldValue = this.value;
    this._value = newValue;

    if (typeof this.options.hooks.onChange === 'function') {
      this.options.hooks.onChange(this, oldValue);
    }
  }

  /**
   * Applies a StatModifier
   *
   * @param {function (float) : float} fn modification function (applies)
   */
  applyModifier(fn) {
    const newValue = fn(this.value);
    const oldValue = this.value;

    if (typeof this.options.hooks.beforeApply === 'function') {
      this.options.hooks.beforeApply(this, newValue);
    }

    this.value = fn(this.value);

    if (typeof this.options.hooks.afterApply === 'function') {
      this.options.hooks.afterApply(this, oldValue);
    }
  }
}
