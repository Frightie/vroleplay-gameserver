/**
 * @title timeout.js
 * @overview Timeout class (timeout where you can get the remaining time)
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

/**
 * Timeout Class which allows remaining time to be seen.
 */
export default class Timeout {
  /**
   * Creates a new Timeout
   *
   * @param {function()} fn - The function that should be called.
   * @param {number} time - The time (in ms) after which the function should be
   *                        called.
   */
  constructor(fn, time) {
    /** @type {Date} */
    this.started = Date.now();

    /** @type {number} */
    this.time = time;

    /** @type {Object} */
    this.timeout = setTimeout(fn, time);
  }

  /**
   * @type {number}
   */
  get remaining() {
    return this.time - Date.now() + this.started;
  }

  /**
   * Clears the Timeout.
   */
  clear() {
    clearTimeout(this.timeout);
  }
}
