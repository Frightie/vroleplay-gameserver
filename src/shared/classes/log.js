/**
 * @title log.js
 * @overview Log Class Implementation
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

import fs from 'fs';

/**
 * LogFile Class.
 */
export default class LogFile {
  /**
   * Creates a new Log File
   *
   * @param {string} path file path
   * @param {boolean=} [overwrite=false] whether the file should be overwritten
   */
  constructor(path, overwrite = false) {
    /** @type {string} */
    this.path = path;

    // We do not keep a file handle because the files should be closed safely all the time
    // We always open the file, write to it and then close it immediately.


    if (overwrite) {
      fs.truncate(path, 0, () => {});
    }
  }

  /**
   * Writes a string to the log file
   *
   * @param {string} data message
   */
   write(data) {
     const now = new Date();
     const nowParsed = shared.utility.parseDate(now);


     fs.appendFile(this.path, `[${nowParsed}] ${data}\n`, err => {
       if (err) {
         console.log(`[LOG] FAILED: ${err}`);
       }
     });
   }
}
