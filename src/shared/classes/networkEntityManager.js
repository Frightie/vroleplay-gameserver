/**
 * @title networkEntityManager.js
 * @overview NetworkEntityManager Implementation (Base Class)
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

/**
 * Manages a certain NetworkEntity and associates it with our local class for said Entity.
 */
export default class NetworkEntityManager {
  /**
   * Creates a new NetworkEntityManager
   *
   * @param {string} manages the key of what the Manager manages
   * @param {[*]} [data] the existing data
   */
  constructor(manages, data = []) {
    this[manages] = new Map(data);
    this.manages = manages;

    // create a reference so we know where things are stored
    /** @type {Set<*>} */
    this.map = this[manages];
  }

  /**
   * Adds an item to the Manager
   *
   * @param {*} item the item to add
   */
  add(item) {
    if (!item.hasOwnProperty('networkId')) {
      log.debug(`NetworkEntityManager(${this.manages}::add no networkId in item`);
      return false;
    }
    return this.map.set(item.networkId, item);
  }

  /**
   * Returns whether the given item exists in the manager
   *
   * @param {*} item the item
   * @returns {boolean} whether the item exists or not.
   */
  exists(item) {
    if (!item.hasOwnProperty('networkId')) {
      log.debug(`NetworkEntityManager(${this.manages}::add no networkId in item`);
      return false;
    }
    return this.map.has(item.networkId);
  }

  /**
   * Gets a managed Entity from the Manager
   *
   * @param {*} item The root item (GTA:MP Entity)
   * @returns {*} the Entity (V:RP Entity)
   */
  get(item) {
    if (!this.has(item)) {
      return undefined;
    }
    return this.map.get(item.networkId);
  }

  /**
   * Returns whether the given item exists in the manager
   *
   * @param {*} item the item
   * @returns {boolean} whether the item exists or not.
   */
  has(item) {
    return this.exists(item);
  }

  /**
   * Removes an item from the manager
   *
   * @param {*} item the item to remove
   */
  remove(item) {
    if (!item.hasOwnProperty('networkId')) {
      log.debug(`NetworkEntityManager(${this.manages}::add no networkId in item`);
      return false;
    }
    return this.map.delete(item.networkId);
  }
}
