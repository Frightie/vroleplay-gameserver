/**
 * @title vitalStat.js
 * @overview A vital stat is a stat that decreases over time. It starts with the maximum value
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

import Stat from './stat';

/**
 * A Vital Stat is a Stat that decreases its value over time.
 */
export default class VitalStat extends Stat {
  /**
   * Creates a new Vital Stat.
   *
   * @param {string} identifier - Internal Identifier for the Stat.
   * @param {string} name - The Name of the Stat.
   * @param {string} description - The Description of the Stat.
   * @param {number} minValue - The minimum Value of the Stat.
   * @param {number} maxValue - Maximum stat value
   * @param {number} timeToMin - The time until the stat reaches the minimum value (in milliseconds).
   * @param {Object} [options={}] - Options
   */
  constructor(identifier, name, description, minValue, maxValue, timeToMin, options = {}, stepValue = 1.0) {
    super(identifier, name, description, minValue, maxValue, options);

    /** @type {number} */
    this._value = this.maxValue;

    /** @type {number} */
    this.timeToMin = timeToMin;

    /** @type {number} */
    this.stepValue = stepValue;

    // Create the timer
    /** @type {Object} */
    this._timer = setInterval(() => {
      /** @type {number} */
      this.value -= this.stepValue;
    }, this.timeToMin / (this.maxValue - this.minValue) * this.stepValue);
  }

  /**
   * Destroys the VitalStat.
   */
  destroy() {
    clearInterval(this._timer);
  }

  /**
   * Resets the Stat to its maximum Value.
   */
  reset() {
    this._value = this.maxValue;
  }
}
