/**
 * @title statModifier.js
 * @overview Stat Modfier (modifies a stat, you guessed right)
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

const modifiers = new Map();

/**
 * Modifies a Stat with a Value or by a given Ratio.
 */
export default class StatModifier {
  /**
   * Creates a new StatModifier
   *
   * @param {string} stat - The name of the Stat.
   * @param {Object} modifier - The modifications that will be made
   * @param {string} modifier.type - The type (ratio = * modifier, absolute (default) = +/- modifier)
   * @param {float} modifier.value - The modification value
   * @param {Object} options - Other options
   * @param {function (player: Player)} options.beforeApply - Callback when the stat will be applied
   * @param {function (player: Player)} options.afterApply - Callback when the stat has been applied
   */
  constructor(stat, modifier, options = {}) {
    /** @type {string} */
    this.stat = stat;

    /** @type {Object} */
    this.modifier = modifier;

    /** @type {Object} */
    this.options = options;

    shared.utility.concat(this.modifier, {type: 'value'});
  }

  /**
   * Tries to find an existing StatModifier (for performance increase) or creates a new one.
   *
   * @param {string} stat - The name of the Stat.
   * @param {Object} modifier - The modifications that will be made
   * @param {string} modifier.type - The type (ratio = * modifier, absolute (default) = +/- modifier)
   * @param {float} modifier.value - The modification value
   * @param {Object} options - Other options
   * @param {function (player: Player)} options.beforeApply - Callback when the stat will be applied
   * @param {function (player: Player)} options.afterApply - Callback when the stat has been applied
   *
   * @returns {StatModifier}
   */
  static findOrCreate(stat, modifier, options = {}, Class = StatModifier) {
    const json = JSON.stringify(options);
    if (modifiers.has(`${stat}:${modifier.type}:${modifier.value}:${json}`)) {
      return modifiers.get(`${stat}:${modifier.type}:${modifier.value}:${json}`);
    }
    return new Class(stat, modifier, options);
  }

  /**
   * Applies the StatModifier to a Player.
   *
   * @param {Player} player - The Player where the Stat Modifier should be applied on.
   */
  apply(player) {
    player.applyStatModifier(this.stat, stat => {
      if (this.modifier.type === 'ratio') {
        return stat * this.modifier.value;
      }
      return stat + this.modifier.value;
    });
  }
}
