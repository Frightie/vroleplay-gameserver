/**
 * @title commandGroup.js
 * @overview Command Group Implementation
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */
/* global modules */

import Command from './command';

/**
 * A Group of Commands.
 */
export default class CommandGroup extends Command {
  /**
   * Creates a new Command Group.
   *
  * @constructor
  * @param {string|string[]} trigger - One or multiple triggers for the command (typed in the chat)
  * @param {Object} [options] - The options for that Command
  * @param {Array<Array<string>>} [options.parameters] - Command parameters
  * @param {function (player: Player): bool} [options.validator] - Command validator
  * @param {function(player: Player, ..args: *): boolean} handler - The command handler
   */
  constructor(trigger, options) {
    super(trigger, shared.utility.concat(options, { skipUsage: true }), () => { });
    /** @type {function} */
    this._handler = this.handle;

    // Override parameters
    this.options.parameters = [];

    /** @type {Set<Command>} */
    this.commands = new Set();

    this.generateUsageMessage();
  }

  /**
   * Adds a Command to the Group.
   *
   * @param {Command} command - The command that should be added.
   *
   * @returns {CommandGroup} this
   */
  add(command) {
    if (!this.commands.add(command)) {
      log.warn(`Could not add Command "${command.trigger}"`);
    }
    command.options.parent = this;
    command.generateUsageMessage();

    return this;
  }

  /**
   * Genereates the Usage Message for the Command Group.
   */
  generateUsageMessage() {
    super.generateUsageMessage();

    // Regenerate sub-command usage messages
    this.commands.forEach(command => {
      command.generateUsageMessage();
    });
  }

  /**
   * Handles the Command.
   *
   * @param {...*} args - Command arguments.
   */
  handle(...args) {
    // args[0] = player
    // args[1] = subCommand
    if (args.length < 2 && !(args.length === 1 && args[1] === 'help')) {
      log.warn('Internal issue: command group received less than 2 arguments');
    }

    const player = args[0];

    if (args.length === 1 || args[1] === 'help') {
      // TODO: Clean this up, tidy this up

      const availableCommands = new Set();
      const unavailableCommands = new Set();

      this.commands.forEach(command => {
        if (command.options.validator(player)) {
          availableCommands.add(command);
        } else {
          unavailableCommands.add(command);
        }
      });

      // Generate Help message
      let cmdGroupName = '/';
      if (this.trigger.size > 1) {
        cmdGroupName = `${cmdGroupName}[`;
      }
      this.trigger.forEach(trigger => {
        cmdGroupName = `${cmdGroupName}${trigger}|`;
      });
      cmdGroupName = cmdGroupName.slice(0, cmdGroupName.length - 1);
      if (this.trigger.size > 1) {
        cmdGroupName = `${cmdGroupName}]`;
      }

      player.sendMessage(`Help for "${cmdGroupName}"`);

      const buildTable = set => {
        let tempStr = '<table><tr>';
        let i = 1;
        set.forEach(command => {
          const usage = command.usage.slice(7);
          tempStr = `${tempStr}<td>${usage}</td>`;

          if (i % 3 === 0) {
            tempStr = `${tempStr}</tr><tr>`;
          }
          i++;
        });
        tempStr = `${tempStr}</tr></table>`;

        return tempStr;
      };

      if (unavailableCommands.size > 0) {
        player.sendMessage('<br />Unavailable:');
        player.sendMessage(buildTable(unavailableCommands), vrp.config.game.color.message.unavailableCommand);
      }

      player.sendMessage(`Available Commands:<br />${buildTable(availableCommands)}`);
      return undefined;
    }

    const subCommand = args[1];

    let command;
    for (const tempCommand of this.commands.values()) {
      if (tempCommand.trigger.has(subCommand)) {
        command = tempCommand;
        break;
      }
    }


    if (typeof command === 'undefined') {
      player.sendMessage(this.usage);
      return undefined;
    }

    const argArray = [];
    if (args.length > 2) {
      for (let i = 2; i < args.length; i++) {
        argArray.push(args[i]);
      }
    }
    return command.execute(player, argArray);
  }
}
