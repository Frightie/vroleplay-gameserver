/**
 * @title baseManager.js
 * @overview Manager Implementation (Base Class)
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

/**
 * Manages a certain 'thing' and holds it in a Set.
 */
export default class Manager {
  /**
   * Creates a new Manager
   *
   * @param {string} manages the key of what the Manager manages
   * @param {[*]} [data] the existing data
   */
  constructor(manages, data = []) {
    this[manages] = new Set(data);

    // create a reference so we know where things are stored
    /** @type {Set<*>} */
    this.set = this[manages];
  }

  /**
   * Adds an item to the Manager
   *
   * @param {*} item the item to add
   */
  add(item) {
    return this.set.add(item);
  }

  /**
   * Returns whether the given item exists in the manager
   *
   * @param {*} item the item
   * @returns {boolean} whether the item exists or not.
   */
  exists(item) {
    return this.set.has(item);
  }

  /**
   * Returns whether the given item exists in the manager
   *
   * @param {*} item the item
   * @returns {boolean} whether the item exists or not.
   */
  has(item) {
    return this.set.has(item);
  }

  /**
   * Removes an item from the manager
   *
   * @param {*} item the item to remove
   */
  remove(item) {
    return this.set.delete(item);
  }
}
