/**
 * @title events.js
 * @overview Package Events File
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

export default class Events {
  /**
   * Registers all Events
   *
   */
  static register() {
    events.Add('ClientConnected', Events.onClientConnected);

    events.Add('PlayerCreated', Events.onCreated);
    events.Add('PlayerDestroyed', Events.onDestroyed);
    events.Add('PlayerDeath', Events.onDeath);

    log.success('Registered Player Events.');
  }

  /**
   * Called when a new client connects to the server
   *
   * @param {Client} client the new client
   */
  static onClientConnected(client) {
    log.debug(`player.events.onClientConnected(id: ${client.networkId})`, 4);
    // Get IP Bans
    vrp.database.Ban.first({ ip: client.ipAddress, expiration: '>(SELECT CURRENT_TIMESTAMP())' }, ban => {
      if (typeof ban === 'undefined') {
        return;
      }

      log.print(`Banned IP ${client.ipAddress} tried to connect to the server.`);
      client.Kick('You are banned from the server.');
    });
  }

  /**
   * Called when a new player gets created (when he connects)
   *
   * @param {Player} player the new player.
   */
  static onCreated(_player) {
    // FIXME: remove me later
    _player.stats.flyingAbility = 100;

    log.debug(`player.events.onCreated(name: "${_player.name}", netId: ${_player.networkId})`, 4);
    console.time('HandleCreate');
    const player = new global.player.CPlayer(_player);

    if (!global.player.manager.add(player)) {
      log.implement('Player fallback routine');
      throw new ScriptError('Player Collection broken!');
    }

    vrp.api.incr('player:count');

    // send the welcome message and freeze the player
    player.sendMessage('Welcome to V: Role Play (v-rp.com)!', vrp.config.game.color.green.light);
    setTimeout(() => {
      player.ToggleControls(false, false);

      const cam = player.graphics.CreateCamera();
      cam.active = true;

      player.stats.stamina = 0;

      const random = Math.floor(Math.random() * vrp.config.game.position.loginScreen.length);
      // has to be set near the camera position so the scene gets loaded
      player.position = vrp.config.game.position.loginScreen[random].position;

      cam.position = vrp.config.game.position.loginScreen[random].position;
      cam.pointAtCoord = vrp.config.game.position.loginScreen[random].lookAt;
      cam.fov = 40.0;

      player.graphics.RenderCameras(true);
      player.loginCamera = cam;

      const loginUI = player.uiManager.create(vrp.config.ui.login);

      loginUI.on('submit', (uiPlayer, data) => {
        log.debug('Validating Login');
        uiPlayer.validateLogin(data.username, data.password, result => {
          loginUI.invoke('loginResult', result);
        });
      });
    }, 500);
    console.timeEnd('HandleCreate');
  }

  /**
   * Called when a Player dies.
   *
   * @param {Player} player the player that just died.
   */
  static onDeath(player) {
    log.debug(`player.events.onPlayerDeath(name: "${player.name}")`, 4);
    // Extend the player with our own class (important)

    // Reset Stats
    player.vstats.forEach(stat => {
      stat.reset();
    });

    // Also, let's save the player
    player.save();
  }

  /**
   * Called when a player gets destroyed (disconnects)
   *
   * @param {Player} player the new player.
   */
  static onDestroyed(player) {
    log.debug(`player.events.onDestroyed(name: "${player.name}", netId: ${player.client.networkId})`, 4);
    console.time('HandleDestroy');

    // Extend the player with our own class (important)
    try {
      log.debug(`onDestroyed Removing Player: ${global.player.manager.remove(player)}`);

      log.debug('Trying to save player');

      if (player.character !== null) {
        player.character.last_pos = player.position;
      }

      try {
        player.save(true, () => {
          log.debug('player saved');
          player.destroy();
        });
      } catch (e) {
        player.destroy();
      }
    } catch (e) {
      log.warn(`Could not save user: ${e}`);
    }

    vrp.api.decr('player:count');

    console.timeEnd('HandleDestroy');
  }
}
