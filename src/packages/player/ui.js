/**
 * @title ui.js
 * @overview User Interface
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

/**
 * The UserInterface class.
 */
export default class UI {
  /**
   * Creates a new UI
   *
   * @param {Object} info - Info about the UI
   * @param {string} info.name - Name of the UI (ex: vrp-login)
   * @param {Array<string>} info.receives - Array of Events that can be received by the UI
   * @param {Array<string>} info.sends - Array of Events that can be recieved from the UI
   * @param {Player} player - The Player the UI should be created for.
   * @param {function(ui: UI)} done - Callback when the UI has been created.
   */
  constructor(info, player, done) {
    /** @type {string} */
    this.name = info.name;

    /** @type {Array<string>} */
    this.receives = info.receives;

    /** @type {Array<string>} */
    this.sends = info.sends;

    /** @type {boolean} */
    this.created = false;

    /** @type {boolean} */
    this.destroyed = false;

    /** @type {Map<string, Set<function(args: ...*)>} */
    this.handlers = new Map();

    /** @type {CPlayer} */
    this.player = player;

    player.invokeCommand('create_ui', JSON.stringify({ name: this.name, sends: this.sends }));

    events.Add('RemoteCommand', (evPlayer, command, strData) => {
      if (this.destroyed) {
        return;
      }

      if (evPlayer.client.networkId === player.client.networkId) {
        if (!this.created && command === 'ui_created' && strData === this.name) {
          this.created = true;
          done(this);
          this.invoke('created');
          return;
        } else if (this.created && command === 'ui_destroyed' && strData === this.name) {
          this.destroyed = true;
          this.invoke('destroyed');
        }

        if (command === this.name) {
          let data;
          try {
            data = JSON.parse(strData);
          } catch (e) {
            log.warn(`Could not parse RemoteCommand(${command} - ${strData}): ${e}`);
            return;
          }

          if (!this.handlers.has(data.event)) {
            return;
          }

          const handlers = this.handlers.get(data.event);
          const execHandler = handler => handler(player, data.data);

          for (let i = 0; i < this.sends.length; i++) {
            if (this.sends[i] === data.event) {
              handlers.forEach(execHandler);
              return;
            }
          }
          log.warn(`Weird Error in ${data.event} for UI ${command}`);
        }
      }
    });
  }

  /**
   * Invokes a Command to the UI
   *
   * @param {string} event - Name of the Event.
   * @param {*} data - Event Data.
   */
  invoke(event, data) {
    if (!this.created) {
      return false;
    }

    log.debug(`UI(${this.name} invoke('${event}'))`, 3);
    this.player.refresh().invokeCommand(this.name, JSON.stringify({ event, data }));
  }

  /**
   * Adds a Handler for an Event
   *
   * @param {string} event - Event Name.
   * @param {function(player: Player, data: Object)} handler - Handler Function.
   */
  on(event, handler) {
    let exists = false;
    for (let i = 0; i < this.sends.length; i++) {
      if (this.sends[i] === event) {
        exists = true;
        break;
      }
    }

    if (!exists) {
      log.warn(`Trying to add Event ${event} to UI ${this.name} - it doesnt exist in 'sends'.`);
      return;
    }

    if (!this.handlers.has(event)) {
      this.handlers.set(event, new Set());
    }

    this.handlers.get(event).add(handler);
  }
}
