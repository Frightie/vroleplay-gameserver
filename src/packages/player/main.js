/**
 * @title main.js
 * @overview Main Package File
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

// Predefined V:RP Header to install our own error types & handler
import error from '../../shared/error';
import errorHandler from '../../shared/errorHandler';
error.install(global);
process.on('uncaughtException', errorHandler);

import Package from '../../shared/classes/package';
import CPlayer from './player';
import _playerManager from './playerManager';

class PlayerSystem extends Package {
  /**
   * Constructs the Package
   * @constructor
   */
  constructor() {
    super('player');
    this.CPlayer = CPlayer;
  }

  /**
   * Starts the Package
   */
  start() {
    this.manager = _playerManager;
    super.start();
  }

  /**
   * Returns the Dependencies
   * @returns {[string]}
   */
  get dependencies() {
    return ['api', 'config', 'chat', 'database', 'permission'];
  }

  /**
   * Registers all Permissions. Static Permissions are returned as string array,
   * dynamic permissions can be created using vrp.permission.create.
   */
  registerPermissions() {
    return [];
  }

  receiveDependencies(deps) { super.receiveDependencies(global, deps); }
  receiveVRPNamespace(namespace) { super.receiveVRPNamespace(global, namespace); }
}

// Register globals
global.package = new PlayerSystem();
global[global.package.name] = global.package;
