/**
 * @title commands.js
 * @overview Implements Package Commands
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

/**
  * @namespace
  */
const Commands = {};
export default Commands;

const blockable = {
  pm: 'PM',
};

Commands.block = new shared.Command('block', {
  parameters: [
    ['player', 'target'],
    ['type', 'string'],
  ],
}, (player, target, _type) => {
  const type = _type.toLowerCase();
  if (!blockable.hasOwnProperty(type)) {
    player.sendError(`Invalid block type ${_type}. Available Types:`);
    let msg = '';
    for (const idx in blockable) {
      if (blockable.hasOwnProperty(idx)) {
        msg += `${idx}, `;
      }
    }
    player.sendError(msg.substr(0, msg.length - 2));
    return;
  }

  if (player.blocked.has(type) && player.blocked.get(type).has(target.character.id)) {
    player.sendError(`${target.character.name} is already blocked.`);
    return;
  }

  if (!player.blocked.has(type)) {
    player.blocked.set(type, new Set());
  }

  player.blocked.get(type).add(target.character.id);
  player.sendSuccess(`Blocked ${type} for player ${target.character.name}.`);
});

Commands.login = new shared.Command('login', {
  parameters: [
    ['Account Name', 'string'],
    ['Password', 'text'],
  ],
  validator: player => {
    if (player.loggedIn) {
      return 'You are already logged in.';
    }
    if (player.authCode !== 0) {
      return 'Two-factor authorization is active. Please enter the authentication code (/vauth).';
    }
  },
}, (player, accountName, password) => {
  player.validateLogin(accountName, password, result => {
    switch (result) {
    case shared.enum.LoginResult.Success:
      player.sendSuccess('You successfully logged in.');
      break;
    case shared.enum.LoginResult.ErrorInternal:
      player.sendError('An internal error occured.');
      break;
    case shared.enum.LoginResult.ErrorLoginDisabled:
      let restriction = vrp.server.loginRestriction;
      if (vrp.server.loginRestriction instanceof vrp.admin.AccessLevel) {
        restriction = `${vrp.admin.levels[restriction].name} and higher only`;
      }
      player.sendError(`Login is currently disabled. Current Restriction: ${restriction}`);
      break;
    case shared.enum.LoginResult.ErrorUnknownAccount:
    case shared.enum.LoginResult.ErrorWrongPassword:
      player.sendError('Account and/or password wrong.');
      break;
    case shared.enum.LoginResult.ErrorBanned:
      player.sendError('Your account or IP is currently banned.');
      break;
    case shared.enum.LoginResult.ErrorAuthName:
      player.sendError('Could not resolve your Slack name. No login for you.');
      break;
    case shared.enum.LoginResult.SuccessWaitForAuth:
      player.sendInfo('Please enter the authentication code. (/vauth)');
      break;
    default:
      player.sendError(`Unknown error (${result})`);
      break;
    }
  });
});

Commands.kickMe = new shared.Command(['kickme', 'kickmyass'], {}, player => {
  player.client.Kick('As you wish, milord.');
});

Commands.selectCharacter = new shared.Command('selchar', {
  parameters: [
    ['Character ID', 'integer'],
  ],
  validator: player => {
    return player.loggedIn && !player.playing;
  },
}, (player, id) => {
  if (id < 0 || player.availableCharacters.length < id) {
    return player.sendError('Invalid Character ID.');
  }

  if (player.selectCharacter(player.availableCharacters[id])) {
    return player.sendSuccess(`You are now playing as ${player.character.name}.`);
  }
  return player.sendError('Internal Error setting your character.');
});

Commands.toggle = new shared.Command('toggle', {
  parameters: [
    ['target', 'string'],
    ['on/off', 'string', 'toggle'],
  ],
}, (player, target, toggle) => {
  const targets = {
    ooc: 'CAN_SEE_OOC_CHAT',
    pm: 'CAN_SEE_PMs',
  };

  if (!targets.hasOwnProperty(target)) {
    player.sendError(`Invalid target '${target}'. Available Targets:`);
    let msg = '';
    for (const idx in targets) {
      if (targets.hasOwnProperty(idx)) {
        msg += `${idx}, `;
      }
    }
    if (msg.length > 2) {
      msg = msg.slice(0, msg.length - 2);
    }

    player.sendError(msg);
    return;
  }

  let newState = 'disabled';
  if ((toggle === 'off' || toggle === 'toggle') && player.permissions.has(targets[target])) {
    if (!player.permissions.remove(targets[target])) {
      player.sendError('Internal Error: could not remove permission.');
      log.error(`Could not remove Permission '${targets[target]}' from ${player.character.name}`);
    }
  } else {
    if ((toggle === 'on' || toggle === 'off') && !player.permissions.has(targets[target])) {
      if (!player.permissions.add(targets[target])) {
        player.sendError('Internal Error: could not add permission.');
        log.error(`Could not add Permission '${targets[target]}' to ${player.character.name}`);
      }
    }
    newState = 'enabled';
  }
  player.sendSuccess(`${target} is now ${newState}`);
});

Commands.unblock = new shared.Command('unblock', {
  parameters: [
    ['player', 'target'],
    ['type', 'string'],
  ],
}, (player, target, _type) => {
  const type = _type.toLowerCase();
  if (!blockable.hasOwnProperty(type)) {
    player.sendError(`Invalid block type ${_type}. Available Types:`);
    let msg = '';
    for (const idx in blockable) {
      if (blockable.hasOwnProperty(idx)) {
        msg += `${idx}, `;
      }
    }
    player.sendError(msg.substr(0, msg.length - 2));
    return;
  }

  if (!player.blocked.has(type) || !player.blocked.get(type).has(target.character.id)) {
    player.sendError(`${target.character.name} is not blocked.`);
    return;
  }

  player.blocked.get(type).delete(target.character.id);
  player.sendSuccess(`Unblocked ${type} for player ${target.character.name}.`);
});

Commands.staffAuthentication = new shared.Command('vauth', {
  parameters: [
    ['Authentication Code', 'integer'],
  ],
  validator: player => {
    return player.account !== null && player.authCode !== 0 && !player.playing;
  },
  permission: ['ENFORCE_2FA'],
}, (player, authCode) => {
  const result = player.validateAuthCode(authCode);
  switch (result) {
  case shared.enum.LoginResult.SuccessAuth:
    player.sendSuccess('Successfully logged in.');
    break;
  case shared.enum.LoginResult.ErrorWrongAuthCode:
    player.sendError('The authentication code is invalid.');
    break;
  default:
    player.sendError(`Unknown error (${result})`);
    break;
  }
});
