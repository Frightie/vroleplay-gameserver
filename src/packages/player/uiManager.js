/**
 * @title uiManager.js
 * @overview Manages all UIs for a Player
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

import BaseManager from '../../shared/classes/baseManager';
import UI from './ui';

/**
 * The UIManager manages all User Interfaces for a Player.
 */
export default class UIManager extends BaseManager {
  /**
   * Creates the UIManager
   *
   * @param {Player} player - The UIManager's associated Player.
   */
  constructor(player) {
    super('interfaces');
    /** @type {boolean} */
    this.initialized = false;

    /** @type {Set<UI>} */
    this.interfaces = this.interfaces;

    /** @type {Player} */
    this.player = player;
  }

  /**
   * Initializes the UI Manager
   *
   */
  init() {
    this.initialized = true;
  }

  /**
   * Creates a new Interface.
   *
   * @param {Object} info - Info about the UI
   * @param {string} info.name - Name of the UI (ex: vrp-login)
   * @param {Array<string>} info.receives - Array of Events that can be received by the UI
   * @param {Array<string>} info.sends - Array of Events that can be recieved from the UI
   * @param {function(ui: UI)} [done=function()] - Callback when the UI has been created.
   *
   * @returns {undefined|UI} undefined if the UI can't be created, a handle to the UI otherwise
   */
  create(info, done = () => {}) {
    let exists = false;
    this.interfaces.forEach(ui => {
      if (ui.name === info.name) {
        exists = true;
      }
    });
    if (exists) {
      return undefined;
    }

    const ui = new UI(info, this.player.refresh(), done);

    if (!this.add(ui)) {
      return undefined;
    }

    return ui;
  }

  /**
   * Gets a UI by its Name
   *
   * @param {string} name - The Name of the UI
   *
   * @returns {undefined|UI} Handle to the UI
   */
  get(name) {
    let ui;
    this.interfaces.forEach(tempUI => {
      if (tempUI.name === name) {
        ui = this;
      }
    });

    return ui;
  }
}
