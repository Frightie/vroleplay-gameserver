/**
 * @title playerManager.js
 * @overview Player Manager Implementation
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

import BaseManager from '../../shared/classes/baseManager';

/**
 * PlayerManager Class
 *
 * This Class is responsible for Managing all connected Players. It associates
 * the GTA:MP Player with the internal V:RP Player. It also contains various
 * utility functions.
 */
class PlayerManager extends BaseManager {
  /**
   * Creates the Player Manager
   *
   * @constructor
   */
  constructor() {
    super('players');
    /** @type {boolean} */
    this.initialized = false;
    /** @type {Set<Player>} */
    this.players = this.players;
  }

  /**
   * Initializes the Faction manager
   *
   * @this {PlayerManager}
   */
  init() {
    log.print('Initializing Player Manager');

    events.Add('find player', (data, cb) => {
      cb(this.findPlayer(data));
    });

    events.Add('time: hour', () => {
      this.players.forEach(player => {
        if (player.playing) {
          const timeSinceLastPaycheck = player.character.playtime - player.character.last_paycheck;
          player.debug(`You'll receive ${Math.round(timeSinceLastPaycheck / 60 * 100)}% of the paycheck because you played ${timeSinceLastPaycheck} minutes since the last one.`);
          log.implement('Give Paycheck');
          player.character.last_paycheck = player.character.playtime;
          player.character.save();
        }
      });
    });
    events.Add('time: minute', () => {
      this.players.forEach(player => {
        if (player.playing) {
          player.debug(`Playtime: ${player.character.playtime}`);
          player.character.playtime++;
          player.minutesPlayed++;
        }
      });
    });


    const fnDestroyPlayer = instance => {
      // Get the related player
      let player;
      if (this.model.name === 'Account') {
        for (const tempPlayer of this.players.values()) {
          if (tempPlayer.account === instance) {
            player = tempPlayer;
            break;
          }
        }
      } else {
        (() => {
          for (const tempPlayer of this.players.values()) {
            for (let i = 0; i < tempPlayer.availableCharacters.length; i++) {
              if (tempPlayer.availableCharacters[i] === instance) {
                player = tempPlayer;
                return true;
              }
            }
          }
        })();
      }

      if (typeof player === 'undefined') {
        log.warn('Unknown Player destroyed');
        return;
      }

      player.debug('You have been deleted, sucker.');
      player.client.Kick('Internal Error');
      this.remove(player);
    };
    vrp.database.Account.destroyHandlers.push(fnDestroyPlayer);
    vrp.database.Character.destroyHandlers.push(fnDestroyPlayer);

    this.initialized = true;
  }

  /**
   * Adds a player to the Manager
   *
   * @param {Player} player the player to add
   */
  add(player) {
    const success = super.add(player);
    if (success) {
      events.Call('player.manager PlayerAdded', player);
    }
    return success;
  }

  /**
   * Broadcasts a message.
   *
   * @this {PlayerManager}
   * @param {string} Message
   * @param {RGB} color
   */
  broadcast(message, color = new RGB(255, 255, 255)) {
    for (const player of this.players.values()) {
      player.sendMessage(message, color);
    }
  }

  /**
   * Returns the player from his id or (part of his) Name
   *
   * @param  {string/number} idOrName Networkid or name of the player (or some digits of the name)
   * @param  {boolean=} [allowDuplicates=false] False: If multiple players have the same Name only the first one found is returned.
   *                                            True: Returns an array with all duplicate players with the name
   * @param  {boolean=} [caseSensitive=false] True if case sensitive, false if not
   * @return {[CPlayer]} An array with the players found with the id or the name,
   *                  only contains the first one found if allowDuplicates was false, empty array if no player was found
   */
  findPlayer(idOrName, allowDuplicates = false, caseSensitive = false) {
    const id = parseInt(idOrName, 10);
    let fnCheck;
    let targetInfo = idOrName;

    if (isNaN(id)) {
      if (!caseSensitive) {
        targetInfo = idOrName.toLowerCase();
      }

      fnCheck = target => {
        if (target.character === null) {
          return false;
        }
        let targetName;
        if (!caseSensitive) {
          targetName = target.character.name.toLowerCase();
        } else {
          targetName = target.character.name;
        }
        if (targetName.indexOf(targetInfo) === 0) {
          return true;
        }
        return false;
      };
    } else {
      fnCheck = target => target.client.networkId === id;
    }

    const playerArray = [];
    for (const tempPlayer of this.players.values()) {
      if (fnCheck(tempPlayer)) {
        playerArray.push(tempPlayer);
        if (!allowDuplicates) {
          // exit the loop, because we just return the first player found
          break;
        }
      }
    }
    return playerArray;
  }

  /**
   * Removes a Player
   *
   * @param {Player} player - Targeted player.
   *
   * @returns {boolean}
   */
  remove(player) {
    let finalPlayer;
    this.players.forEach(tempPlayer => {
      if (tempPlayer.client.networkId === player.client.networkId) {
        finalPlayer = tempPlayer;
      }
    });

    return super.remove(finalPlayer);
  }

  /**
   * Sends a message based on the distance to a certain point
   *
   * @param {string} message the message to send
   * @param {Player|Vector3f} origin the origin of the message
   * @param {float} maxDistance maximum distance to have the message sent
   * @param {Object} [options = {}] Options for the message:
   *                               	color: RGB (will use this instead of dynamic gray tone)
   *                               	dropStart: float (offset before color changes)
   *                               	minValue: integer (minimum gray tone)
   */
  sendRangedMessage(message, origin, maxDistance, options = {}) {
    const messageOptions = shared.utility.concat(options, {
      dropStart: 0.0,
      minValue: 128,
    });

    if (!shared.utility.isVector3(origin)) {
      return;
    }

    let distance;

    for (const player of this.players.values()) {
      distance = shared.utility.distance(player.position, origin);

      if (distance > maxDistance) {
        return;
      }

      if (typeof options.color !== 'undefined') {
        player.sendMessage(message, messageOptions.color);
      } else {
        player.sendMessage(message, shared.utility.getColorFromDistance(distance,
                                    maxDistance, messageOptions.dropStart, messageOptions.minValue));
      }
    }
  }
}

/**
 * PlayerManager instance
 */
export default new PlayerManager();
