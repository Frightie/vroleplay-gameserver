/**
 * @title player.js
 * @overview Extended Player Class
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

import  bcrypt from 'bcryptjs';
import UIManager from './uiManager';

/**
 * V:RP Player Class
 *
 * This class is the representation of a Player in V:RP.
 * It is merged with the GTA:MP player (see Player~patch).
 */
export default class Player {
  /**
   * Creates a new Player
   *
   * @class
   * @this {Player}
   * @param {Player} player the GTA:MP player instance
   */
  constructor(player) {
    log.debug(`Constructing new Player based for ${player.name}`, 3);
    console.time('Player Initialization');

    // GTA:MP stubs
    /** @type {Vector3f} */
    this.position = new Vector3f(0.0, 0.0, 0.0);
    this.client = {};

    /** @type {boolean} */
    this.loggedIn = false;
    /** @type {int} */
    this.authCode = 0;
    /** @type {boolean} */
    this.playing = false;
    /** @type {boolean} */
    this.firstConnection = false;

    /** @type {Set<database.Boost>} */
    this.boosts = new Set();
    /** @type {Map} */
    this.timeouts = new Map();
    /** @type {Map} */
    this.intervals = new Map();
    /** @type {Map<string, Set>} */
    this.blocked = new Map();
    /** @type {StorageContainer} */
    this.storages = new vrp.item.StorageContainer();
    /** @type {Map<string, shared.Stat>} */
    this.vstats = new Map();
    /** @type {Set<VRPVehicle>} */
    this.vehicles = new Set();
    /** @type {PermissionContainer} */
    this.permissions = vrp.permission.createContainer(false, () => [this]);
    /** @type {UIManager} */
    this.uiManager = new UIManager(this);

    /** @type {Camera} */
    this.loginCamera = {};
    /** @type {AccessLevel} */
    this._accessLevel = vrp.admin.levels.Player;
    /** @type {Object} */
    this.radioChannel = {
      frequency: null,
      key: '',
    };
    /** @type {integer} */
    this.minutesPlayed = 0;
    /** @type {Weapon} */
    this.vrpEquippedWeapon = null;

    // copy over the client object
    /** @type {Object} */
    this.client = player.client;

    /* DB Related */
    /** @type {Array<database.Character>} */
    this.availableCharacters = [];
    /** @type {database.Account} */
    this.account = null;
    /** @type {database.Character} */
    this.character = null;
    /** @type {Faction} */
    this.faction = null;

    /** @type {Rank} */
    this.factionRank = null;
    /** @type {Set<Faction>} */
    this.pendingFactionInvites = new Set();
    /** @type {Array<vrp.database.Ban>} */
    this.bans = [];

    player.patched = false;

    this.patch(player);

    this.setPermissions();
    this.setupWorld(vrp.server.world);

    console.timeEnd('Player Initialization');
  }

  /**
   * Patches a vrp.entity.Player (this) with a GTA:MP player
   *
   * @this {Player}
   * @param {Player} player the GTA:MP player we want to patch into *this*
   */
  patch(player) {
    console.time(`Patching Player ${player.name}`);
    log.debug(`Patching Player ${player.name}`, 4);
    if (player.patched) {
      log.debug(`Player ${player.name} has been patched already.`, 4);
      return;
    }

    const self = this;

    const fnRegisterProperty = prop => {
      Object.defineProperty(self, prop, {
        get() { return player[prop]; },
        set(value) { player[prop] = value; },
      });
    };

    // Write player properties and functions into the cplayer instance
    const added = new Set();
    for (const property in player) {
      if (typeof self[property] === 'undefined') {
        // Applying is enough for functions
        if (typeof player[property] === 'function') {
          self[property] = (...args) => {//eslint-disable-line
            return player[property](...args);
          };
        } else {
          // Thanks stackoverflow
          fnRegisterProperty(property);
        }
        added.add(property);
      }
    }

    const fnRegisterProp = prop => {
      Object.defineProperty(player, prop, {
        get() { return self[prop]; },
        set(value) { self[prop] = value; },
      });
    };

    // Write our properties and functions into the player.
    let props = Object.getOwnPropertyNames(Object.getPrototypeOf(self));
    props = props.concat(Object.getOwnPropertyNames(self));
    for (let i = 0; i < props.length; i++) {
      const property = props[i];
      if (added.has(property)) {
        log.debug(property);
        continue;
      }
      if (property === 'constructor' || property === 'client' || property === 'position') {
        continue;
      }

      if (typeof player[property] === 'undefined') {
        // Applying is enough for functions
        if (typeof self[property] === 'function') {
          player[property] = (...args) => {//eslint-disable-line
            return self[property](...args);
          };
        } else {
          // Thanks stackoverflow
          fnRegisterProp(property);
        }
      } else {
        log.warn(`Property ${property} already exists in the player!`);
      }
    }

    player.patched = true;
    log.debug(`Player ${player.name} patched.`, 4);
    console.timeEnd(`Patching Player ${player.name}`);
  }

  /**
   * Gets a fresh GTA:MP Player and patches it. Use it when you crash when calling a GTA:MP Player Function.
   */
  refresh() {
    /*const players = g_players;//eslint-disable-line
    for (let i = 0; i < players.length; i++) {
      if (players[i].client.networkId === this.client.networkId) {
        return global.player.manager.getPlayerFromPlayer(players[i]);
      }
    }*/
    return this;
  }

  /**
   * Accepts a Faction invitation
   *
   * @param {Faction} faction the faction to join
   */
  acceptFactionInvitation(faction) {
    if (!this.permissions.has('CAN_JOIN_FACTION') || this.faction !== null) {
      return this.sendError('You are not allowed to join a faction.');
    }

    this.faction = faction.invitationAccepted(this);
    if (this.faction === null) {
      return this.sendError('Could not join the faction.');
    }
    this.sendSuccess(`You joined the Faction "${faction.name}".`);
  }

  /**
   * @type {AccessLevel}
  */
  get accessLevel() {
    return this._accessLevel;
  }

  /**
   * Sets the AccessLevel and re-assigns permissions.
   *
   * @type {AccessLevel}
   */
  set accessLevel(level) {
    this.accessLevel.permissions.unapplyPermissions(this.permissions);
    this._accessLevel = level;
    this.accessLevel.permissions.applyPermissions(this.permissions);
  }

  /**
   * Applies a Boost (StatModifier over time)
   *
   * @param {string} stat stat name
   * @param {function (currentValue: float, function(value: float), Set<database.Boost>)} fn the function that will handle applying and unapplying the modification
   */
  applyBoost(stat, fn) {
    if (typeof this.stats[stat] !== 'undefined') {
      fn(this.stats[stat], value => {
        if (this.stats[stat] + value > 100) {
          const diff = 100 - this.stats[stat];
          this.stats[stat] = 100;
          return diff;
        } else if (this.stats[stat] + value < 0) {
          const diff = 0 - this.stats[stat];
          this.stats[stat] = 0;
          return diff;
        }
        this.stats[stat] += value;
        return value;
      }, this.boosts);
      return true;
    }
    if (!this.vstats.has(stat)) {
      log.debug(`Cannot apply stat modification: unknown stat '${stat}'`);
      return false;
    }

    fn(this.vstats.get(stat).value, value => {
      const s = this.vstats.get(stat);
      if (s.value + value > s.maxValue) {
        const diff = s.maxValue - s.value;
        s.value = s.maxValue;
        return diff;
      } else if (s.value + value < s.minValue) {
        const diff = s.minValue - s.value;
        s.value = s.minValue;
        return diff;
      }
      s.value += value;
      return value;
    }, this.boosts);
  }

  /**
   * Applies a stat modification
   *
   * @param {string} stat the stat name
   * @param {function (float) : float} modFn modification function (applies)
   */
  applyStatModifier(stat, modFn) {
    if (typeof this.stats[stat] !== 'undefined') {
      let newValue = modFn(this.stats[stat]);
      if (stat !== 'money') {
        let cut = 0;
        if (newValue > 100) {
          cut = newValue - 100;
        } else if (newValue < 0) {
          cut = 0 + newValue;
        }
        if (cut !== 0) {
          shared.Boost.manageBoosts(this.boosts, stat, cut);
          if (cut > 0) {
            newValue = 100;
          } else {
            newValue = 0;
          }
        }
      }
      this.stats[stat] = newValue;
      this.character.stats[stat] = this.stats[stat];
      return true;
    }
    if (!this.vstats.has(stat)) {
      log.debug(`Cannot apply stat modification: unknown stat '${stat}'`);
      return false;
    }

    let newValue = modFn(this.vstats.get(stat));
    let cut = 0;
    if (newValue > this.vstats.get(stat).maxValue) {
      cut = newValue - this.vstats.get(stat).maxValue;
    } else if (newValue < this.vstats.get(stat).minValue) {
      cut = this.vstats.get(stat).minValue + newValue;
    }
    if (cut !== 0) {
      shared.StatModifier.manageBoosts(this.boosts, stat, cut);
      if (cut > 0) {
        newValue = this.vstats.get(stat).maxValue;
      } else {
        newValue = this.vstats.get(stat).minValue;
      }
    }
    return this.vstats.get(stat).applyModifier(modFn);
  }

  /**
   * Creates the player stats (fills the Map with infos)
   */
  createStats() {
    if (this.character === null) {
      throw new ScriptError(`CreateStats called with invalid character for player ${this.name}`);
    }

    // Updates the character stat
    const update = (statName, stat) => {
      // get unboosted value
      let unboostedValue = stat.value;
      this.boosts.forEach(boost => {
        if (boost.stat === stat.identifier) {
          unboostedValue -= boost.value;
        }
      });
      this.character.stats[statName] = unboostedValue;
    };

    this.vstats.set('hunger', new shared.VitalStat('hunger', 'Hunger', 'The umm... hunger? Like... being hungry, you know', 0, 100, 30 * 60 * 1000, {
      hooks: {
        onChange: (stat, oldValue) => {
          this.debug(`Hunger changed to ${stat.value}.`);
          update('hunger', stat);

          switch (stat.value) {
          case 20:
            this.sendMessage('You feel dizzy and your stomach grumbles...', vrp.config.game.color.message.vitalStat);
            break;
          case 10:
            this.sendMessage('You are so hungry that it hurts', vrp.config.game.color.message.vitalStat);
            break;
          case 0:
            if (oldValue === 1) {
              this.sendMessage('You feel like you could pass out any second.', vrp.config.game.color.message.vitalStatCritical);
            }
            break;
          default:
            break;
          }

          if (stat.value <= 10 && stat.value > 0) {
            this.health -= 5;
          } else if (stat.value === 0) {
            if (this.health - 8 < 0) {
              this.sendMessage('Everything starts to fade...', vrp.config.game.color.message.vitalStatCritical);
              this.health = 0;
            } else {
              this.health -= 8;
            }
          }
        },
      },
    }));

    this.vstats.set('thirst', new shared.VitalStat('thirst', 'Thirst', 'The umm... thirst? Like... being thirsty, you know', 0, 100, 30 * 60 * 1000), {
      hooks: {
        onChange: stat => {
          update('thirst', stat);
        },
      },
    });

    // Get Values from the Database
    this.vstats.forEach(stat => {
      if (typeof this.character.stats[stat.identifier] !== 'undefined') {
        stat.value = this.character.stats[stat.identifier];
      } else {
        stat.value = stat.maxValue;
      }
    });
    for (const stat in this.stats) {
      if (typeof this.character.stats[stat] !== 'undefined' && stat !== 'money') {
        this.stats[stat] = this.character.stats[stat];
      }
    }
  }

  /**
   * Sends an debug message to the player. and prints it in the server console.
   *
   * @param {string} message the message.
   */
  debug(message) {
    log.debug(`Player [${this.name}]: ${message}`);
    return this.sendMessage(`<strong>Debug: </strong>${message}`, vrp.config.game.color.message.debug);
  }

  /**
   * destroys a player (destructor, kill all timers etc)
   */
  destroy() {
    // Remove the player from his faction
    if (this.faction) {
      this.faction.onlineMembers.delete(this.character);
    }

    this.timeouts.forEach(timeout => {
      if (timeout instanceof shared.Timeout) {
        timeout.clear();
      } else {
        clearTimeout(timeout);
      }
    });
    this.intervals.forEach(interval => clearInterval(interval));

    // Kill VitalStat intervals
    this.vstats.forEach(stat => {
      if (stat instanceof shared.VitalStat) {
        stat.destroy();
      }
    });

    // Reset some variables
    this.playing = false;
    this.loggedIn = false;
    this.account = null;
    this.character = null;
    this.availableCharacters = [];
  }

  /**
   * Gets all active bans based on the player account id.
   *
   * @param {function ()} done callback when the bans were fetched.
   */
  getActiveBans(done) {
    vrp.database.Ban.find({ expiration: '>(SELECT CURRENT_TIMESTAMP())', account: this.account.id }, bans => {
      this.bans = bans;
      log.debug(`Player ${this.name} has ${this.bans.length} active bans.`, 3);
      done();
    });
  }

  /**
   * Logs a player in and fetches his characters
   */
  login() {
    if (typeof this.account === 'undefined') {
      return;
    }

    /* For my beloved Ariel

    May 10, 2015. A sunny afternoon.

    jan [4:14 PM]
    BTW
    pls
    no access level 1337
    im gonna cry otherwise

    fright [4:14 PM]
    of course there will be access level 1337

    jan [4:15 PM]
    no

    fright [4:15 PM]
    1337 is the godly number m8

    jan [4:15 PM]
    I'll autoban everyone who joins with accesslevel 1337

    fright [4:15 PM]
    1338 then

    jan [4:15 PM]
    everything that is > 10

    fright [4:15 PM]
    think for a moment
    lets say we create 10 ranks
    then we want to add more
    we will have to push '10' to be 11

    jan [4:15 PM]
    then ill ban everyone over 11
    :smile:
    I'll ban every ariel

    fright [4:16 PM]
    and replace it in the whole script
    BUT
    there is only one cast in which the highest rank will be used
    either by me you or daniel
    so if we ever want to add features to that specific admin rank
    no need to touch any others
    so 1337 it is
    thx
    bye

    jan [4:17 PM]
    its not 1337
    it will never be 1337

    fright [4:17 PM]
    give a better number then

    jan [4:17 PM]
    we're not 11 years old :anguished:
    42
    :smile:

    fright [4:17 PM]
    42?

    jan [4:17 PM]
    42.

    fright [4:17 PM]
    ok
    no problem

    jan [4:17 PM]
    The answer to everything
    because
    42

    fright [4:17 PM]
    ok 42
    im fine with that

    jan [4:18 PM]
    42 is my rank tho
    :laughing:

    fright [4:18 PM]
    your rank is my rank

    jan [4:18 PM]
    no
    my rank is your rank

    fright [4:21 PM]
    so ur rank is 1337

    jan [4:21 PM]
    no
    mine is 42

    fright [4:21 PM]
    and mine is 1337

    jan [4:21 PM]
    :angry:
    */
    if (this.account.access_level === 1337) {
      vrp.server.banIP(this.client.ipAddress, {id: 0, username: 'Almighty Jan'}, -1, 'No 1337 on V:RP.', () => {
        this.client.Kick('bb.');
      });
    }

    // Set the AccessLevel
    const acl = vrp.admin.numberToLevel(this.account.access_level);
    if (typeof acl === 'undefined') {
      log.warn(`Cannot set accessLevel of ${this.account.username}, invalid acl ${this.account.access_level}`);
    } else {
      this.accessLevel = acl;
    }

    this.account.loginAttempt(this, 'success');
    this.loggedIn = true;
    this.authCode = 0;

    log.print(`Player ${this.name} logged in.`);

    if (this.permissions.has('ENFORCE_2FA')) {
      shared.integration.slack.sendInfo(`Staff Member "${this.name}" logged into the game.`, { symbol: 'bell', channel: '#administrators' });
    }

    this.account.getCharacters(characters => {
      this.availableCharacters = characters;
      const charInfo = [];

      for (let i = 0; i < characters.length; i++) {
        log.debug(`Character[${i}] for ${this.account.username}: ${characters[i].name}`, 5);
        this.sendMessage(`[${i}] Character: ${characters[i].name}`);

        const charFaction = vrp.faction.manager.findById(characters[i].faction);

        const faction = (typeof charFaction !== 'undefined') ? charFaction.acronym : 'None';
        charInfo.push({ name: characters[i].name, level: 6, faction});
      }
      this.sendInfo('use /selchar [Character ID] to select a character.');

      // Open the character selection GUI
      const characterSelection = this.uiManager.create(vrp.config.ui.characterSelection, () => {
        characterSelection.invoke('availableCharacters', charInfo);
      });

      characterSelection.on('submit', (uiPlayer, data) => {
        log.debug('Validating Character Selection');

        if (typeof data === 'undefined' || data >= uiPlayer.availableCharacters.length || data < 0) {
          characterSelection.invoke('selectionResult', 0);
          return;
        }

        characterSelection.invoke('selectionResult', uiPlayer.selectCharacter(uiPlayer.availableCharacters[data]));
      });

      log.print(`Player ${this.name} has ${this.availableCharacters.length} available characters.`);
    });
  }

  /**
   * Receive an invitation to a faction by another player.
   *
   * @param {Faction} faction the inviting faction
   * @param {Player} invitingPlayer the player that invites us.
   */
  receiveFactionInvitation(faction, invitingPlayer) {
    if (!this.permissions.has('CAN_JOIN_FACTION') || this.pendingFactionInvites.has(faction)) {
      return;
    }
    if (invitingPlayer.faction !== faction) {
      log.warn(`invitingPlayer.faction !== faction (inviting is: ${invitingPlayer.account.name})`);
      return;
    }
    this.pendingFactionInvites.add(faction);
    this.sendInfo(`You received a faction invite to "${faction.name}" from ${invitingPlayer.character.name}.`);
    this.sendInfo(`Type "/faction join ${faction.acronym}" to accept the invite.`);
  }

  /**
   * Mutes the Player for a given time
   *
   * @param {number} duration in seconds
   */
  mute(duration) {
    if (this.timeouts.has('mute')) {
      clearTimeout(this.timeouts.get('mute'));
    }

    this.timeouts.set('mute', setTimeout(() => {
      this.timeouts.delete('mute');
      this.permissions.add('CAN_WRITE_IN_OOC');
      this.sendInfo('You are not muted anymore. Remember to behave properly in the OOC chat!');
    }, duration * 1000));
    this.permissions.remove('CAN_WRITE_IN_OOC');
  }

  /**
   * Saves the Account & Character of the player
   *
   * @param {boolean} [wait=false] whether we should wait until everything finsihed saving
   * @param {function()} [done] callback
   */
  save(wait = false, done = () => {}) {
    // Update Stats
    if (this.character !== null) {
      for (const stat in this.stats) {
        if (this.stats.hasOwnProperty(stat) && stat !== 'money') {
          this.character.stats[stat] = this.stats[stat];
        }
      }
    }

    if (this.account !== null) {
      if (wait) {
        this.account.save(() => {
          if (this.character !== null) {
            this.character.save(done);
          } else {
            done();
          }
        });
      } else {
        this.account.save();
        if (this.character !== null) {
          this.character.save(done);
        }
      }
    }

    this.boosts.forEach(boost => {
      if (boost.dbModel) {
        boost.dbModel.remaining = boost.timeout.remaining;
        boost.dbModel.save();
      }
    });
  }

  /**
   * Sets the character of an account. Has to be part of his available accounts.
   *
   * @param {database.Character} newCharacter the new character
   * @returns {boolean} whether the character has been applied.
   */
  selectCharacter(newCharacter) {
    let bIsOwnChar = false;
    for (let i = 0; i < this.availableCharacters.length; i++) {
      const character = this.availableCharacters[i];

      if (character === newCharacter) {
        bIsOwnChar = true;
        break;
      }
    }

    if (!bIsOwnChar) {
      throw new ScriptError(`Trying to assign character ${newCharacter.name} to account ${this.account.username}`);
    }

    this.character = newCharacter;

    // Set the Faction
    this.faction = vrp.faction.manager.findById(this.character.faction) || null;
    if (this.faction !== null) {
      if (!this.faction.ranks.has(this.character.faction_rank)) {
        throw new ScriptError(`Player's faction_rank is invalid (looking for "${this.character.faction_rank}" in faction "${this.faction.name}").`);
      }
      this.factionRank = this.faction.ranks.get(this.character.faction_rank);
      this.factionRank.applyPermissions(this.permissions);
    } else {
      if (this.character.faction !== 0) {
        log.error(`could not find faction with id ${this.character.faction} for character ${this.character.name}!`);
        this.sendError('could not find your faction, please report this to the <strong>V:RP Development Team</strong>!');
      }
    }

    // If the faction is valid, add him to the faction's online members
    if (this.faction) {
      this.faction.onlineMembers.set(this.character, this);
    }

    // Load and Create Storages
    vrp.database.Storage.find({owner: this.character.id, owner_type: 'c'}, storages => {
      for (let i = 0; i < storages.length; i++) {
        if (!this.storages.add(new vrp.item.DBStorage(storages[i]))) {
          log.error(`Could not create DBStorage ${storages[i].id} for account ${this.account.username}(id ${this.account.id}).`);
        }
      }
    });

    // Load and Create Boosts
    vrp.database.Boost.find({ applied_to: this.character.id }, boosts => {
      for (let i = 0; i < boosts.length; i++) {
        if (!shared.Boost.fromDBModel(boosts[i]).applied) {
          throw new ScriptError(`Could not apply boost ${boosts[i].id} on ${this.character.name}(${this.character.id})`);
        }
      }
    });

    // Set his last online timestamp
    this.character.last_online = new Date();

    // Set the player's model
    this.model = this.character.skin;

    // Check if it is the player's first connection
    const p = this.character.last_pos;
    this.firstConnection = p.x === 0 && p.y === 0 && p.z === 0;

    if (this.firstConnection) {
      log.implement('Tutorial for Characters');
    }

    // Set the player to his last position
    this.position = p;
    this.loginCamera.active = false;
    this.graphics.RenderCameras(false);
    this.ToggleControls(true, true);

    // Create the V:RP Stats (Hunger, Thirst etc.)
    this.createStats();

    // Evaluate / Add Permissions
    this.permissions.evaluate('CAN_CHAT');
    this.permissions.evaluate('CAN_JOIN_FACTION');
    this.permissions.add('CAN_SEE_OOC_CHAT');
    this.permissions.add('CAN_WRITE_IN_OOC');
    this.permissions.add('CAN_SEE_PMs');
    this.permissions.add('CAN_SEND_PMs');

    // Get his vehicles
    this.vehicles = new Set(vrp.vehicle.manager.spawnCharacterVehicles(this.character));

    // Start 'save' interval
    this.intervals.set('save', setInterval(this.save.bind(this), vrp.config.game.saveInterval));

    this.playing = true;
    return true;
  }

  /**
   * Sends an error message to the player.
   *
   * @param {string} message the message.
   */
  sendError(message) {
    return this.sendMessage(`Error: ${message}`, vrp.config.game.color.message.error);
  }

  /**
   * Sends an info message to the player.
   *
   * @param {string} message the message.
   */
  sendInfo(message) {
    return this.sendMessage(`Info: ${message}`, vrp.config.game.color.message.info);
  }

  /**
   * Sends a colored message to the player.
   *
   * @param {string} message the message.
   * @param {RGB} color the message color.
   */
  sendMessage(message, color) {
    this.client.SendChatMessage(message, color);
  }

  /**
   * Sends a colored message to the player.
   *
   * @param {string} message the message.
   * @param {RGB} color the message color.
   */
  sendRaw(message, color) {
    this.client.SendChatMessage(message, color);
  }

  /**
   * Sends an success message to the player.
   *
   * @param {string} message the message.
   */
  sendSuccess(message) {
    return this.sendMessage(`Success: ${message}`, vrp.config.game.color.message.success);
  }

  /**
   * Sets the player's world (game) up
   *
   * @param {Object} info the current world info
   */
  setupWorld(info) {
    for (let i = 0; i < info.IPLs.length; i++) {
      this.world.RequestIPL(info.IPLs[i]);
    }
    for (let i = 0; i < info.interiors.length; i++) {
      this.world.EnableInterior(info.interiors[i]);
    }
    this.world.weatherPersistNow = info.weather;
    this.world.rainLevel = info.rainLevel;
    this.world.snowLevel = info.snowLevel;
    this.world.windLevel = info.windLevel;
    this.world.timeScale = info.timeScale;

    /** @type {Date} */
    const time = info.time;
    this.world.SetTime(time.getHours(), time.getMinutes(), time.getSeconds());

    this.debug('Game set up.');
    log.debug(`player.setupWorld(${this.name}) finished`, 4);
  }

  /**
   * Sets the default Player permissions
   */
   setPermissions() {
   }

  /**
   * Validates the entered admin authentication code
   *
   * @this {Player}
   * @param {integer} code the authentication code
   * @returns {integer} shared.enum.LoginResult
   */
  validateAuthCode(code) {
    this.account.getLoginAttempts(this, attempts => {
      if (attempts.length > 4) {
        log.debug(`Too many login attempts for ${this.name}.`, 2);
        vrp.server.banIP(this.client.ipAddress, {id: 0, username: 'The Server' }, { minute: 15 }, 'Too many failed login attempts');
        this.debug('You have been banned for 15 minutes because you\'ve entered the password wrong too many times.');
        this.client.Kick('You have been banned for 15 minutes because you\'ve entered the password wrong too many times.');
      }
    });

    if (code === this.authCode && this.authCode !== 0) {
      this.login();
      return shared.enum.LoginResult.SuccessAuth;
    }
    this.account.loginAttempt(this, 'fail');
    return shared.enum.LoginResult.ErrorWrongAuthCode;
  }

  /**
   * Validates a login request.
   *
   * @this {Player}
   * @param {string} accountName
   * @param {string} password the user password to check.
   * @param {function (result: LoginResult)} done callback
   */
  validateLogin(accountName, password, done) {
    log.debug('validateLogin');
    if (vrp.server.loginRestriction === 'Locked') {
      done(shared.enum.LoginResult.ErrorLoginDisabled);
      return;
    }

    // Check if the account exists
    vrp.database.Account.first({username: accountName}, account => {
      if (typeof account === 'undefined') {
        done(shared.enum.LoginResult.ErrorUnknownAccount);
        return;
      }

      this.account = account;

      // Check if we have any login restrictions and if there are any, if the account is allowed to bypass them
      const restriction = vrp.server.loginRestriction;
      if (restriction !== 'None') {
        if (this.accessLevel !== restriction) {
          done(shared.enum.LoginResult.ErrorLoginDisabled);
          return;
        }
      }

      // Update bans
      this.getActiveBans(() => {
        if (this.bans.length > 0) {
          log.warn(`Banned player ${this.name} tried to log in.`);
          this.debug('You are banned from the server.');
          this.client.Kick('You are banned form the server.');
          return;
        }

        this.account.getLoginAttempts(this, attempts => {
          // we want to make this async because we dont want to block the server execution
          bcrypt.compare(password, this.account.password.replace('$2y$', '$2a$'), (err, res) => {
            if (err) {
              log.error('Error comparing user password: ' + err);
              done(shared.enum.LoginResult.ErrorInternal);
              return;
            }
            if (!res) {
              this.account.loginAttempt(this, 'fail');
              done(shared.enum.LoginResult.ErrorWrongPassword);

              if (attempts.length >= 4) {
                vrp.server.banIP(this.client.ipAddress, {id: 0, username: 'The Server'}, { minute: 15 }, 'Too many failed login attempts');
                this.debug('You have been banned for 15 minutes because you\'ve entered the password wrong too many times.');
                this.client.Kick('You have been banned for 15 minutes because you\'ve entered the password wrong too many times.');
              }
              return;
            }
            if (this.account.access_level <= 1 || !vrp.config.environment.production) {
              log.debug(`Logging player ${this.name} in.`, 4);
              this.login();
              done(shared.enum.LoginResult.Success);
            } else {
              // Get the slack name for that account
              vrp.database.SlackUser.first({account: this.account.id}, slackUser => {
                if (typeof slackUser === 'undefined') {
                  done(shared.enum.LoginResult.ErrorAuthName);
                  return;
                }

                // Generate a random number
                this.authCode = Math.round(Math.random() * (99999 - 10000) + 10000);

                shared.integration.slack.sendMessage(`${this.authCode}`, {
                  color: '#e67e22',
                  channel: '@' + slackUser.slack_name,
                  symbol: 'bell',
                  title: 'Authentication Code',
                });

                log.debug(`Admin ${this.account.username} is logging in. Sent out slack auth code.`, 2);
                done(shared.enum.LoginResult.SuccessWaitForAuth);
              });
            }
          });
        });
      });
    });
  }
}

/** @typedef {shared.enum.LoginResult} LoginResult */
/** @typedef {GTAMP.Vector3f} Vector3f */
