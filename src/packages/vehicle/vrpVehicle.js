/**
 * @title Vehicle.js
 * @overview VRP Vehicle Class
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

/**
 * V:RP Vehicle Class Implementation
 */
export default class VRPVehicle {
  /**
  * Creates a new V:RP Vehicle from the Database Model
  *
  * @param {database.Vehicle} dbModel - The Database Model
  * @param {function(vehicle: VRPVehicle)} [done=()=>{}] - Callback (after owner has been assigned)
  */
  constructor(dbModel, done = () => {}) {
    /** @type {database.Vehicle} */
    this.dbModel = dbModel;

    /** @type {boolean} */
    this.spawned = false;

    /** @type {integer} */
    this.networkId = -1;

    /** @type {database.Character|VehicleManager|Faction} */
    this._owner = undefined;

    /** @type {boolean} */
    this.valid = false;

    /** @type {boolean} */
    this.inactive = false;

    this._assignOwner(done);

    global.package.manager.dbVehicles.set(this.dbModel.id, this);
    log.debug(`Created new VRPVehicle for Vehicle ${dbModel.id}`, 3);
  }

  /**
  * Tries to assign the owner of the vehicle.
  *
  * @param {function(vehicle: VRPVehicle)} [done=()=>{}] - Callback
  */
  _assignOwner(done = () => {}) {
    if (!this.dbModel) {
      done(this);
      return;
    }

    const inactiveDate = new Date();
    inactiveDate.setDate(inactiveDate.getDate() - 7);

    switch (this.dbModel.owner_type) {
    case 'c':
      vrp.database.Character.first({id: this.dbModel.owner}, character => {
        if (typeof character === 'undefined') {
          log.warn(`Vehicle ${this.dbModel.id} - invalid owning character: ${this.dbModel.owner}`);
          done(this);
          return;
        }
        this._owner = character;
        this.valid = true;

        if (character.last_online < inactiveDate) {
          this.inactive = true;
        }
        done(this);
      });
      break;
    case 'f':
      const faction = vrp.faction.manager.findById(this.dbModel.owner);

      if (typeof faction === 'undefined') {
        log.warn(`Vehicle ${this.dbModel.id} - invalid owning faction: ${this.dbModel.owner}`);
        done(this);
        return;
      }
      this._owner = faction;
      this.valid = true;
      done(this);
      break;
    case 's':
    default:
      log.debug(`Adding Script-Owned vehicle ${this.dbModel.id}`);
      this._owner = global.package.manager;
      this.valid = true;
      done(this);
      break;
    }
  }

  /**
   * Destroys the vehicle and removes it from the Manager
   */
  destroy() {
    global.package.manager.remove(this);
    global.package.manager.dbVehicles.remove(this.dbModel.id);
  }

  /**
   * Returns the GTA:MP Vehicle
   *
   * @type {Vehicle}
   */
  get gtamp() {
    return global.package.manager.getGTAMPVehicle(this);
  }

  /**
   * Returns whether the Vehicle is owned by a Faction.
   *
   * @returns {boolean}
   */
  isFactionVehicle() {
    return this._owner && this.dbModel.owner_type === 'f';
  }

  /**
   * Returns whether the Owner of the Vehicle is online / available.
   *
   * @returns {boolean}
   */
  isOwnerOnline() {
    if (this.isFactionVehicle() || this.isScriptVehicle()) {
      return true;
    } else if (this.isPlayerVehicle() && this.owner.getPlayer()) {
      return true;
    }
    return false;
  }

  /**
   * Returns whether the Vehicle is owned by a Player (Character).
   *
   * @returns {boolean}
   */
  isPlayerVehicle() {
    return this._owner && this.dbModel.owner_type === 'c';
  }

  /**
   * Returns whether the Vehicle is owned by the Script.
   *
   * @returns {boolean}
   */
  isScriptVehicle() {
    return this._owner === global.package.manager;
  }

  /**
   * @type {database.Character|VehicleManager|Faction}
   */
  get owner() {
    return this._owner;
  }

  /**
   * Spawns the Vehicle.
   *
   * @returns {boolean} whether the vehicle could be spawned.
   */
  spawn() {
    if (this.spawned) {
      return false;
    }

    log.debug(`Spawning Vehicle ${this.dbModel.id}`);

    const vehicle = new Vehicle(GTAHash(this.dbModel.model_name), this.dbModel.position, this.dbModel.rotation);
    vehicle.SetRGBAColor(this.dbModel.primary_color, this.dbModel.secondary_color);

    this.networkId = vehicle.networkId;
    this.spawned = true;

    global.package.manager.add(this);

    return true;
  }
}
