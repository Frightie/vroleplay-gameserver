/**
 * @title commands.js
 * @overview Implements Package Commands
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

/**
 * Vehicle System Commands
 */
const Commands = {};

/**
 * Vehicle System Commands
 */
export default Commands;

Commands.vehicle = new shared.CommandGroup(['vehicle', 'v'], {});

Commands.vehicle.add(new shared.Command('lock', {
  parameters: [
    ['state', 'integer'],
  ],
}, (player, state) => {
  player.vehicle.doorLockState = state;
}));

Commands.vehicle.add(new shared.Command('list', {
  parameters: [],
}, (player) => {
  player.sendInfo(`<em>Vehicles: ${player.vehicles.size}</em>`);
  player.vehicles.forEach(vehicle => {
    player.sendInfo(`<em>${vehicle.spawned ? '' : 'not spawned '}</em>${vehicle.dbModel.model_name}`);
  });
}));
