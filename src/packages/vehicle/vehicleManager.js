/**
 * @title VehicleManager.js
 * @overview Vehicle Manager Implementation
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

import NetworkEntityManager from '../../shared/classes/networkEntityManager';
import VRPVehicle from './vrpVehicle';

/**
 * VehicleManager Class
 *
 * This Class is responsible for Managing all connected Vehicles. It associates
 * the GTA:MP Vehicle with the internal V:RP Vehicle. It also contains various
 * utility functions.
 */
class VehicleManager extends NetworkEntityManager {
  /**
   * Creates the Vehicle Manager
   *
   * @constructor
   */
  constructor() {
    super('vehicles');
    /** @type {boolean} */
    this.initialized = false;
    /** @type {Map<int, VRPVehicle>} */
    this.vehicles = this.vehicles;
    /** @type {Map<int, VRPVehicle>} */
    this.dbVehicles = new Map();

    /** @type {VRPVehicle} */
    this.VRPVehicle = VRPVehicle;
  }

  /**
   * Initializes the Faction manager
   *
   * @param {function()} [done=()=>{}] - Callback
   */
  init(done = () => {}) {
    log.print('Initializing Vehicle Manager');

    const fnAfterCreate = vehicle => {
      if (!vehicle.valid) {
        log.error(`Invalid Vehicle ${vehicle.dbModel.id}.`);
        return;
      }

      if (vehicle.isPlayerVehicle() && vehicle.inactive) {
        log.debug(`Skipping Vehicle ${vehicle.dbModel.id} because of an inactive owner.`);
        return;
      }

      if (!vehicle.spawn()) {
        log.warn(`Could not spawn Vehicle ${vehicle.dbModel.id}.`);
      }

      if (vehicle.isFactionVehicle()) {
        vehicle.owner.vehicles.add(vehicle);
      }
    };

    vrp.database.Vehicle.all(vehicles => {
      for (let i = 0; i < vehicles.length; i++) {
        new VRPVehicle(vehicles[i], fnAfterCreate); //eslint-disable-line
      }
      this.initialized = true;
      done();
    });
  }

  /**
   * Gets a GTA:MP Vehicle from a V:RP Vehicle
   *
   * @param {VRPVehicle} vehicle - The V:RP Vehicle
   *
   * @returns {undefined|Vehicle} a GTA:MP Vehicle or undefined if it was not found.
   */
  getGTAMPVehicle(vehicle) {
    for (const veh of g_vehicles) {//eslint-disable-line
      if (veh.networkId === vehicle.networkId) {
        return veh;
      }
    }
    log.warn(`Could not get GTA:MP Vehicle for Vehicle ${vehicle.networkId}`);
    return undefined;
  }

  /**
   * Tries to spawn the vehicles of a Character.
   *
   * @param {database.Character} character - The Character whose vehicles should be spawned.
   *
   * @returns {Array<VRPVehicle>} the character's vehicles
   */
  spawnCharacterVehicles(character) {
    const charVehicles = [];

    this.dbVehicles.forEach(vehicle => {
      if (vehicle.dbModel.owner === character.id && vehicle.isPlayerVehicle()) {
        if (!vehicle.spawned) {
          if (!vehicle.spawn()) {
            log.warn(`Could not spawn Vehicle ${vehicle.dbModel.id}`);
          }
        }
        charVehicles.push(vehicle);
      }
    });

    return charVehicles;
  }
}

/**
 * VehicleManager instance
 */
export default new VehicleManager();
