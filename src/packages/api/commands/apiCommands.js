/**
 * @title apiCommands.js
 * @overview API Commands Implementation
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

const APICommands = {};
let pubSub;

/**
 * API Commands
 */
export default APICommands;

/**
 * Registers all API Commands
 *
 * @param {PubSub} pubSub instance
 */
APICommands.register = _pubSub => {
  pubSub = _pubSub;
  pubSub.registerHandler('updated', 'account', APICommands.updateAccount);
  pubSub.registerHandler('vrp_server', 'get_status', APICommands.getStatus);
  pubSub.registerHandler('vrp_server', 'get_player_status', APICommands.getPlayerStatus);
  log.debug('APICommands registered', 2);
};

/**
 * Called when an account was updated outside our server (remote change)
 *
 * @param {integer} id account id
 */
APICommands.updateAccount = id => {
  log.debug(`APICommands.updateAccount(${id})`, 4);
  log.print(`Apparently, an account with the id "${id}" was updated. (${typeof id})`);
};

/**
 * Called when someone else wants to know if the server is running
 *
 */
APICommands.getStatus = () => {
  pubSub.publish('vrp_server', 'status 1');
};

/**
 * Called when someone wants to know the online status of a character/account/player
 */
APICommands.getPlayerStatus = (...args) => {
  const name = args.join(' ');
  for (const player of vrp.player.manager.players.values()) {
    if (player.name === name ||
      player.character !== null && player.character.name === name ||
      player.account !== null && player.account.username === name) {
      pubSub.publish('vrp_server', `player_status ${name} 1`);
      return;
    }
  }
  pubSub.publish('vrp_server', `player_status ${name} 0`);
};
