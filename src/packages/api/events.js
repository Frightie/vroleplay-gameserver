/**
 * @title events.js
 * @overview Package Events File
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

/* global Package */

/**
 * API Events
 */
export default class Events {
  /**
   * Registers all Events
   *
   */
  static register() {
    events.Add('api.toggle', Events.toggleAPI);
  }

  /**
   * Toggles the API's state
   *
   * @param {boolean} state - The state of the API.
   */
  static toggleAPI(state) {
    Package.enabled = state;
  }
}
