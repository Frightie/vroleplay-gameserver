/**
 * @title main.js
 * @overview Main Package File
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

// Predefined V:RP Header to install our own error types & handler
import error from '../../shared/error';
import errorHandler from '../../shared/errorHandler';
error.install(global);
process.on('uncaughtException', errorHandler);

import Package from '../../shared/classes/package';
import apiCommands from './commands/apiCommands';
import Redis from '././redisImpl';

class API extends Package {
  /**
   * Constructs the Package
   * @constructor
   */
  constructor() {
    super('api');
    this.enabled = true;
    this.initialized = false;
  }

  /**
   * Starts the Package
   */
  start() {
    super.start();
  }

  /**
   * Returns the Dependencies
   * @returns {[string]}
   */
  get dependencies() {
    return [];
  }

  /**
   * Initializes the API
   *
   * @param {function ()} [done=()=>{}] callback
   */
  initialize(done = () => {}) {
    this.redis = new Redis('127.0.0.1', 4203, () => {
      apiCommands.register(this.redis);

      // Copy over redis impl
      shared.utility.concat(this, this.redis);

      done();
      this.initialized = true;
    });

    for (let i = 0; i < Redis.functions.length; i++) {
      const key = Redis.functions[i];
      if (typeof this.redis[key] === 'function' && key !== 'connect') {
        this[key] = this.redis[key];
      }
    }
  }

  receiveDependencies(deps) { super.receiveDependencies(global, deps); }
  receiveVRPNamespace(namespace) { super.receiveVRPNamespace(global, namespace); }
}

// Register globals
global.package = new API();
global[global.package.name] = global.package;
