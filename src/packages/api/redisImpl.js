/**
 * @title redisImpl.js
 * @overview Implements redis pub/sub
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

const redis = require('redis');

/**
 * The Redis Implementation (Wrapper) for PubSub usage.
 */
export default class RedisImplementation {
  /**
   * Creates a new RedisImplementation instance
   *
   * @constructor
   * @param {integer} port the redis port
   * @param {string} host the redis host
   * @param {function ()} done callback
   * @param {string} [auth=''] the authentication key
   */
  constructor(host, port, done, auth = '') {
    /** @type {string} */
    this.host = host;

    /** @type {int} */
    this.port = port;

    /** @type {string} */
    this.auth = auth;

    /** @type {boolean} */
    this.connected = false;

    /** @type {Object} */
    this.handler = {};

    /** @type {function (channel: string, message: string)} */
    this.onReceiveMessage = (channel, message) => {
      if (typeof this.handler[channel] === 'undefined') {
        return log.print(`RedisImplementation - unhandled Message: (${channel}) ${message}`);
      }

      const args = message.split(' ');
      let command;
      if (args.length > 0) {
        command = args[0];
      }
      for (const idx in args) {
        if (!isNaN(args[idx])) {
          args[idx] = Number(args[idx]);
        }
      }
      for (let i = 0; i < this.handler[channel].length; i++) {
        const receiver = this.handler[channel][i];
        if (receiver.command === '' || typeof command !== 'undefined' && message === receiver.command || message.indexOf(receiver.command + ' ') === 0) {
          // FIXME: Not the dirty way, please
          if (receiver.command !== '') {
            args.splice(0, 1);
          }
          receiver.handler.apply(null, args);
          args.unshift(command);
        }
      }
    };

    this.connect(done);
  }

  /**
   * @type {Array<string>}
   */
  static get functions() {
    return ['decr', 'get', 'incr', 'publish', 'registerHandler', 'set', 'subscribe', 'write'];
  }

  /**
   * Connects to the Redis Server.
   *
   * @param {function ()} done - Callback
   */
  connect(done) {
    let ready = 0;
    const options = {
      max_attempts: 5,
      retry_max_delay: 10000,
      onTimeout: () => {
        log.error(`Could not connect to the API (timeout after ${options.max_attempts} tries).`);
        options.onTimeout = undefined;
      },
    };
    let lastError = '';

    /** @type {redis.Client} */
    this.pub = redis.createClient(this.port, this.host, options);
    this.pub.on('error', err => {
      if (err.message === lastError) {
        log.debug(`RedisImplementation - Pub: ${err}`, 2);
        return;
      }
      lastError = err.message;

      log.error(`RedisImplementation - Pub: ${err}`);
    });
    this.pub.on('ready', () => {
      ready++;
      if (ready === 2) {
        this.connected = true;
        done();
      }
    });
    this.pub.auth(this.auth);

    /** @type {redis.Client} */
    this.sub = redis.createClient(this.port, this.host, options);
    this.sub.on('error', err => {
      if (err.message === lastError) {
        log.debug(`RedisImplementation - Sub: ${err}`, 2);
        return;
      }
      lastError = err.message;

      log.error(`RedisImplementation - Sub: ${err}`);
    });
    this.sub.on('ready', () => {
      ready++;
      if (ready === 2) {
        this.connected = true;
        done();
      }
    });
    this.sub.on('message', this.onReceiveMessage);
    this.sub.auth(this.auth);
  }

  /**
   * Decreases the key by 1.
   *
   * @param {string} key - The key that should be decreased.
   */
  decr(key) {
    if (!this.connected) {
      return;
    }
    this.pub.decr(key);
  }

  /**
   * Gets the value of the given key.
   *
   * @param {string} key - The key.
   * @param {function(reply: *, err: Error)} done - Callback with the Result.
   */
  get(key, done) {
    if (!this.connected) {
      done(undefined, '!connected');
      return;
    }
    this.pub.get(key, (err, reply) => {
      done(reply, err);
    });
  }

  /**
   * Increases the key by 1.
   *
   * @param {string} key - The key that should be increased.
   */
  incr(key) {
    if (!this.connected) {
      return;
    }
    this.pub.incr(key);
  }

  /**
   * Publishes a Message on the given channel.
   *
   * @param {string} channel - The channel where the Message should be published.
   * @param {string} data - The data that should be sent.
   */
  publish(channel, data) {
    if (!this.connected) {
      return;
    }
    this.pub.publish(channel, data);
  }

  /**
   * Registers a Handler for a certain command in a Channel.
   *
   * @see {RedisImplementation#removeHandler}
   *
   * @param {string} channel - The Channel where the Handler should be added.
   * @param {string} command - The Command name.
   * @param {function(args: ...*)} handler - The Handler for the Command.
   *
   * @returns {undefined|Symbol} A Symbol to the created handler. This can be used to
   *                             delete the Handler at a later point. Undefined if the
   *                             Handler could not be added.
   */
  registerHandler(channel, command, handler) {
    if (!this.connected) {
      return undefined;
    }
    if (typeof this.handler[channel] === 'undefined') {
      this.subscribe(channel);
      this.handler[channel] = [];
    }
    this.handler[channel].push({ command: command, handler: handler, sym: Symbol() });
    return this.handler[channel][this.handler[channel].length - 1].sym;
  }

  /**
   * Removes a Handler with the given Symbol from a channel.
   *
   * @param {string} channel - The Channel of the command.
   * @param {Symbol} symbol - The Symbol of the Handler.
   */
  removeHandler(channel, symbol) {
    if (typeof this.handler[channel] === 'undefined') {
      return;
    }
    let idx;
    for (let i = 0; i < this.handler[channel].length; i++) {
      if (this.handler[channel].sym === symbol) {
        idx = i;
        break;
      }
    }
    if (typeof idx === 'undefined') {
      return;
    }
    this.handler[channel].splice(idx, 1);
  }

  /**
   * Sets the value of the given key.
   *
   * @param {string} key - The key.
   * @param {*} value - The value.
   */
  set(key, value) {
    if (!this.connected) {
      return;
    }
    this.pub.set(key, value);
  }

  /**
   * Subscribes to a Channel.
   *
   * @private
   * @param {string} channel - The name of the Channel.
   */
  subscribe(channel) {
    if (!this.connected) {
      return;
    }
    this.sub.subscribe(channel);
  }

  /**
   * Writes Data to a Channel.
   *
   * @private
   * @param {string} channel - The Channel's name.
   * @param {*} data - The data to write.
   */
  write(channel, data) {
    if (!this.connected) {
      return;
    }
    this.publish(channel, data);
  }
}
