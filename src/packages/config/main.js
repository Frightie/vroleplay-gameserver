/**
 * @title main.js
 * @overview Main Package File
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

// Predefined V:RP Header to install our own error types & handler
import error from '../../shared/error';
import errorHandler from '../../shared/errorHandler';
error.install(global);
process.on('uncaughtException', errorHandler);

import os from 'os';
import Package from '../../shared/classes/package';
import _cfg from '../../config/config';

class Config extends Package {
  /**
   * Constructs the Package
   * @constructor
   */
  constructor() {
    super('config');
  }

  /**
   * Starts the Package
   */
  start() {
    super.start();
    this._cfg = _cfg;
    shared.utility.concat(this, this._cfg);
    if (typeof this.environment !== 'undefined') {
      this.environment.production = false;
    }
    this.detectEnvironment();
    this.initialized = true;
  }

  get logLevel() {
    return this._cfg.environment.logLevel;
  }

  set logLevel(level) {
    let newLevel = level;
    if (level > 5) {
      newLevel = 5;
    } else if (level < 0 ) {
      newLevel = 0;
    }
    this._cfg.environment.logLevel = newLevel;
    events.Call('config logLevel update', newLevel);
  }

  /**
   * Detect the current environment (production or local)
   */
  detectEnvironment() {
    const hostname = os.hostname();
    for (let i = 0; i < this.environment.localHosts.length; i++) {
      if (this.environment.localHosts[i] === hostname) {
        this.environment.production = false;
        return;
      }
    }
    this.environment.production = true;
    shared.integration.slack.enabled = true;
  }

  receiveDependencies(deps) { super.receiveDependencies(global, deps); }
  receiveVRPNamespace(namespace) { super.receiveVRPNamespace(global, namespace); }
}

// Register globals
global.package = new Config();
global[global.package.name] = global.package;
