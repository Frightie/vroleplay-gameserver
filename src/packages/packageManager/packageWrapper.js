/**
 * @title packageWrapper.js
 * @overview Package Wrapper class
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

/**
 * A wrapper class for packages.
 */
export default class PackageWrapper {
  /**
   * Creates a new PackageWrapper
   *
   * @constructor
   * @param {string} name the Package's name.
   */
  constructor(name) {
    /** @type {string} */
    this.name = name;
    /** @type {Object} */
    this.package = {};

    // ESDoc Stubs
    /** @type {function ()} */
    this.fulfill = undefined;
    /** @type {Timeout} */
    this.timer = undefined;
  }

  /**
   * The Package's dependencies
   *
   * @returns {Array<string>}
   */
  get dependencies() {
    return this.package.dependencies;
  }

  /**
   * The Package's interface
   *
   * @returns {Object}
   */
  get interface() {
    if (typeof this.package.interface === 'function') {
      return this.package.interface();
    }
    return this.package.interface;
  }

  /**
   * Try to retrieve the package because it could have been loaded prior to the
   * packageManager package. No interval will be started.
   *
   */
  earlyRetrieve() {
    if (typeof this.package !== 'undefined') {
      return;
    }

    // This function skips all the Interval stuff because it has been detected already.
    const retn = events.Call(`dear ${this.name}, please give yourself to me.`);

    if (retn.length > 0) {
      if (retn.length > 1) {
        throw new Error(`Too many results for package ${this.name}!`);
      }

      console.log(`${this.name} earlyRetrieve`);
      this.package = retn[0];
      clearInterval(this.timer);
      if (typeof this.fulfill === 'function') {
        this.fulfill();
        delete this.fulfill;
      }
      delete this.timer;
    }
  }

  /**
   * Try to retrieve the package every 100ms.
   *
   * @returns {Promise}
   */
  retrieve() {
    return new Promise(fulfill => {
      this.fulfill = fulfill;
      const fnTryRetrieve = () => {
        const retn = events.Call(`dear ${this.name}, please give yourself to me.`);

        if (retn.length > 0) {
          if (retn.length > 1) {
            throw new Error(`Too many results for package ${this.name}!`);
          }

          this.package = retn[0];
          clearInterval(this.timer);
          delete this.timer;
          fulfill(this);
          delete this.fulfill;
        }
      };
      this.timer = setInterval(fnTryRetrieve, 100);
    });
  }
}
