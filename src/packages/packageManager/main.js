/**
 * @title main.js
 * @overview Package Manager Implementation
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

import fs from 'fs';
require('../../shared/node_modules/colors');

import PackageWrapper from './packageWrapper';
import shared_ from '../../shared/shared';

/**
 * The PackageManager is the core of V:RP in terms of loading packages.
 * All packages get registered in it and when their dependencies have been loaded,
 * the package will be started.
 */
export default class PackageManager {
  /**
   * Constructs the Package
   */
  constructor() {
    /** @type Map<string, PackageWrapper> */
    this.packages = new Map();
  }

  /**
   * Checks for cross dependencies between packages.
   */
  checkCrossDependencies() {
    this.packages.forEach(pack => {
      for (let i = 0; i < pack.dependencies.length; i++) {
        const dependency = pack.dependencies[i];
        if (this.packages.has(dependency)) {
          const deppackage = this.packages.get(dependency);
          for (let i2 = 0; i2 < deppackage.dependencies.length; i2++) {
            if (deppackage.dependencies[i2] === pack.name) {
              throw new Error(`Cross-Dependency found: ${pack.name} <> ${deppackage.name}`);
            }
          }
        }
      }
    });
    return;
  }

  /**
   * Start the process of getting all packages.
   *
   * @private
   */
  getPackages() {
    console.time('Package Loading');
    const promises = [];

    this.packages.forEach(pack => {
      promises.push(pack.retrieve());
    });

    Promise.all(promises)
      .then(this.checkCrossDependencies.bind(this))
      .then(this.registerEventHandler.bind(this))
      .then(this.probeActivePackages.bind(this))
      .then(this.waitStart.bind(this))
      .catch(console.log.bind(console));
  }

  /**
   * Probes the Server for already loaded packages because the packageManager might
   * not be loaded as the first package.
   */
  probeActivePackages() {
    // Look in g_packages
    const names = new Set();
    for (let i = 0; i < g_packages.length; i++) { //eslint-disable-line
      let pkgName = g_packages[i].dir.substr(0, g_packages[i].dir.length - 1);
      pkgName = pkgName.substr(pkgName.lastIndexOf('/') + 1);
      names.add(pkgName);
    }

    this.packages.forEach(pack => {
      if (names.has(pack.name)) {
        pack.earlyRetrieve();
      }
    });
  }

  /**
   * Probes the given directory for potentially available packages.
   *
   * @param {string} dir the directory to probe.
   * @returns {Promise}
   */
  probePackages(dir) {
    return new Promise((fulfill, reject) => {
      fs.readdir(dir, (err, files) => {
        if (err) {
          reject(err);
        }

        // Loop through all files and stat them
        for (let i = 0; i < files.length; i++) {
          if (files[i] === 'packageManager') {
            continue;
          }

          const stat = fs.statSync(`${dir}/${files[i]}`);

          if (stat.isDirectory()) {
            this.packages.set(files[i], new PackageWrapper(files[i]));
          }
        }

        fulfill();
      });
    });
  }

  /**
   * Registers the event handler for Loaded Packages (GTA:MP Event)
   *
   * @private
   */
  registerEventHandler() {
    events.Add('PackageLoaded', pack => {
      this.packages.forEach(regPack => {
        let pkgName = pack.dir.substr(0, pack.dir.length - 1);
        pkgName = pkgName.substr(pkgName.lastIndexOf('/') + 1);
        if (pkgName === regPack.name) {
          regPack.earlyReceive();
        }
      });
    });
  }

  /**
   * Starts the process to initialize/start all packages. This function continuously checks
   * the loaded packages and their dependencies and tries to start them ASAP.
   *
   * @private
   */
  waitStart() {
    const fnTryStart = () => {
      let dispatched = 0;
      for (const pack of this.packages.values()) {
        if (pack.dispatched) {
          dispatched++;
          continue;
        }

        let depsLoaded = 0;
        for (let i = 0; i < pack.dependencies.length; i++) {
          if (!this.packages.has(pack.dependencies[i])) {
            throw new Error(`Unknown Package "${pack.dependencies[i]}"`);
          }

          const dep = this.packages.get(pack.dependencies[i]);

          if (typeof dep.package !== 'undefined' && dep.package.started) {
            depsLoaded++;
          }
        }

        if (depsLoaded === pack.dependencies.length) {
          const deps = new Map();
          deps.set('shared', shared_);
          if (pack.dependencies.length > 0) {
            for (let i = 0; i < pack.dependencies.length; i++) {
              const deppackage = this.packages.get(pack.dependencies[i]);

              deps.set(pack.dependencies[i], deppackage.interface);
            }
          }
          pack.package.receiveDependencies(deps);
          pack.package.start();
          pack.dispatched = true;
          dispatched++;
        }
      }

      if (dispatched === this.packages.size) {
        // Build the global namespace
        const namespace = {};
        this.packages.forEach(pack => {
          namespace[pack.name] = pack.interface;
        });
        namespace.shared = shared_;

        this.packages.forEach(pack => {
          pack.package.receiveVRPNamespace(namespace);
        });
        console.log('All packages started.');

        events.Call('packageManager.allDispatched', this.packages);

        const vrp = {};
        this.packages.forEach(pack => {
          vrp[pack.name] = pack.interface;
        });
        shared._setVRP(vrp);

        console.timeEnd('Package Loading');
        clearTimeout(this._timer);
      }
    };
    // Waits for all packages to be existant and all dependencies to be loaded
    this._timer = setInterval(fnTryStart, 200);
  }
}

/**
 * Main Function
 */
function main() {
  const packageManager = new PackageManager();

  // Probe packages
  packageManager.probePackages('./packages').then(packageManager.getPackages.bind(packageManager)).catch(e => { console.log(e); });
}

main();
