/**
 * @title server.js
 * @overview Server class Implementation
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

// Predefined V:RP Header to install our own error types & handler
import error from '../../shared/error';
import errorHandler from '../../shared/errorHandler';
error.install(global);
process.on('uncaughtException', errorHandler);

import moment from 'moment';
require('../../shared/node_modules/colors');

/**
 * The Server Class is the general manager for the whole V:RP gamemode.
 * It keeps track of all packages, managers and loads them on startup.
 * The server is also responsible for establishing the database connections.
 */
class Server {
  constructor() {
    /** @type {World} */
    this.world = require('./world');

    /** @type {Map<string, function>} */
    this.inputCommands = new Map();

    /** @type {string} */
    this.loginRestriction = 'Locked';

    /** @type {boolean} */
    this.initialized = false;

    events.Add('packageManager.allDispatched', this.init.bind(this));

    g_server.AddInputHandler(message => {
      const args = message.split(' ');
      if (!args.length) {
        return false;
      }
      const [ command ] = args.splice(0, 1);
      if (this.inputCommands.has(command)) {
        this.inputCommands.get(command)(...args);
        return true;
      }
    });

    // Add our input commands
    this.inputCommands.set('restrictLogin', state => {
      let newState = state;
      let text = state;
      if (state !== 'None' && state !== 'Locked') {
        if (!vrp.admin.levels.hasOwnProperty(state)) {
          log.error(`Invalid State '${state}`);
          return;
        }
        newState = vrp.admin.levels[state];
        text = `${newState.name} and above`;
      }

      this.loginRestriction = newState;
      log.success(`Login Restriction changed to '${text}' via the console.`, true);
    });
  }

  /**
   * Initializes the server.
   *
   * @param {Object} packages packages
   */
  init(packages) {
    /** @type {Map<string, Object>} */
    this.packages = packages;

    console.time('Server Initialization'.cyan);

    vrp.api.initialize(() => {
      log.success('API was initialized');
      vrp.api.set('player:count', 0);
    });

    // Establish database connection
    vrp.database.connect(orm => {
      vrp.database.connection = orm;

      this.world.initialize(vrp.config.game.world);

      vrp.permission.init();
      vrp.admin.init();
      vrp.command.manager.init(packages);
      vrp.item.manager.init();
      vrp.player.manager.init();
      vrp.faction.manager.init(() => {
        packages.forEach(pack => {
          if (typeof pack.package.events.register === 'function') {
            pack.package.events.register();
          }
        });


        vrp.vehicle.manager.init(() => {
          this.initialized = true;
          console.timeEnd('Server Initialization'.cyan);
          setTimeout(() => {
            this.printStatus();
            log.success('Server started.', true);
          }, 500);

          // We're done, lift login restriction
          this.loginRestriction = vrp.config.game.loginRestriction;
        });
      });
    });

    // Start the Time timer
    setInterval(() => {
      const cur = new Date();

      if (cur.getSeconds() === 0) {
        log.debug('time: minute', 5);
        events.Call('time: minute', cur);
      }
      if (cur.getMinutes() === 0 && cur.getSeconds() === 0) {
        log.debug('time: hour', 5);
        events.Call('time: hour', cur);
      }
    }, 1000);
  }

  /**
   * Bans an IP Address.
   *
   * @param {string} ipAddress the ip address to ban.
   * @param {database.Account|Object} issuer the issuer (account)
   * @param {int|Object} duration the duration of the ban (in days or as object)
   * @param {string} reason the reason for the ban.
   * @param {function (success: bool)} [done] callback when the function finished.
   */
  banIP(ipAddress, issuer, duration, reason, done = () => {}) {
    vrp.database.Ban.first({ ip: ipAddress, expiration: '>(SELECT CURRENT_TIMESTAMP())' }, ban => {
      if (typeof ban !== 'undefined') {
        done(false);
        return;
      }

      let expirationDate = moment();
      if (duration === -1) {
        expirationDate = expirationDate.set({ year: 2038, month: 0, date: 18 });
      } else {
        if (typeof duration === 'object') {
          for (const key in duration) {
            if (duration.hasOwnProperty(key)) {
              expirationDate = expirationDate.add(key, duration[key]);
            }
          }
        } else {
          expirationDate = expirationDate.add(duration, 'day');
        }
      }
      vrp.database.Ban.create({ip: ipAddress, issuer: issuer.id, expiration: expirationDate.format(), duration: duration.toString(), reason: reason}, newBan => {
        log.info(`[BAN] ${issuer.username} banned IP address ${ipAddress} until ${expirationDate.format('MMMM Do YYYY, h:mm:ss a')}. Reason: ${reason}`);
        done(typeof newBan !== 'undefined');
      });
    });
  }

  /**
   * Prints the Server's status
   *
   */
  printStatus() {
    console.log('');
    console.log('');
    console.log('--------------------------------------------------------------------------------');
    console.log(`                                   ${(this.initialized ? 'Server Status'.green : 'Server Status'.cyan)}`);
    console.log('');
    console.log('   Packages:');
    this.packages.forEach(pack => {
      let identifier = pack.package.started;
      if (typeof pack.package.initialized !== 'undefined') {
        identifier = pack.package.initialized;
      }
      console.log(`         [${(identifier ? '+'.green : '?'.red)}] ${pack.name} ${(typeof pack.package.enabled !== 'undefined' && !pack.package.enabled ? ' (' + 'disabled'.yellow + ')' : '')}`);
      if (typeof pack.package.manager !== 'undefined') {
        console.log(`            [${(pack.package.manager.initialized ? '+'.green : 'x'.red )}] ${pack.name}.manager`);
      }
    });
    console.log('');
    console.log(`   Environment:    ${(vrp.config.environment.production ? 'production'.green : 'local'.cyan)}`);
    console.log(`   Database:       ${(typeof vrp.database.connection !== 'undefined' ? 'connected'.green : 'disconnected'.red)}`);
    console.log(`   Redis:          ${(vrp.api.redis.connected ? 'connected'.green : 'disconnected'.yellow)}`);
    console.log(`   API:            ${(vrp.api.enabled ? 'enabled'.green : 'disabled'.red)}`);
    console.log('');
    console.log('--------------------------------------------------------------------------------');
  }
}

/**
 * Server instance
 */
export default new Server();
