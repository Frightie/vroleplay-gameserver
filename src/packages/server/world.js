 /**
  * @title world.js
  * @overview Manages the World options (weather, time etc)
  * @author Ariel Weinberger
  * @copyright (c) 2015 V:Role Play Team [v-rp.com]
  */

/** @typedef {number} float */
/** @typedef {number} int */


/**
 * The World class is responsible for managing the ingame's weather conditions, time
 * and other various world properties for each player.
 */
class World {
  /**
   * Constructs a new World (that sounds weird)
   *
   * @constructor
   */
  constructor() {
    // Set the values. They will be overwritten when initialize is called.
    // This is being done mainly for better code documentation.
    /** @type {Array<string>} */
    this.interiors = [];
    /** @type {Array<string>} */
    this.IPLs = [];
    /** @type {float} */
    this._timeScale = 1.0;
    /** @type {int} */
    this._weather = 0;
    /** @type {float} */
    this._rainLevel = 0.0;
    /** @type {float} */
    this._windLevel = 0.0;
    /** @type {Object} */
    this.currentTransition = {};
  }

  /**
   * Initializes the World with the given data
   *
   * @param {Object} info the default world information
   */
  initialize(info) {
    // Concat the information
    for (const key in info) {
      if (typeof this[key] !== 'function' && typeof this[key] !== 'undefined') {
        this[key] = info[key];
      }
    }

    events.Add('time: minute', time => {
      vrp.player.manager.players.forEach(player => {
        player.world.SetTime(time.getHours(), time.getMinutes(), time.getSeconds());
      });
    });

    /*
     * 1: Sunny
     * 	> clouds in distance
     * 2: Clear
     * 	> more clouds, windy
     * 3: Cloudy
     * 	> clouds everywhere, windy
     * 4: Smog
     * 	> mostly cloudy, fog like, windy
     * 5: Fog
     *  > mostly cloudy, fog, windy
     * 6: Rain (Overcase)
     * 	> clouds everywhere, rain, windy
     * 7: Storm
     * 	> thunders, dark ambient, rain, very windy
     * 8: Rain
     *  > cloudy, light ambient, rain, windy
     * 9: Clearing
     *  > many clouds -> clearing to almost no clouds, rain decreasing (probably around 20 seconds), slightly windy
     * 10: Snow
     *  > mostly cloudy, light snowfall, windy, dark ambient
     * 11: Hardc0re Snow
     *  > clouds everywhere, snowfall, very windy, dark ambient
     * 12: Light Snow
     *  > clouds everywhere, snowfall, almost no wind
     */
    const world = this;
    /** @type {Object} */
    this.transitions = {
      sunny: { to: 1, runner: function* sunny() {
          if (world.weather >= 3 && world.weather !== 9) {
            world.weather = 9;
            yield 30000;
          }
          world.weather = 1;
          yield 210000;
        },
      },
      clear: { to: 2, runner: function* clear() {
          if (world.weather === 6 || world.weather === 7) {
            world.weather = 9;
            yield 60000;
          }
          world.weather = 2;
          yield 600000;
        },
      },
      cloudy: { to: 3, runner: function* cloudy() {
          world.weather = 3;
          yield 600000;
        },
      },
      lightRain: { to: 8, runner: function* lightRain() {
          if (world.weather !== 7 && world.weather !== 6) {
            world.weather = 3;
            yield 630000;
            world.weatherNow = 3;
          }
          world.weather = 8;
          yield 120000;
          // 10 minutes
          for (let i = 0; i < 1500; i++) {
            world.rainLevel = 0.1;
            yield 400;
          }
        },
      },
      rain: { to: 8, runner: function* rain() {
          if (world.weather === 1 || world.weather === 2 || world.weather === 9) {
            world.weather = 3;
            yield 600000;
          }
          world.weather = 8;
          yield 600000;
        },
      },
      heavyRain: { to: 6, runner: function* heavyRain() {
          if (world.weather === 1 || world.weather === 2) {
            world.weather = 3;
            yield 600000;
          }
          world.weather = 8;
          yield 120000;
          world.weather = 6;
          yield 600000;
        },
      },
      storm: { to: 7, runner: function* storm() {
          if (world.weather < 3 || world.weather > 9) {
            world.weather = 3;
            yield 600000;
          }
          if (world.weather === 3) {
            world.weather = 8;
            yield 480000;
          }
          world.weather = 7;
          yield 2100000;
        },
      },
    };

    const fnDynamicWeather = () => {
      let w = Math.floor(Math.random() * (Object.getOwnPropertyNames(this.transitions).length - 1) + 1);
      let transition;
      for (const key in this.transitions) {
        if (w === 0) {
          break;
        }
        w--;
        transition = key;
      }
      log.debug('Dynamic Weather: going ' + transition);
      this.transition(this.transitions[transition], fnDynamicWeather);
    };
    fnDynamicWeather();
  }

  /**
   * @type {float}
   */
  get rainLevel() {
    return this._rainLevel;
  }

  /**
   * Sets the rainLevel. Valid range: 0.0f - 2.5f
   *
   * @type {float} level rain level from 0.0f - 2.5f
   */
  set rainLevel(level) {
    let newLevel = level;
    if (level < 0.0) {
      newLevel = 0.0;
    } else if (level > 2.5) {
      newLevel = 2.5;
    }
    log.debug('changing rain level from ' + this.rainLevel + ' to ' + newLevel, 1);
    this._rainLevel = newLevel;
    vrp.player.manager.players.forEach(player => {
      player.world.rainLevel = this._rainLevel;
    });
  }

  /**
   * @type {Date}
   */
  get time() {
    return new Date();
  }

  /**
   * @type {float}
   */
  get timeScale() {
    return this._timeScale;
  }

  /**
   * Sets the time scale. Valid range: 0.0f - 1.0f
   *
   * @type {float} scale time scale from 0.0f - 1.0f
   */
  set timeScale(scale) {
    let newScale = scale;
    if (scale < 0.0 || scale > 1.0) {
      log.error('invalid time scale supplied (' + scale + ')');
      newScale = 1.0;
    }
    log.debug('changing time scale from ' + this.timeScale + ' to ' + newScale, 1);
    this._timeScale = newScale;
    log.implement('sync time scale change');
  }

  /**
   * Transitions a weather to another one. Very descriptive.
   *
   * @param {Object} trans desired transition (see world.transitions)
   * @param {function ()} done callback
   */
  transition(trans, done) {
    if (this.currentTransition.active) {
      log.error('trying to override current transition');
      return;
    }
    this.currentTransition = trans;
    this.currentTransition.active = true;
    this.currentTransition.from = this.weather;

    const it = this.currentTransition.runner.bind(this)();

    const fnRecv = timeout => {
      if (timeout === undefined) {
        this.currentTransition.active = false;
        log.debug('weather transition to ' + this.currentTransition.from + ' finished', 1);
        done();
      } else {
        setTimeout(() => {
          fnRecv(it.next().value);
        }, timeout);
      }
    };
    fnRecv(it.next().value);
  }

  /**
   * @type {int}
   */
  get weather() {
    return this._weather;
  }

  /**
   * Sets the weather and applies it to all connected players. Note that this will appear
   * as a weather transition in all clients which takes up to 12 minutes.
   *
   * @type {int} id weather ID from 1 - 12
   */
  set weather(id) {
    let newId = id;
    if (id === this._weather) {
      return;
    }
    if (id > 13 || id < 1) {
      log.error('world.weather: invalid weather id (' + id + ') supplied.');
      newId = 1;
    }
    log.debug('changing weather from ' + this.weather + ' to ' + newId, 1);
    this._weather = newId;
    vrp.player.manager.players.forEach(player => {
      player.world.weatherPersist = this._weather;
    });
  }

  /**
   * @type {int}
   */
  get weatherNow() {
    return this._weather;
  }

  /**
   * Sets the weather immediately and applies it to all connected players.
   *
   * @type {int} id weather id from 1 - 12
   */
  set weatherNow(id) {
    let newId = id;
    if (id > 13 || id < 1) {
      log.error('world.weatherNow: invalid weather id (' + id + ') supplied.');
      newId = 1;
    }
    log.debug('changing weather from ' + this.weather + ' to ' + newId + ' NOW', 1);
    this._weather = newId;
    vrp.player.manager.players.forEach(player => {
      player.world.weatherPersistNow = this._weather;
    });
  }

  /**
   * @type {float}
   */
  get windLevel() {
    return this._windLevel;
  }

  /**
   * Sets the windLevel. Valid range: 0.0f - 2.5f
   *
   * @type {float} level wind level from 0.0f - 2.5f
   */
  set windLevel(level) {
    let newLevel = level;
    if (level < 0.0) {
      newLevel = 0.0;
    } else if (level > 2.5) {
      newLevel = 2.5;
    }
    log.debug('changing wind level from ' + this.windLevel + ' to ' + newLevel, 1);
    this._windLevel = newLevel;
    vrp.player.manager.players.forEach(player => {
      player.world.windLevel = this._windLevel;
    });
  }
}

/**
 * World instance
 */
export default new World();
