/**
 * @title item.js
 * @overview Item Base Class
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

/**
 * An Item is something that can be stored inside Storages. It has certain attributes
 * and always inherits a descriptor. Items can have a usage function.
 */
export default class Item {
  /**
   * Creates a new Item and adds it to the stack
   *
   * @param {string|ItemDescriptor} _descriptor the item descriptor
   * @param {ItemStack|DBItemStack} stack the stack it belongs to.
   */
  constructor(_descriptor, stack) {
    const descriptor = global.package.manager.resolveDescriptor(_descriptor);
    /** @type {ItemDescriptor} */
    this.descriptor = descriptor;
    /** @type {DBItemStack|ItemStack} */
    this.stack = stack;
    /** @type {boolean} */
    this.used = false;
  }

  /**
   * @type {Object}
   */
  get attributes() {
    if (this.stack) {
      return this.stack.attributes;
    }
    return shared.utility.concat({}, this.descriptor.attributes);
  }

  /**
   * @type {string}
   */
  get name() {
    if (this.stack) {
      return this.stack.name;
    }
    return this.descriptor.name;
  }

  /**
   * Moves the Item to a different Stack
   *
   * @param {DBItemStack|ItemStack} stack - The new Stack the Item belongs to.
   *
   * @returns {boolean} Whether the Item could be moved.
   */
  move(stack) {
    if (this.stack) {
      this.stack.remove(this);
    }

    this.stack = stack;
    const r = this.stack.add(1, [this]);
    log.debug(`Item.move return: ${r}`);
    return r;
  }

  /**
   * Use the Item as a certain player. If the ItemDescriptor of the Item has a custom handler
   * for the 'use' event, it will be executed. This function is also being called from a Consumable
   * to handle deletion of the item.
   *
   * @param {Player} player - The player that uses the item.
   * @param {Object} [data=Object] - The data (only needed for Consumables).
   */
  use(player, data = {used: false, shouldDelete: false}) {
    let del = data;
    if (this.descriptor.hooks.has('use')) {
      const r = this.descriptor.invoke('use', player, this);
      if (r.length > 1) {
        log.warn('multiple returns in item.use not supported');
        del = r[0];
      } else if (r.length === 1) {
        del = r[0];
      }
    } else {
      if (typeof del === 'undefined') {
        return {used: false, shouldDelete: false};
      }
    }

    if (!del.used) {
      return del;
    }

    if (!this.descriptor.other.permanent) {
      this.used = true;
      if (del.shouldDelete) {
        if (this.stack) {
          this.stack.remove(this);
        }
      }
    }
    return del;
  }

  /**
   * @type {float}
   */
  get volume() {
    return this.descriptor.volume;
  }

  /**
   * @type {int}
   */
  get weight() {
    return this.descriptor.weight;
  }
}
