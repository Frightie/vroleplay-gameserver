/**
 * @title consumable.js
 * @overview Consumable Item Class
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

import Item from './item';

/**
 * A Consumable is an item that can be used and will be removed after it has been used (unless it is permanent).
 * It has one or more StatModifier's.
 */
export default class Consumable extends Item {
  /**
   * Creates a new Consumable
   *
   * @param {string|ItemDescriptor} _descriptor the item descriptor
   * @param {ItemStack|DBItemStack} stack the stack it belongs to.
   * @param {Object} [attributes = {}] the Consumable's attributes.
   */
  constructor(_descriptor, stack, attributes = {}) {
    const descriptor = global.package.manager.resolveDescriptor(_descriptor);
    super(descriptor, stack, attributes);

    /** @type {Array<StatModifier>} */
    this.modifiers = descriptor.modifiers;

    /** @type {boolean} */
    this.used = false;

    /** @type {function ()} */
    this.onUse = () => {};
  }

  /**
   * Use the Consumable
   *
   * @param {Player} player the target Player the Consumable is being used on.
   * @returns {boolean}
   */
  use(player) {
    if (this.used) {
      return false;
    }

    for (let i = 0; i < this.modifiers.length; i++) {
      this.modifiers[i].apply(player);
    }

    this.onUse(player);
    return super.use(player, {used: true, shouldDelete: typeof this.descriptor.other.permanent === 'undefined' || !this.descriptor.other.permanent });
  }
}
