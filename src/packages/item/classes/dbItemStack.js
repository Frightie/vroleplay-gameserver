/**
 * @title dbItemStack.js
 * @overview Database ItemStack Class
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */
import ItemStack from './itemStack';

/**
 * The DBItemStack is an owned, registered ItemStack loaded from the database.
 */
export default class DBItemStack extends ItemStack {
  /**
   * Create a new DBItemStack from a database model.
   *
   * @param {database.Item} dbModel - The Database Model for the Item Stack.
   * @param {boolean} [contentWillBeMoved=false] - Check this flag if the content already exists to reduce performance impact.
   * @param {Object} [attributes=undefined] - Custom attributes for the stack.
   */
  constructor(dbModel, contentWillBeMoved = false, attributes = undefined) {
    super(global.package.manager.getDescriptorById(dbModel.descriptor), dbModel.quantity, undefined, contentWillBeMoved, attributes);

    /** @type {database.Item} */
    this.dbModel = dbModel;

    if (typeof attributes !== 'undefined') {
      /** @type {Object} */
      this.attributes = this.dbModel.attributes = attributes;
    } else {
      /** @type {Object} */
      this.attributes = dbModel.attributes;
    }
    for (const property of Object.getOwnPropertyNames(this.descriptor.attributes)) {
      if (!this.attributes.hasOwnProperty(property)) {
        log.warn(`Missing Attribute ${property} in item ${this.dbModel.id}`);
        this.attributes[property] = this.descriptor.attributes[property];
      }
    }

    // Try to find the storage now
    /** @type {DBStorage} */
    this.storage = global.package.manager.storages.get(dbModel.storage);

    if (typeof this.storage === 'undefined') {
      throw new DatabaseError(`DBItemStack(id ${dbModel.id}) belongs to a unknown storage(id ${dbModel.id})`);
    }

    if (!contentWillBeMoved && this.descriptor.type === global.package.Weapon) {
      this.first().setDBModel(this.dbModel, this.storage.loaded);
    }
    this.descriptor.invoke('newDBStack', this);
  }

  /**
   * Creates a new DBItemStack from an array of items
   *
   * @param {Array<Item>} items - The existing items.
   * @param {Storage} storage - The owning Storage.
   * @returns {DBItemStack}
   */
  static fromItems(items, storage, oldStack = undefined, done = () => {}, attributes = undefined) {
    if (!items.length) {
      return false;
    }
    const descriptor = items[0].descriptor;

    let finalAttributes = attributes;
    if (typeof attributes === 'undefined') {
      if (oldStack) {
        finalAttributes = oldStack.attributes;
      } else {
        finalAttributes = shared.utility.concat({}, descriptor.attributes);
      }
    }

    for (let i = 0; i < items.length; i++) {
      if (items[i].descriptor !== descriptor) {
        log.warn(`Item descriptors are different in createDBStackFromItems (${descriptor.name} and ${items[i].descriptor.name})`);
        return false;
      }
    }

    vrp.database.Item.create({
      attributes: finalAttributes,
      storage: storage.dbModel.id,
      descriptor: descriptor.dbModel.id,
      quantity: items.length,
    }, item => {
      if (!item) {
        throw new DatabaseError('Could not create new ItemStack');
      }
      const stack = new DBItemStack(item, true);
      for (let i = 0; i < items.length; i++) {
        items[i].move(stack);
      }
      stack.move(storage);
      if (descriptor.type === global.package.Weapon) {
        stack.first().setDBModel(stack.dbModel, storage.loaded);
      }
      done(stack);
    });
    return true;
  }

  /**
   * Creates a new DBItemStack from an existing ItemStack
   *
   * @param {ItemStack} stack - The existing ItemStack.
   * @param {function(stack: DBItemStack)} done - Function to be called when the stack has been created.
   * @returns {DBItemStack}
   */
  static fromStack(stack, done) {
    vrp.database.Item.create({
      storage: stack.storage.dbModel.id,
      descriptor: stack.descriptor.dbModel.id,
      quantity: stack.quantity,
      attributes: stack.attributes,
    }, item => {
      if (!item) {
        throw new DatabaseError('Could not create new ItemStack');
      }
      const newstack = new DBItemStack(item, true);
      stack.items.forEach(itm => {
        itm.move(newstack);
      });
      newstack.move(stack.storage);
      if (newstack.descriptor.type === global.package.Weapon) {
        newstack.first().setDBModel(newstack.dbModel, stack.storage.loaded);
      }
      done(newstack);
    });
    return true;
  }

  /**
   * @type {Storage}
   */
  get storage() {
    return this._storage;
  }

  /**
   * Sets the Storage of the Stack and removes it from the old Storage, if it has been
   * previously assigned to a different Storage. Weapons are automatically unequipped.
   *
   * @type {Storage}
   */
  set storage(storage) {
    if (this._storage) {
      if (this.descriptor.type === global.package.Weapon && this._storage.owner.model === vrp.database.Character) {
        if (!this.first().unequip(this._storage.owner.getPlayer())) {
          log.warn(`Could not unequip Weapon ${this.dbModel.id}`);
        }
      }
      this._storage.remove(this);
    }

    this._storage = storage;
    if (this._storage) {
      this._storage.add(this);
      this.dbModel.storage = storage.dbModel.id;
      this.deleteIfEmpty();
      this.dbModel.save();
    } else {
      log.warn(`Stack ${this.dbModel.id}'s storage has been set to ${this._storage}`);
    }
    this.descriptor.invoke('dbStackStorageChanged', this, storage);
  }

  /**
   * @type {boolean}
   */
  get dropped() {
    return this.attributes.dropPosition !== null;
  }

  /**
   * @type {Vector3f}
   */
  get dropPosition() {
    return this.attributes.dropPosition;
  }

  /**
   * Sets the Position of where the Stack has been dropped.
   *
   * @type {Vector3f}
   */
  set dropPosition(pos) {
    this.attributes.dropPosition = pos;
  }

  /**
   * Adds the given Item(s) to the Stack.
   *
   * @param {int|Array<Item>} itemS - An amount of items or an array of Items that should be added.
   *
   * @returns {int} The amount of items that have been added.
   */
  add(itemS) {
    this.dontSave = true;
    const r = super.add(itemS);
    this.dontSave = false;
    if (this.dbModel) {
      this.dbModel.quantity = this.quantity;
      this.deleteIfEmpty();
      this.dbModel.save();
      this.descriptor.invoke('dbStackQuantityAdded', this);
    }
    return r;
  }

  /**
   * Destroys the DBItemStack and removes it from its storage.
   *
   */
  delete() {
    log.info(`Deleting DBItemStack ${this.dbModel.id}`);
    this.dbModel.destroy();
    this.items.forEach(item => {
      item.stack = undefined;
    });
    this.storage.remove(this);
  }

  /**
   * Checks whether the quantity of the Stack is 0 and if it is, deletes it.
   *
   * @private
   */
  deleteIfEmpty() {
    if (this.dbModel.quantity === 0) {
      this.delete();
    }
  }

  /**
   * Drops a Stack at the given position with the specified amount of quantity.
   *
   * @param {Vector3f} position - The drop position.
   * @param {function(stack: DBItemStack)} [done=function()] - Callback with the dropped stack.
   * @param {int} [quantity=1] - The amount of Items to drop. If it is higher than the Stack's quantity,
   *                             the operation fails.
   *
   * @returns {boolean} whether the Stack could be dropped or not.
   */
  drop(position, done = () => {}, quantity = 1) {
    if (quantity > this.quantity) {
      return false;
    }

    if (this.descriptor.type === global.package.Weapon) {
      this.first().unequip(this.storage.owner.getPlayer());
    }

    if (this.quantity === quantity) {
      this.attributes.dropPosition = position;
      this.move(global.package.globalStorage);
      done(this);
      return true;
    }

    this.popStack(quantity, global.package.globalStorage, done, shared.utility.concat({dropPosition: position}, this.attributes));
    return true;
  }

  /**
   * Let a player pick up this Stack if it has been dropped.
   *
   * @param {Player} player - The player that tries to pick up the storage. If the player does not have
   *                          room for the stack, the operation is cancelled.
   *
   * @returns {boolean} Whether the Stack could be picked up by the player.
   */
  pickup(player) {
    this.attributes.dropPosition = null;
    const storage = player.storages.getFreeStorage(this.descriptor, this.quantity, this.weight);

    if (typeof storage === 'undefined') {
      log.debug(`DBItemStack::pickup invalid storage (no free storage found)`, 2);
      return false;
    }

    return this.move(storage);
  }

  /**
   * Moves the given amount of Items to a different ItemStack.
   *
   * @param {ItemStack} stack - the new Stack for the items.
   * @param {int} [quantity=1] - the amount of Items that should be moved.
   *
   * @returns {boolean} Whether the items could be moved.
   */
  moveItems(stack, quantity = 1) {
    this.dbModel.dontSave = true;
    const retn = super.moveItems(stack, quantity);
    this.dbModel.dontSave = false;
    if (retn) {
      this.deleteIfEmpty();
      this.dbModel.save();
    }
    return retn;
  }

  /**
   * Pops the given amount of items.
   *
   * @param {int} [quantity = 1] - The amount of items to drop.
   *
   * @returns {Array<Item>}
   */
  pop(quantity = 1) {
    const item = super.pop(quantity);
    this.deleteIfEmpty();
    if (quantity) {
      this.dbModel.save();
    }
    return item;
  }

  /**
   * Pops the given amount of items and creates a new DBItemStack of it.
   * If the given quantity equals the Stack's quantity, no new stack is created. The
   * ownership will just be changed to the new Storage.
   *
   * @param {number} quantity - The amount of items to pop.
   * @param {DBStorage} storage - The new storage for the popped Stack.
   * @param {function(stack: DBItemStack)} [done=function()] - Callback for the completed function.
   * @param {Object} [attributes = undefined] - Custom attributes for the popped Stack.
   *
   * @returns {boolean} Whether the stack could be popped.
   */
  popStack(quantity, storage, done, attributes = undefined) {
    if (!quantity || !this.has(quantity)) {
      return false;
    }

    let compareAttributes = this.attributes;
    if (typeof attributes !== 'undefined') {
      compareAttributes = attributes;
    }

    if (storage.has(this.descriptor, compareAttributes)) {
      const stack = storage.getStacks(this.descriptor, compareAttributes)[0];
      if (stack.quantity < stack.descriptor.other.maxStackSize) {
        this.moveItems(stack, quantity);
        done(stack);
        return true;
      }
    }

    if (quantity === this.quantity) {
      log.debug(`Changing ownership of DBItemStack ${this.dbModel.id}`);
      // Just change ownership and attributes
      this.attributes = compareAttributes;
      this.move(storage);
      done(this);
      return true;
    }

    let items = this.pop(quantity);
    if (quantity === 1) {
      items = [items];
    }
    if (items.length === 0) {
      return false;
    }

    this.dbModel.dontSave = false;
    this.dbModel.save();
    return DBItemStack.fromItems(items, storage, this, done, compareAttributes);
  }

  /**
   * Removes the given amount, item Array or single Item from the stack.
   *
   * @param {int|Item|Array<Item>} quantityOrItemS - A amount of items, a single Item or Array of Items that
   *                                                 should be removed from the ItemStack.
   *
   * @returns {boolean} Whether the item(s) could be removed.
   */
  remove(quantityOrItemS) {
    const retn = super.remove(quantityOrItemS);
    this.dbModel.quantity = this.quantity;
    this.deleteIfEmpty();
    this.dbModel.save();
    this.descriptor.invoke('dbStackQuantityRemoved', this);
    return retn;
  }
}
