/**
 * @title dbStorage.js
 * @overview Database Storage Class
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

import Storage from './storage';

/**
 * The DBStorage is a Storage that is registered in the Database.
 */
export default class DBStorage extends Storage {
  /**
   * Creates a new DBStorage from a database.Storage model.
   *
   * @param {database.Storage} dbModel - The Database Model for the Storage.
   * @param {function()} done - Callback that is being called when the Content
   *                            for the Storage has been loaded.
   */
  constructor(dbModel, done) {
    super(global.package.manager.getDescriptorById(dbModel.descriptor));
    /** @type {database.Storage} */
    this.dbModel = dbModel;

    /** @type {boolean} */
    this.loaded = false;

    /** @type {StorageOwner} */
    this._owner = undefined;

    // Try to find the storage now
    switch (dbModel.owner_type) {
    case 'c':
      vrp.player.manager.players.forEach(player => {
        if (player.character !== null) {
          if (player.character.id === dbModel.owner) {
            this._owner = player.character;
          }
        }
      });
      break;
    case 'i':
      this._owner = global.package.manager;
      break;
    default:
      log.warn(`Unknown dbModel.owner_type in DBStorage(id ${dbModel.id}): ${dbModel.owner_type}>${dbModel.owner}`);
      break;
    }

    if (typeof this.owner === 'undefined') {
      throw new DatabaseError(`DBStorage(id ${dbModel.id}) belongs to a unknown owner(id ${dbModel.id})`);
    }

    /** @type {Object} */
    this.attributes = dbModel.attributes;
    for (const property of Object.getOwnPropertyNames(this.descriptor.attributes)) {
      if (!this.attributes.hasOwnProperty(property)) {
        log.warn(`Missing Attribute ${property} in storage ${this.dbModel.id}`);
        this.attributes[property] = this.descriptor.attributes[property];
      }
    }

    global.package.manager.storages.set(dbModel.id, this);
    this._loadContent(done);
  }

  /**
   * Loads the Content of the Storage from the Database
   *
   * @private
   * @param {function()} done - Callback when the loading finished.
   */
  _loadContent(done = () => {}) {
    vrp.database.Item.find({storage: this.dbModel.id}, items => {
      if (typeof items === 'undefined') {
        return;
      }

      // Now lets create some stacks
      for (let i = 0; i < items.length; i++) {
        // Get the descriptor
        const descriptor = global.package.manager.getDescriptorById(items[i].descriptor);
        if (this.has(descriptor, items[i].attributes)) {
          const stack = this.getStacks(descriptor, items[i].attributes)[0];
          if (stack) {
            log.warn(`Storage.loadContent: Descriptor already exists. Adding quantity to it and deleting the database model.`);
            stack.add(items[i].quantity);
            items[i].destroy();
            done();
            continue;
          }
        }
        const stack = new global.package.DBItemStack(items[i]);
        if (!this.add(stack)) {
          log.error(`Could not add item ${items[i].id}(${descriptor.name}) to Storage of ${this.owner.name}(${this.owner.id})`);
          this.owner.getPlayer().sendError(`There has been an internal error while retrieving your Storage. Please consult an Administrator. Error: STORAGE_ADD(${stack.dbModel.id} => ${this.dbModel.id})`);
          stack.storage = null;
        }
      }
      done();
      this._invoke('finishedLoading');
      this.loaded = true;

      if (this.contentVolume > this.capacity) {
        log.error(`STORAGE OVERWEIGHT: ${this.contentVolume}/${this.capacity} in Storage ${this.dbModel.id}, owned by ${this.owner.name}(${this.owner.id}).`);
        log.info(`Kicking the player. This needs manual resolve. Message has been posted to Slack.`);
        this.owner.getPlayer().Kick(`INTERNAL ERROR: Please consult an admin. Error: STORAGE_OVERWEIGHT(${this.dbModel.id}`);
      }
    });
  }

  /**
   * Adds the given ItemStack to the Storage. If a similar stack (matching descriptor & attributes)
   * exists, the Stacks will be merged and the provided one will be deleted. If a ItemStack and not
   * a DBItemStack is provided, it will be converted into a DBItemStack.
   *
   * @param {ItemStack} itemStack - The Stack that should be added.
   * @param {function()} done - Callback when the action finished.
   *
   * @returns {boolean} Whether the Stack could be added or not.
   */
  add(itemStack, done = () => {}) {
    if (!(itemStack instanceof global.package.DBItemStack)) {
      return global.package.DBItemStack.fromStack(itemStack, done);
    }
    const retn = super.add(itemStack);
    if (retn) {
      this.descriptor.invoke('dbStorageAddStack', this, itemStack);
    }
    done(itemStack);
    return retn;
  }

  /**
   * Removes the given Stack from the Storage.
   *
   * @param {DBItemStack} itemStack - The item stack that should be removed
   *
   * @returns {boolean} Whether the stack could be removed.
   */
  remove(itemStack) {
    const retn = super.remove(itemStack);
    if (retn) {
      this.descriptor.invoke('dbStorageRemoveStack', this, itemStack);
    }
    return retn;
  }

  /**
   * @type {StorageOwner}
   */
  get owner() {
    return this._owner;
  }

  /**
   * @type {StorageOwner}
   */
  set owner(owner) {
    this._owner = owner;
    this.dbModel.owner = owner.id;
    this.dbModel.save();
  }
}
