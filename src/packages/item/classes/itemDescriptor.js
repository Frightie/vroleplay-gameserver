/**
 * @title consumable.js
 * @overview Consumable Item Class
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

/**
 * An ItemDescriptor describes the basic Information for an Item. It contains
 * information like the name, description, weight and default attributes of an Item.
 * Item descriptors are saved in the database and loaded from the ItemManager at startup.
 */
export default class ItemDescriptor {
  /**
   * Creates a new ItemDescriptor from the given Database Model.
   *
   * @param {database.ItemDescriptor} dbModel - The Database Model for the Descriptor.
   */
  constructor(dbModel) {
    /** @type {database.ItemDescriptor} */
    this.dbModel = dbModel;
    this.dbModel.attributes.dropPosition = null;

    /** @type {Map<string, Set>} */
    this.hooks = new Map();

    /** @type {Consumable|DBItemStack|DBStorage|Item|ItemStack|Storage|Weapon} */
    this._type = global.package.manager.resolveType(this.dbModel.type);

    if (typeof this._type === 'undefined') {
      throw new DatabaseError(`Misconfigured ItemDescriptor '${this.dbModel.name}'. Invalid Type '${this.dbModel.type}'`);
    }

    // Resolve Modifiers
    /** @type {Array<StatModifier|Boost>} */
    this.modifiers = [];
    for (let i = 0; i < this.dbModel.other.modifiers.length; i++) {
      if (this.dbModel.other.modifiers[i].boost) {
        this.modifiers.push(shared.Boost.findOrCreate(this.dbModel.other.modifiers[i].stat, this.dbModel.other.modifiers[i].data, { boost: this.dbModel.other.modifiers[i].boost }));
      } else {
        this.modifiers.push(shared.StatModifier.findOrCreate(this.dbModel.other.modifiers[i].stat, this.dbModel.other.modifiers[i].data));
      }
    }

    // Weapon System stuff
    const recountAmmo = stack => {
      if (stack.storage.owner.model !== vrp.database.Character) {
        return;
      }
      const weapon = stack.storage.owner.getPlayer().vrpEquippedWeapon;
      if (weapon) {
        if (weapon.ammoDescriptor === stack.descriptor) {
          weapon.recountAmmo();
        } else if (weapon.descriptor.other.throwable) {
          weapon.recountAmmo();
        }
      }
    };

    if (this._type === global.package.Weapon) {
      const fnToggleEquip = (player, weapon) => {
        if (weapon.attributes.equipped) {
          if (weapon.unequip(player)) {
            player.sendSuccess(`Unequipped ${weapon.stack.name}.`);
            return {used: true, shouldDelete: false};
          }
          player.sendError(`Could not unequip ${weapon.stack.name}.`);
          return {used: false, shouldDelete: false};
        }

        if (weapon.equip(player)) {
          player.sendSuccess(`Equipped ${weapon.stack.name}.`);
          return {used: true, shouldDelete: false};
        }
        player.sendError(`Could not equip ${weapon.stack.name}.`);
        return {used: false, shouldDelete: false};
      };

      this.on('use', fnToggleEquip);
      this.on('dbStackQuantityRemoved', stack => {
        if (stack.quantity === 0) {
          stack.deleteIfEmpty();
        }
      });
    } else if (this.other.magazine) {
      const getMagWeight = stack => stack.descriptor.dbModel.weight + stack.quantity * stack.descriptor.dbModel.other.bulletWeight * stack.attributes.bullets;
      const getMagName = stack => `${stack.descriptor.name} (${stack.attributes.bullets === 0 ? 'Empty' : `${stack.attributes.bullets} Bullets`})`;

      this.on('getStackWeight', getMagWeight);
      this.on('newDBStack', recountAmmo);
      this.on('dbStackQuantityAdded', recountAmmo);
      this.on('dbStackQuantityRemoved', recountAmmo);
      this.on('dbStackOwnerChanged', recountAmmo);
      if (this.name !== 'RPG Rocket' && this.name !== 'Flare') {
        this.on('getName', getMagName);
      }
    } else if (this.other.throwable) {
      this.on('dbStackQuantityAdded', recountAmmo);
      this.on('dbStackQuantityRemoved', recountAmmo);
      this.on('dbStackOwnerChanged', recountAmmo);
      this.on('newDBStack', recountAmmo);
    } else if (this.other.component) {
      const equipComponent = (player, component) => {
        if (!player.vrpEquippedWeapon) {
          player.sendError(`You dont have equipped any weapon.`);
          return {used: false, shouldDelete: false};
        }

        // Check if the weapon is suitable
        const weaponComponents = new Set(player.vrpEquippedWeapon.attributes.components);
        for (let i = 0; i < component.descriptor.other.hashes.length; i++) {
          for (let i2 = 0; i2 < component.descriptor.other.hashes[i].weapons.length; i2++) {
            if (component.descriptor.other.hashes[i].weapons[i2] !== player.vrpEquippedWeapon.descriptor.dbModel.id) {
              continue;
            }
            if (weaponComponents.has(component.descriptor.dbModel.id)) {
              player.sendError(`You already have a ${component.stack.name} equipped on your ${player.vrpEquippedWeapon.stack.name}.`);
              return {used: false, shouldDelete: false};
            }

            player.vrpEquippedWeapon.addComponent(player, component, component.descriptor.other.hashes[i].hash);
            player.sendSuccess(`Attached ${component.stack.name} to your ${player.vrpEquippedWeapon.stack.name}.`);
            return {used: true, shouldDelete: true};
          }
        }
        return {used: false, shouldDelete: false};
      };
      this.on('use', equipComponent);
    }

    log.debug(`Created ItemDescriptor '${this.name}', type '${this.dbModel.type}'.`, 3);
  }

  /**
   * Invokes the given Event.
   *
   * @param {string} event - The name of the Event.
   * @param {...*} ...args - Arguments.
   *
   * @returns {Array<*>} Event Handler result.
   */
  invoke(event, ...args) {
    if (this.hooks.has(event)) {
      const retn = [];
      this.hooks.get(event).forEach(handler => {
        retn.push(handler(...args));
      });
      return retn;
    }
  }

  /**
   * Creates a new Item of this Descriptor and assigns it to the given ItemStack.
   *
   * @param {ItemStack} stack - The owning Stack.
   *
   * @returns {Item} the created Item.
   */
  create(stack) {
    log.debug(`Instantiating new ${this.dbModel.type} from ItemDescriptor '${this.name}'.`, 3);
    return new this._type(this, stack);
  }

  /**
   * Creates a new ItemStack of the Descriptor with the given amount and assigns it to the given Storage.
   *
   * @param {Storage} storage - The Storage the Stack should be assigned to.
   * @param {int} quantity - The amount of Items.
   *
   * @returns {ItemStack} The created ItemStack.
   */
  createStack(storage, quantity) {
    log.debug(`Instantiating new Stack of ${this.dbModel.type} from ItemDescriptor '${this.name}'.`, 3);
    return new global.package.ItemStack(this, storage, quantity);
  }

  /**
   * @type {Object}
   */
  get attributes() {
    return this.dbModel.attributes;
  }

  /**
   * @type {string}
   */
  get description() {
    return this.dbModel.description;
  }

  /**
   * @type {string}
   */
  get name() {
    return this.dbModel.name;
  }

  /**
   * Adds a new Handler for an event.
   *
   * @param {string} event - The name of the event.
   * @param {function()} handler - The handler for the event.
   */
  on(event, handler) {
    if (!this.hooks.has(event)) {
      this.hooks.set(event, new Set());
    }
    this.hooks.get(event).add(handler);
  }

  /**
   * @type {Object}
   */
  get other() {
    return this.dbModel.other;
  }

  /**
   * @type {float}
   */
  get size() {
    return this.dbModel.volume;
  }

  /**
   * Returns a string representation of the Item's size, can be used to print it or
   * send it to a Player.
   *
   * @type {string}
   */
  get sizeName() {
    return 'TODO: size association';
  }

  /**
   * @type {Consumable|DBItemStack|DBStorage|Item|ItemStack|Storage|Weapon}
   */
  get type() {
    return this._type;
  }

  /**
   * @type {float}
   */
  get volume() {
    return this.dbModel.volume;
  }

  /**
   * @type {int}
   */
  get weight() {
    const r = this.invoke('getWeight', this);
    if (typeof r === 'undefined' || r.length === 0) {
      return this.dbModel.weight;
    }
    if (r.length > 1) {
      log.warn('multiple getWeight not supported yet');
    }
    return r[0];
  }
}
