/**
 * @title weapon.js
 * @overview Weapon Item Class
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

import Item from './item';
import DBItemStack from './dbItemStack';

/**
 * A Weapon is a Item that can be equipped by a Player.
 * It generally has a magazine unless it is a Melee weapon or a throwable weapon.
 * Weapons need Database Models.
 */
export default class Weapon extends Item {
  /**
   * Creates a new Weapon
   *
   * @param {string|ItemDescriptor} descriptor - The Item Descriptor.
   * @param {DBItemStack} stack - The ItemStack the Weapon belongs to.
   */
  constructor(descriptor, stack) {
    super(descriptor, stack);

    /** @type {int} */
    this.ammo = 0;

    /** @type {undefined|ItemDescriptor} */
    this.ammoDescriptor = global.package.manager.getDescriptorById(this.descriptor.other.magazine);

    if (!(stack instanceof DBItemStack)) {
      throw new FatalError(`Constructed a Weapon inside a normal ItemStack.`);
    }

    if (typeof this.ammoDescriptor === 'undefined' && !this.descriptor.other.noAmmo) {
      throw new FatalError(`Invalid ammo descriptor for Weapon ${this.descriptor.name}`);
    }
  }

  /**
   * @type {boolean}
   */
  get didFirstEquip() {
    return this.stack.didFirstEquip;
  }

  /**
   * @type {boolean}
   */
  set didFirstEquip(value) {
    this.stack.didFirstEquip = value;
  }

  /**
   * @type {Object}
   */
  get attributes() {
    return this.stack.attributes;
  }

  /**
   * Sets the Database Model of the Weapon and if needed recounts the Ammo for the Weapon.
   *
   * @param {database.Item} model - Database Model for the Weapon.
   * @param {boolean} [countNow=false] - Flag whether the Ammo should be recounted.
   */
  setDBModel(model, countNow = false) {
    this.dbModel = model;
    if (!countNow) {
      this.stack.storage.on('finishedLoading', this.recountAmmo.bind(this));
    }
  }

  /**
   * Equips a Weapon to the player.
   *
   * @param {Player} player - The Player that should equip this weapon. Has to be the
   *                          owner of the Weapon's storage.
   *
   * @returns {boolean} Whether the Weapon could be equipped or not.
   */
  equip(player) {
    if (this.stack.storage.owner !== player.character) {
      log.error(`Invalid player tries to equip weapon`);
      return false;
    }

    if (player.vrpEquippedWeapon) {
      player.vrpEquippedWeapon.unequip(player);
    }

    const hash = shared.utility.findWeapon(this.descriptor.other.identifier).hash;

    this.attributes.equipped = true;

    this.dbModel.save();
    player.debug(`Adding ${this.descriptor.name} with ${this.ammo} ammo.`);
    player.AddWeapon(hash, 0, true);
    player.vrpEquippedWeapon = this;
    this.didFirstEquip = true;
    this.addComponents(player);
    this.applyTint(player);
    this._countAmmo();
    return true;
  }

  /**
   * Adds the Components the weapon has to the player.
   *
   * @param {Player} player - The Player that should get the components.
   */
  addComponents(player) {
    if (!this.attributes.equipped) {
      return false;
    }
    const weapon = shared.utility.findWeapon(this.descriptor.other.identifier).hash;
    for (let i = 0; i < this.attributes.components.length; i++) {
      const hash = global.package.manager.getComponentHashForWeapon(this.descriptor.dbModel.id, this.attributes.components[i]);

      if (typeof hash === 'undefined') {
        player.sendError(`Could not attach Weapon Component ${this.attributes.components[i]} to Weapon ${this.stack.name}`);
        log.error(`Could not attach Weapon Component ${this.attributes.components[i]} to Weapon ${this.stack.name}(${this.dbModel.id}. Player: ${player.character.name}(${player.character.dbModel.id}`);
      }

      player.SetWeaponComponent(weapon, hash);
    }
  }

  /**
   * Adds the given component to the Weapon.
   *
   * @param {Player} player - The targeted player.
   * @param {Item} component - The Weapon Component.
   * @param {int} hash - The Component's hash.
   */
  addComponent(player, component, hash) {
    if (!this.attributes.equipped) {
      return false;
    }
    player.SetWeaponComponent(shared.utility.findWeapon(this.descriptor.other.identifier).hash, hash);
    this.attributes.components.push(component.descriptor.dbModel.id);
    this.dbModel.save();
  }

  /**
   * Applies the Tint of the Weapon to the given player. The
   * Weapon needs to be equipped in order to apply the tint.
   *
   * @param {Player} player - The Player which should get the weapon tinted.
   *
   * @returns {boolean} Whether the Tint could be applied.
   */
  applyTint(player) {
    if (!this.attributes.equipped) {
      return false;
    }

    const hash = shared.utility.findWeapon(this.descriptor.other.identifier).hash;
    player.SetWeaponTint(hash, this.attributes.tint);
    return true;
  }

  /**
   * Removes this Weapon from a Player, even if it is not equipped.
   *
   * @param {Player} player - The Player that should be unequipped.
   *
   * @returns {boolean}
   */
  bulkUnequip(player) {
    const hash = shared.utility.findWeapon(this.descriptor.other.identifier).hash;
    player.SetWeaponAmmo(hash, 0);
    player.ResetWeaponComponents(hash);
    player.SetWeaponTint(hash, 0);
    player.RemoveWeapon(hash);
    player.vrpEquippedWeapon = null;
    return true;
  }

  /**
   * Unequips the Weapon from a Player. The Player has to be the owner of the
   * Weapon's Storage.
   */
  unequip(player) {
    if (this.stack.storage.owner !== player.character) {
      log.error(`Invalid player tries to unequip weapon`);
      return false;
    }

    const hash = shared.utility.findWeapon(this.descriptor.other.identifier).hash;
    player.SetWeaponAmmo(hash, 0);
    player.ResetWeaponComponents(hash);
    player.SetWeaponTint(hash, 0);
    player.RemoveWeapon(hash);

    this.attributes.equipped = false;
    this.dbModel.save();
    player.vrpEquippedWeapon = null;
    return true;
  }

  /**
   * Removes the given Component from the Weapon and the Player.
   *
   * @param {Player} player - The Player that should unequip the Component.
   * @param {int|Item} hashOrComponent - The Component hash or the Component (Item).
   *
   * @returns {boolean} Whether the Component could be removed.
   */
  removeComponent(player, hashOrComponent) {
    if (isNaN(hashOrComponent)) {
      const hash = global.package.manager.getComponentHashForWeapon(this.descriptor.dbModel.id, hashOrComponent.dbModel.id);

      player.RemoveWeaponComponent(shared.utility.findWeapon(this.descriptor.other.identifier).hash, hash);

      // Find it in our actual component list
      let idx;
      for (let i = 0; i < this.attributes.components.length; i++) {
        if (this.attributes.components[i] === hashOrComponent.dbModel.id) {
          idx = i;
          break;
        }
      }
      if (typeof idx === 'undefined') {
        log.warn(`Removed unknown Component from Weapon ${this.dbModel.id}. Hash: ${hash}`);
        return false;
      }

      this.attributes.components.splice(idx, 1);
      this.dbModel.save();

      global.package.createStack(hashOrComponent, player.storages.getFreeStorage(hashOrComponent), 1);
      return true;
    }
    // Safe-Remove
    player.RemoveWeaponComponent(shared.utility.findWeapon(this.descriptor.other.identifier).hash, hashOrComponent);
    return true;
  }

  /**
   * Sets the Tint of the Weapon.
   *
   * @param {int} index - The Weapon Tint index.
   * @param {Player} [player=undefined] - If the weapon is carried, apply the tint to this player's
   *                                      Weapon too.
   *
   * @returns {boolean} Whether the Tint could be set.
   */
  setTint(index, player = undefined) {
    if (!this.attributes.equipped) {
      return false;
    }
    if (!player || player.character !== this.stack.storage.owner) {
      log.warn(`Trying to set weapon ${this.dbModel.id} tint for player ${player.character.id}, but the owner is ${this.stack.storage.owner.id}.`);
      return false;
    }

    this.attributes.tint = index;
    this.dbModel.save();
    this.applyTint(player);
    return true;
  }

  /**
   * Recount the Ammo for the Weapon based on Magazines or Quantity.
   *
   * @private
   */
  _countAmmo() {
    if (!this.attributes.equipped) {
      return;
    }
    const player = this.stack.storage.owner.getPlayer();
    this.ammo = 0;
    // We have to use a refreshed player here because otherwise the Server crashes. I dont know why though.

    if (!this.didFirstEquip) {
      this.didFirstEquip = true;
      this.equip(player);
      return;
    }

    if (this.descriptor.other.melee) {
      this.ammo = 1;
      return;
    }

    log.debug('Counting Ammo');
    const hash = shared.utility.findWeapon(this.descriptor.other.identifier).hash;

    if (this.descriptor.other.throwable) {
      this.ammo = this.stack.quantity;
      player.SetWeaponAmmo(hash, this.stack.quantity);
      return;
    }


    const magazines = player.storages.getStacks(this.ammoDescriptor);

    for (let i = 0; i < magazines.length; i++) {
      this.ammo += magazines[i].quantity * magazines[i].attributes.bullets;
    }

    if (this.ammo === 0) {
      player.debug(`You dont have any Magazines for ${this.descriptor.name}. Needed MagType: ${this.ammoDescriptor.name}`);
      player.SetWeaponAmmo(hash, 0);
      return;
    }

    player.SetWeaponAmmo(hash, this.ammo);
  }

  /**
   * Recount the Ammonition for the Weapon.
   */
  recountAmmo() {
    log.debug('Recounting Ammo for Weapon', 4);
    if (!this.stack.storage) {
      return;
    }

    if (this.stack.storage.owner && this.stack.storage.owner.model === vrp.database.Character) {
      this._countAmmo();
    }
  }
}
