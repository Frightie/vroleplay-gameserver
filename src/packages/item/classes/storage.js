/**
 * @title storage.js
 * @overview Storage Class
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

/** @typedef {ItemManager|database.Character} StorageOwner */

/**
 * A Storage holds multiple ItemStacks in it. A Storage always has
 * a specified capacity of weight it can hold.
 */
export default class Storage {
  /**
   * Creates a new Storage
   *
   * @param {string|ItemDescriptor} _descriptor - The Descriptor for the Storage.
   * @param {StorageOwner} owner - The owner of the Storage.
   */
  constructor(_descriptor, owner) {
    const descriptor = global.package.manager.resolveDescriptor(_descriptor);
    if (typeof descriptor === 'undefined') {
      throw new ScriptError(`Storage descriptor is not defined: ${_descriptor}`);
    }
    /** @type {ItemDescriptor} */
    this.descriptor = descriptor;

    /** @type {StorageOwner} */
    this._owner = owner;

    /** @type {Set<ItemStack>} */
    this.content = new Set();

    /** @type {Map<string, Set>} */
    this.hooks = new Map();
  }

  /**
   * Checks whether the Storage can carry any item of the given descriptor size-wise.
   *
   * @param {ItemDescriptor} descriptor - The Descriptor in question.
   *
   * @returns {boolean} Whether the Storage can carry any items of the size.
   */
  canCarry(descriptor) {
    return this.capacity >= descriptor.volume;
  }

  /**
   * Invokes an Event for the Storage.
   *
   * @private
   * @param {string} event - The Name of the Event.
   * @param {...*} args - Event Arguments.
   *
   * @returns {Array<*>} Event handler return values.
   */
  _invoke(event, ...args) {
    if (this.hooks.has(event)) {
      const retn = [];
      this.hooks.get(event).forEach(handler => {
        retn.push(handler(...args));
      });
      return retn;
    }
  }

  /**
   * Adds the given ItemStack to the Storage. If a similar stack (matching descriptor & attributes)
   * exists, the Stacks will be merged.
   *
   * @param {ItemStack} itemStack - The Stack that should be added.
   *
   * @returns {boolean} Whether the Stack could be added or not.
   */
  add(itemStack) {
    if (!this.hasRoomFor(itemStack.descriptor, itemStack.quantity)) {
      log.debug(`Storage does not have Room for ItemStack.`, 4);
      return false;
    }

    if (this.has(itemStack.descriptor, itemStack.attributes, false) && this.getStacks(itemStack.descriptor, itemStack.attributes)[0] !== itemStack) {
      const stacks = this.getStacks(itemStack.descriptor, itemStack.attributes);

      for (let i = 0; i < stacks.length; i++) {
        if (stacks[i].full) {
          continue;
        }
        log.debug(`Adding ${itemStack.quantity} items.`, 4);
        return itemStack.moveItems(stacks[i], itemStack.quantity);
      }
    }

    return this.content.add(itemStack);
  }

  /**
   * @type {float}
   */
  get capacity() {
    return this.descriptor.other.capacity;
  }

  /**
   * Computed Description, either from the Descriptor or a custom function
   * (via on('getDescription')).
   *
   * @type {string}
   */
  get description() {
    const desc = this.descriptor.invoke('getDescription', this);
    if (typeof desc === 'undefined') {
      return this.descriptor.description;
    }

    if (desc.length > 0) {
      log.warn('getDescription(Storage): multi-return not supported');
    }
    return desc[0];
  }

  /**
   * @type {boolean}
   */
  get full() {
    return this.contentVolume >= this.capacity;
  }

  /**
   * Computed Name, either from the Descriptor or a custom function (via
   * on('getName')).
   *
   * @type {string}
   */
  get name() {
    const desc = this.descriptor.invoke('getName', this);
    if (typeof desc === 'undefined') {
      return this.descriptor.name;
    }

    if (desc.length > 0) {
      log.warn('getName(Storage): multi-return not supported');
    }
    return desc[0];
  }

  /**
   * Checks whether the Storage has a Stack of the given Descriptor and matching attributes.
   *
   * @see {@link ItemStack#matchAttributes}
   *
   * @param {string|ItemDescriptor} _descriptor - The Item Descriptor.
   * @param {Object} [attributes=Object] - The attributes to check against.
   *
   * @returns {boolean} Whether the Storage has any Stacks of the descriptor and attributes.
   */
  has(_descriptor, attributes = {}) {
    const descriptor = global.package.manager.resolveDescriptor(_descriptor);
    for (const stack of this.content.values()) {
      if (stack.descriptor === descriptor && stack.matchAttributes(attributes)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Checks whether the Storage has any Stacks of the given Descriptor.
   *
   * @param {string|ItemDescriptor} _descriptor - The Item Descriptor.
   *
   * @returns {boolean} Whether the Storage has any Stacks of the descriptor.
   */
  hasAny(_descriptor) {
    const descriptor = global.package.manager.resolveDescriptor(_descriptor);
    for (const stack of this.content.values()) {
      if (stack.descriptor === descriptor) {
        return true;
      }
    }
    return false;
  }

  /**
   * Checks whether the Storage has any room for a certain quantity of a Descriptor.
   *
   * @param {string|ItemDescriptor} _descriptor - The Item Descriptor.
   * @param {int} [quantity=1] - The quantity the Storage should have room for.
   *
   * @returns {boolean} Whether the Storage has enough free room left.
   */
  hasRoomFor(_descriptor, quantity = 1) {
    const descriptor = global.package.manager.resolveDescriptor(_descriptor);

    if (!this.canCarry(descriptor)) {
      return false;
    }

    return this.capacity - this.contentVolume >= descriptor.volume * quantity;
  }

  /**
   * Gets the first Item of the first Stack with the given Descriptor and attributes.
   *
   * @see {@link ItemStack#matchAttributes}
   *
   * @param {string|ItemDescriptor} _descriptor - The Item Descriptor.
   * @param {Object} [attributes=Object] - The attributes to check against.
   *
   * @returns {undefined|Item} The first item or undefined if there is none.
   */
  get(_descriptor, attributes = {}) {
    const descriptor = global.package.manager.resolveDescriptor(_descriptor);
    if (this.has(descriptor, attributes)) {
      return this.getStacks(descriptor, attributes)[0].first();
    }
  }

  /**
   * Gets the first Item of the first Stack with the given Descriptor.
   *
   * @see {@link ItemStack#matchAttributes}
   *
   * @param {string|ItemDescriptor} _descriptor - The Item Descriptor.
   *
   * @returns {undefined|Item} The first item or undefined if there is none.
   */
  getAny(_descriptor) {
    const descriptor = global.package.manager.resolveDescriptor(_descriptor);
    if (this.hasAny(descriptor)) {
      return this.getAnyStacks(descriptor)[0].first();
    }
  }

  /**
   * Gets all available Stacks that match the given Descriptor and attributes.
   *
   * @see {@link ItemStack#matchAttributes}
   *
   * @param {string|ItemDescriptor} _descriptor - The Item Descriptor.
   * @param {Object} [attributes=Object] - The attributes to check against.
   *
   * @returns {Array<ItemStack>}
   */
  getStacks(_descriptor, attributes = {}) {
    const stacks = [];
    const descriptor = global.package.manager.resolveDescriptor(_descriptor);
    for (const stack of this.content.values()) {
      if (stack.descriptor === descriptor && stack.matchAttributes(attributes)) {
        stacks.push(stack);
      }
    }
    return stacks;
  }

  /**
   * Gets all available Stacks that match the given Descriptor.
   *
   * @param {string|ItemDescriptor} _descriptor - The Item Descriptor.
   *
   * @returns {Array<ItemStack>}
   */
  getAnyStacks(_descriptor) {
    const stacks = [];
    const descriptor = global.package.manager.resolveDescriptor(_descriptor);
    for (const stack of this.content.values()) {
      if (stack.descriptor === descriptor) {
        stacks.push(stack);
      }
    }
    return stacks;
  }

  /**
   * Checks whether there are any stacks that exist multiple times and merges them
   * into one stack.
   */
  merge() {
    this.content.forEach(stack => {
      const stacks = this.getStacks(stack.descriptor, stack.attributes);
      if (stacks.length > 1) {
        for (let i = 1; i < stacks.length; i++) {
          stacks[i].moveItems(stacks[0], stacks[i].quantity);
        }
      }
    });
  }

  /**
   * Registers a handler for an Event.
   *
   * @param {string} event - The name of the Event.
   * @param {function(args: ...*)} handler - The handler for the Event.
   */
  on(event, handler) {
    if (!this.hooks.has(event)) {
      this.hooks.set(event, new Set());
    }
    this.hooks.get(event).add(handler);
  }

  /**
   * @type {StorageOwner}
   */
  get owner() {
    return this._owner;
  }

  /**
   * @type {StorageOwner}
   */
  set owner(owner) {
    this._owner = owner;
  }

  /**
   * Pops a Item of the given Descriptor and attributes.
   *
   * @param {string|ItemDescriptor} _descriptor - The Item Descriptor.
   * @param {Object} [attributes=Object] - The Attributes to check against.
   *
   * @returns {undefined|Item}
   */
  pop(_descriptor, attributes = {}) {
    const descriptor = global.package.manager.resolveDescriptor(_descriptor);
    for (const stack of this.content.values()) {
      if (stack.descriptor === descriptor && stack.matchAttributes(attributes)) {
        return stack.pop();
      }
    }
  }

  /**
   * Pops a Item of the given Descriptor.
   *
   * @param {string|ItemDescriptor} _descriptor - The Item Descriptor.
   *
   * @returns {undefined|Item}
   */
  popAny(_descriptor) {
    const descriptor = global.package.manager.resolveDescriptor(_descriptor);
    for (const stack of this.content.values()) {
      if (stack.descriptor === descriptor) {
        return stack.pop();
      }
    }
  }

  /**
   * Pops a ItemStack of the given Descriptor and attributes if it exists in the Storage.
   *
   * @param {string|ItemDescriptor} _descriptor - The Item Descriptor.
   * @param {Object} attributes - The Attributes to check against.
   * @param {int} quantity - The amount of Items to drop.
   * @param {Storage} storage - The new Storage for the popped Stack.
   * @param {function(stack: ItemStack)} done - Callback when the action finished.
   *
   * @returns {boolean} Whether the stack could be popped.
   */
  popStack(_descriptor, attributes, quantity, storage, done) {
    const descriptor = global.package.manager.resolveDescriptor(_descriptor);
    if (this.has(descriptor, attributes)) {
      return this.getStacks(descriptor, attributes)[0].popStack(quantity, storage, done);
    }
    return false;
  }

  /**
   * Pops a ItemStack of the given Descriptor if it exists in the Storage.
   *
   * @param {string|ItemDescriptor} _descriptor - The Item Descriptor.
   * @param {int} quantity - The amount of Items to drop.
   * @param {Storage} storage - The new Storage for the popped Stack.
   * @param {function(stack: ItemStack)} done - Callback when the action finished.
   *
   * @returns {boolean} Whether the stack could be popped.
   */
  popAnyStack(_descriptor, quantity, storage, done) {
    const descriptor = global.package.manager.resolveDescriptor(_descriptor);
    if (this.hasAny(descriptor)) {
      return this.getAnyStacks(descriptor)[0].popStack(quantity, storage, done);
    }
    return false;
  }

  /**
   * Removes the given Stack from the Storage.
   *
   * @param {ItemStack} itemStack - The item stack that should be removed
   *
   * @returns {boolean} Whether the stack could be removed.
   */
  remove(itemStack) {
    return this.content.delete(itemStack);
  }

  /**
   * @type {int}
   */
  get weight() {
    let weight = this.descriptor.weight;
    this.content.forEach(item => {
      weight += item.weight;
    });

    return weight;
  }

  /**
   * @type {float}
   */
  get contentVolume() {
    let volume = 0.0;
    this.content.forEach(stack => {
      volume += stack.volume;
    });
    return volume;
  }

  /**
   * @type {int}
   */
  get contentWeight() {
    return this.weight - this.descriptor.weight;
  }
}
