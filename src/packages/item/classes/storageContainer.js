/**
 * @title storageContainer.js
 * @overview Storage Container Class. Contains one or multiple storages.
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

/**
 * A StorageContainer can manage multiple Storages and mainly just provides a
 * interface similar to a normal Storage.
 */
export default class StorageContainer {
  /**
   * Create a new StorageContainer
   */
  constructor() {
    /** @type {Set<Storage>} */
    this.storages = new Set();
  }

  /**
   * Adds a Storage
   *
   * @param {Storage} storage - The Storage that should be added.
   *
   * @returns {boolean} Whether the Storage could be added or not.
   */
  add(storage) {
    for (const strg of this.storages.values()) {
      if (storage.descriptor === strg.descriptor) {
        log.warn(`Tried to add second Storage of ${strg.descriptor.name}`);
        return false;
      }
    }
    return this.storages.add(storage);
  }

  /**
   * Adds a Stack to any of the storages in the Container.
   *
   * @param {ItemStack} stack - The Stack that should be added.
   *
   * @returns {undefined|Storage} The Storage it has been added to or undefined if
   *                              the Stack could not be added.
   */
  addStack(stack) {
    for (const storage of this.storages.values()) {
      if (storage.add(stack)) {
        return storage;
      }
    }
  }

  /**
   * @type {float}
   */
  get contentVolume() {
    let volume = 0.0;
    this.storages.forEach(storage => {
      volume += storage.contentVolume;
    });
    return volume;
  }

  /**
   * Wrapper for storages.forEach
   *
   * @param {function()} fn - The function that should be executed for each
   *                          Storage.
   */
  forEach(fn = () => {}) {
    this.storages.forEach(fn);
  }

  /**
   * Gets the first Item of the first Stack with the given Descriptor and attributes.
   *
   * @see {ItemStack~matchAttributes}
   *
   * @param {string|ItemDescriptor} _descriptor - The Item Descriptor.
   * @param {Object} [attributes=Object] - The attributes to check against.
   *
   * @returns {undefined|Item} The first item or undefined if there is none.
   */
  get(_descriptor, attributes = {}) {
    const descriptor = global.package.manager.resolveDescriptor(_descriptor);
    for (const storage of this.storages.values()) {
      if (storage.has(descriptor, attributes)) {
        return storage.get(descriptor, attributes);
      }
    }
  }

  /**
   * Gets the first Item of the first Stack with the given Descriptor.
   *
   * @see {ItemStack~matchAttributes}
   *
   * @param {string|ItemDescriptor} _descriptor - The Item Descriptor.
   *
   * @returns {undefined|Item} The first item or undefined if there is none.
   */
  getAny(_descriptor) {
    const descriptor = global.package.manager.resolveDescriptor(_descriptor);
    for (const storage of this.storages.values()) {
      if (storage.hasAny(descriptor)) {
        return storage.getAny(descriptor);
      }
    }
  }

  /**
   * Returns the first Storage that has room for the given Descriptor & quantity or a
   * customized Weight.
   *
   * @param {string|ItemDescriptor} _descriptor - The Item Descriptor.
   * @param {int} [quantity=1] quantity - The amount of Items.
   * @param {int} [customVolume=1] customVolume - If set, the quantity and descriptor volume
   *                                              will be ignored.
   *
   * @returns {undefined|Storage}
   */
  getFreeStorage(_descriptor, quantity = 1, customVolume = undefined) {
    const descriptor = global.package.manager.resolveDescriptor(_descriptor);

    if (this.has(descriptor, descriptor.attributes)) {
      return this.getStacks(descriptor, descriptor.attributes)[0].storage;
    }

    for (const storage of this.storages.values()) {
      if (typeof customVolume !== 'undefined') {
        if (storage.capacity - storage.contentVolume >= customVolume) {
          return storage;
        }
      }
      if (storage.hasRoomFor(descriptor, quantity)) {
        return storage;
      }
    }
  }

  /**
   * Gets all available Stacks that match the given Descriptor and attributes.
   *
   * @see {ItemStack~matchAttributes}
   *
   * @param {string|ItemDescriptor} _descriptor - The Item Descriptor.
   * @param {Object} [attributes=Object] - The attributes to check against.
   *
   * @returns {Array<ItemStack>}
   */
  getStacks(_descriptor, attributes = {}) {
    const descriptor = global.package.manager.resolveDescriptor(_descriptor);
    for (const storage of this.storages.values()) {
      if (storage.has(descriptor, attributes)) {
        return storage.getStacks(descriptor, attributes);
      }
    }
    return [];
  }

  /**
   * Gets all available Stacks that match the given Descriptor.
   *
   * @param {string|ItemDescriptor} _descriptor - The Item Descriptor.
   *
   * @returns {Array<ItemStack>}
   */
  getAnyStacks(_descriptor) {
    const descriptor = global.package.manager.resolveDescriptor(_descriptor);
    for (const storage of this.storages.values()) {
      if (storage.hasAny(descriptor)) {
        return storage.getAnyStacks(descriptor);
      }
    }
    return [];
  }

  /**
   * @type {float}
   */
  get capacity() {
    let capacity = 0.0;
    this.storages.forEach(storage => {
      capacity += storage.capacity;
    });
    return capacity;
  }

  /**
   * @type {int}
   */
  get weight() {
    let weight = 0.0;
    this.storages.forEach(storage => {
      weight += storage.weight;
    });
    return weight;
  }

  /**
   * Checks whether any Storage in the Container has a Stack of the given Descriptor and matching attributes.
   *
   * @see {ItemStack~matchAttributes}
   *
   * @param {string|ItemDescriptor} _descriptor - The Item Descriptor.
   * @param {Object} [attributes=Object] - The attributes to check against.
   *
   * @returns {boolean} Whether the Storage has any Stacks of the descriptor and attributes.
   */
  has(_descriptor, attributes = {}, sloppy = false) {
    const descriptor = global.package.manager.resolveDescriptor(_descriptor);
    for (const storage of this.storages.values()) {
      if (storage.has(descriptor, attributes, sloppy)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Checks whether any Storage in the Container has any Stacks of the given Descriptor.
   *
   * @param {string|ItemDescriptor} _descriptor - The Item Descriptor.
   *
   * @returns {boolean} Whether the Storage has any Stacks of the descriptor.
   */
  hasAny(_descriptor) {
    const descriptor = global.package.manager.resolveDescriptor(_descriptor);
    for (const storage of this.storages.values()) {
      if (storage.hasAny(descriptor)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Checks whether the Storages of the Container have any room for a certain quantity of a Descriptor.
   *
   * @param {string|ItemDescriptor} _descriptor - The Item Descriptor.
   * @param {int} [quantity=1] - The quantity the Storage should have room for.
   *
   * @returns {boolean} Whether the Storage has enough free room left.
   */
  hasRoomFor(_descriptor, quantity = 1) {
    const descriptor = global.package.manager.resolveDescriptor(_descriptor);

    for (const storage of this.storages.values()) {
      if (storage.hasRoomFor(descriptor, quantity)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Pops a Item of the given Descriptor and attributes.
   *
   * @param {string|ItemDescriptor} _descriptor - The Item Descriptor.
   * @param {Object} [attributes=Object] - The Attributes to check against.
   *
   * @returns {undefined|Item}
   */
  pop(_descriptor, attributes = {}, quantity = 1) {
    const descriptor = global.package.manager.resolveDescriptor(_descriptor);
    for (const storage of this.storages.values()) {
      if (storage.has(descriptor, attributes)) {
        return storage.pop(descriptor, attributes, quantity);
      }
    }
  }

  /**
   * Pops a Item of the given Descriptor.
   *
   * @param {string|ItemDescriptor} _descriptor - The Item Descriptor.
   *
   * @returns {undefined|Item}
   */
  popAny(_descriptor, quantity = 1) {
    const descriptor = global.package.manager.resolveDescriptor(_descriptor);
    for (const storage of this.storages.values()) {
      if (storage.hasAny(descriptor)) {
        return storage.popAny(descriptor, quantity);
      }
    }
  }

  /**
   * Pops a ItemStack of the given Descriptor and attributes if it exists in the Storage.
   *
   * @param {string|ItemDescriptor} _descriptor - The Item Descriptor.
   * @param {Object} attributes - The Attributes to check against.
   * @param {int} quantity - The amount of Items to drop.
   * @param {Storage} storage - The new Storage for the popped Stack.
   * @param {function(stack: ItemStack)} done - Callback when the action finished.
   *
   * @returns {boolean} Whether the stack could be popped.
   */
  popStack(_descriptor, attributes, quantity, storage, done) {
    const descriptor = global.package.manager.resolveDescriptor(_descriptor);
    for (const strg of this.storages.values()) {
      if (strg.has(descriptor, attributes)) {
        return strg.getStacks(descriptor, attributes)[0].popStack(quantity, storage, done);
      }
      return false;
    }
  }

  /**
   * Pops a ItemStack of the given Descriptor if it exists in the Storage.
   *
   * @param {string|ItemDescriptor} _descriptor - The Item Descriptor.
   * @param {int} quantity - The amount of Items to drop.
   * @param {Storage} storage - The new Storage for the popped Stack.
   * @param {function(stack: ItemStack)} done - Callback when the action finished.
   *
   * @returns {boolean} Whether the stack could be popped.
   */
  popAnyStack(_descriptor, attributes, quantity, storage, done) {
    const descriptor = global.package.manager.resolveDescriptor(_descriptor);
    for (const strg of this.storages.values()) {
      if (strg.hasAny(descriptor)) {
        return strg.getAnyStacks(descriptor)[0].popStack(quantity, storage, done);
      }
      return false;
    }
  }

  /**
   * Removes the storage from the Container
   *
   * @param {Storage} storage - The Storage that should be removed.
   *
   * @returns {boolean} Whether the Storage could be deleted or not.
   */
  remove(storage) {
    return this.storages.remove(storage.descriptor.name);
  }
}
