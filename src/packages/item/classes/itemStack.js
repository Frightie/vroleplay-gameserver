/**
 * @title itemStack.js
 * @overview ItemStack Class
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

/**
 * ItemStacks hold multiples of the same kind of Items together and make them
 * accessible. Only Items with the same descriptor and attributes can be in
 * the same ItemStack.
 */
export default class ItemStack {
  /**
   * Creates a new ItemStack.
   *
   * @param {string|ItemDescriptor} _descriptor - The ItemDescriptor for the Stack.
   * @param {int} quantity - The amount of Items.
   * @param {Storage} storage - the Storage the ItemStack is held in.
   * @param {boolean} [contentWillBeMoved=false] - Check this flag if the content already exists to reduce performance  impact.
   * @param {Object} [attributes=undefined] - Custom attributes for the stack.
   */
  constructor(_descriptor, quantity, storage, contentWillBeMoved = false, attributes = undefined) {
    const descriptor = global.package.manager.resolveDescriptor(_descriptor);
    let parsedQuantity = quantity;
    /** @type {ItemDescriptor} */
    this.descriptor = descriptor;

    /** @type {Set<Item>} */
    this.items = new Set();

    /** @type {Storage} */
    this._storage = storage;

    if (this.descriptor.type === global.package.Weapon) {
      /**
       * Only present in Weapons
       *
       * @type {boolean}
       */
      this.didFirstEquip = false;
    }

    if (typeof attributes !== 'undefined') {
      /** @type {Object} */
      this.attributes = shared.utility.concat({}, attributes);
    } else {
      /** @type {Object} */
      this.attributes = shared.utility.concat({}, this.descriptor.attributes);
    }

    if (!contentWillBeMoved) {
      if (this.descriptor.stackSize < quantity) {
        log.warn('ItemStack quantity > ItemInfo stackSize');
        parsedQuantity = this.descriptor.stackSize;
      } else if (typeof parsedQuantity === 'undefined') {
        parsedQuantity = 0;
      }

      this.add(parsedQuantity);
    }
    this.descriptor.invoke('newStack', this);
  }

  /**
   * @type {int}
   */
  get size() {
    return this.items.size;
  }

  /**
   * @type {boolean}
   */
  get full() {
    if (typeof this.descriptor.other.maxStackSize === 'undefined') {
      return false;
    }
    return this.quantity >= this.descriptor.other.maxStackSize;
  }

  /**
   * @type {Storage}
   */
  get storage() {
    return this._storage;
  }

  /**
   * @type {Storage}
   */
  set storage(storage) {
    if (this._storage) {
      this._storage.remove(this);
    }
    if (this.descriptor.type === global.package.Weapon) {
      if (!this.first().unequip(this.storage.owner.getPlayer())) {
        log.warn(`Could not unequip Weapon ${this.dbModel.id}`);
      }
    }

    this._storage = storage;

    if (this._storage) {
      this._storage.add(this);
    }
  }

  /**
   * @type {int}
   */
  get quantity() {
    return this.items.size;
  }

  /**
   * @type {int}
   */
  get weight() {
    if (this.descriptor.hooks.has('getStackWeight')) {
      const r = this.descriptor.invoke('getStackWeight', this);
      if (typeof r !== 'undefined' && r.length !== 0) {
        if (r.length > 1) {
          log.warn('getStackWeight: multiple returns not supported');
        }
        return r[0];
      }
    }
    return this.descriptor.weight * this.quantity;
  }

  /**
   * @type {float}
   */
  get volume() {
    return this.descriptor.volume * this.quantity;
  }

  /**
   * Adds the given Item(s) to the Stack.
   *
   * @param {int|Array<Item>} quantityOrItems - An amount of items or an array of Items that should be added.
   * @param {Object} attributes - custom Attributes for the Items.
   *
   * @returns {int} The amount of items that have been added.
   */
  add(quantityOrItems, attributes) {
    let quantity;
    let items;
    if (quantityOrItems instanceof Array) {
      quantity = quantityOrItems.length;
      items = quantityOrItems;
    } else {
      quantity = quantityOrItems;
    }

    let added = 0;
    let newQuantity = quantity;
    if (quantity < 0) {
      return this.remove(quantity * -1);
    }

    if (quantity + this.size > this.descriptor.other.maxStackSize) {
      newQuantity = this.descriptor.other.maxStackSize - this.size;
    }

    if (newQuantity === 0) {
      log.debug('No items to add.');
      return true;
    }

    for (let i = 0; i < newQuantity; i++) {
      if (items) {
        this.items.add(items[i]);
      } else {
        this.items.add(this.descriptor.create(this, attributes));
      }
      added++;
    }
    this.descriptor.invoke('stackQuantityAdded', this, added);
    return added;
  }

  /**
   * Returns the first Item in the Stack.
   *
   * @returns {Item}
   */
  first() {
    return this.items.values().next().value;
  }

  /**
   * @type {string}
   */
  get description() {
    const desc = this.descriptor.invoke('getDescription', this);
    if (typeof desc === 'undefined') {
      return this.descriptor.description;
    }

    if (desc.length > 0) {
      log.warn('getDescription: multi-return not supported');
    }
    return desc[0];
  }

  /**
   * @type {string}
   */
  get name() {
    const desc = this.descriptor.invoke('getName', this);
    if (typeof desc === 'undefined') {
      return this.descriptor.name;
    }

    if (desc.length > 1) {
      log.warn('getName: multi-return not supported');
    }
    return desc[0];
  }

  /**
   * Returns whether the ItemStack has the amount specified.
   *
   * @param {int} quantity - The amount that is needed
   *
   * @returns {boolean} Whether the Quantity in the ItemStack is bigger or equal to the needed quantity.
   */
  has(quantity) {
    return this.quantity >= quantity;
  }

  /**
   * Tries to match the given attributes against the Stack's attributes.
   *
   * @param {Object} [attributes] - The attributes to check against. Use 'key: global.package.symSkip' to
   *                              skip a certain key. The 'equipped' key is skipped by default.
   * @param {boolean} [attributes._useDefaults=true] - Uncheck this in order to NOT use the default descriptor
   *                                                   attributes.
   * @param {boolean} [sloppy=false] - If sloppy mode is enabled, undefined properties in the attributes param will be
   *                                   validated as equal to the Stack's attribute.
   *
   * @returns {boolean} Whether the given attributes matched the Stack's attributes.
   */
  matchAttributes(attributes = { _useDefaults: true }, sloppy = false) {
    if (attributes._useDefaults) {
      shared.utility.concat(attributes, this.descriptor.attributes);
    }

    const attr = {};
    const compAttr = {};
    for (const prop in this.attributes) {
      if (attributes[prop] === global.package.symSkip || prop === 'equipped' || sloppy && typeof attributes[prop] === 'undefined') {
        continue;
      }

      attr[prop] = this.attributes[prop];
      compAttr[prop] = attributes[prop];
    }
    const r = JSON.stringify(attr) === JSON.stringify(compAttr);
    delete attributes._useDefaults;
    return r;
  }

  /**
   * Moves the ItemStack to a different Storage.
   *
   * @param {Storage} storage - The new Storage.
   *
   * @returns {boolean} Whether the Action succeeded.
   */
  move(storage) {
    if (this.storage) {
      this.storage.remove(this);
    }
    this.storage = storage;
    return this.storage.add(this);
  }

  /**
   * Moves the given amount of Items to a different ItemStack.
   *
   * @param {ItemStack} stack - The new Stack for the Items.
   * @param {int} [quantity=1] - The amount of Items that should be moved.
   *
   * @returns {boolean} Whether the Items could be moved.
   */
  moveItems(stack, quantity = 1) {
    if (quantity > this.quantity) {
      return false;
    }

    const it = this.items.values();
    let r = true;
    for (let i = 0; i < quantity; i++) {
      if (it.next().value.move(stack) === false) {
        log.warn(`Could not move item (i: ${i})`);
        r = false;
      }
    }
    this.descriptor.invoke('quantityMoved', this, quantity, stack);
    return r;
  }

  /**
   * Pops the given amount of items.
   *
   * @param {int} [quantity = 1] - The amount of items to drop.
   *
   * @returns {Array<Item>}
   */
  pop(quantity = 1) {
    let itm;
    if (quantity > 1) {
      itm = [];
    }

    for (const item of this.items.values()) {
      if (quantity > 1) {
        if (itm.length === quantity) {
          break;
        }
        itm.push(item);
      } else {
        itm = item;
        break;
      }
    }

    this.remove(itm);
    return itm;
  }

  /**
   * Pops the given amount of items and creates a new ItemStack of it.
   * If the given quantity equals the Stack's quantity, no new stack is created. The
   * ownership will just be changed to the new Storage.
   *
   * @param {number} quantity - The amount of items to pop.
   * @param {DBStorage} storage - The new storage for the popped Stack.
   * @param {function(stack: ItemStack)} [done=function()] - Callback for the completed function.
   * @param {Object} [attributes = undefined] - Custom attributes for the popped Stack.
   *
   * @returns {boolean} Whether the stack could be popped.
   */
  popStack(quantity, storage, done = () => {}, attributes = undefined) {
    if (!this.has(quantity)) {
      return false;
    }

    let compareAttributes = this.attributes;
    if (typeof attributes !== 'undefined') {
      compareAttributes = attributes;
    }

    if (storage.has(this.descriptor, compareAttributes)) {
      const stack = storage.getStacks(this.descriptor, compareAttributes)[0];
      if (stack.quantity < stack.descriptor.other.maxStackSize) {
        this.moveItems(stack, quantity);
        done(stack);
        return true;
      }
    }

    if (quantity === this.quantity) {
      log.debug(`Changing ownership of itemStack`);
      // Just change ownership and attributes
      this.attributes = compareAttributes;
      this.move(storage);
      done(this);
      return true;
    }

    const stack = new ItemStack(this.descriptor, quantity, storage, true, attributes);

    if (!this.moveItems(stack, quantity)) {
      log.warn(`Could not pop a new ItemStack: moveItems failed`);
      return false;
    }
    done(stack);
    return true;
  }

  /**
   * Removes the given amount, item Array or single Item from the stack.
   *
   * @param {int|Item|Array<Item>} quantityOrItemS - A amount of items, a single Item or Array of Items that
   *                                                 should be removed from the ItemStack.
   *
   * @returns {boolean} Whether the item(s) could be removed.
   */
  remove(quantityOrItemS) {
    if (typeof quantityOrItemS === 'number') {
      if (quantityOrItemS < 0) {
        this.add(quantityOrItemS * -1);
        return true;
      }

      let remainingQuantity = quantityOrItemS;

      if (quantityOrItemS > this.size) {
        remainingQuantity = this.size;
      }

      for (const item of this.items.values()) {
        this.items.delete(item);
        item.stack = undefined;
        remainingQuantity--;

        if (remainingQuantity === 0) {
          break;
        }
      }
      this.descriptor.invoke('stackRemoveQuantity', this);
      return true;
    } else if (quantityOrItemS instanceof Array) {
      for (let i = 0; i < quantityOrItemS.length; i++) {
        if (this.items.has(quantityOrItemS[i])) {
          this.items.delete(quantityOrItemS[i]);
          quantityOrItemS[i].stack = undefined;
        }
      }
      this.descriptor.invoke('stackRemoveQuantity', this);
      return true;
    } else if (typeof quantityOrItemS === 'object') {
      if (this.items.has(quantityOrItemS)) {
        quantityOrItemS.stack = undefined;
        this.descriptor.invoke('stackRemoveQuantity', this);
        return this.items.delete(quantityOrItemS);
      }
      return false;
    }
  }
}
