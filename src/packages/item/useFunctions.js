/**
 * @title useFunctions.js
 * @overview Hardcoded Item use functions
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

import weaponSystem from './weaponSystem';

/**
 * Usage Functions for the Weapon System and other Items.
 */
export default function injectUseFunctions(get) {
  weaponSystem(get);

  get('Teaspoon of Salt').on('use', player => {
    player.sendInfo('I doubt you want to eat salt.');
    return {used: true, deleted: false};
  });
}
