/**
 * @title commands.js
 * @overview Implements Package Commands
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

/**
  * @namespace
  */
const Commands = {};

/**
 * Commands for the Item System.
 */
export default Commands;

Commands.item = new shared.CommandGroup(['item', 'i'], {});

Commands.item.add(new shared.Command('create', {
  parameters: [
    ['descriptor', 'text'],
  ],
}, (player, _descriptor) => {
  const descriptor = global.package.manager.resolveDescriptor(_descriptor);
  if (typeof descriptor === 'undefined') {
    player.sendError(`Unknown item descriptor '${_descriptor}'`);
    return;
  }

  if (!player.storages.hasRoomFor(descriptor, 1)) {
    player.sendError(`Adding one ${descriptor.name} would exceed the storage's capacity of ${player.storages.capacity}l.`);
    return;
  }

  if (!vrp.item.createStack(descriptor, player.storages.getFreeStorage(descriptor, 1), 1, {}, () => {
    player.sendSuccess(`New ${_descriptor} created.`);
  })) {
    player.sendError(`Could not create a ${_descriptor}`);
  }
}));

Commands.item.add(new shared.Command('use', {
  parameters: [
    ['descriptor', 'text'],
  ],
}, (player, descriptor) => {
  if (!player.storages.hasAny(descriptor, {})) {
    player.sendError(`Unknown item descriptor '${descriptor}'`);
    return;
  }

  if (!player.storages.getAny(descriptor).use(player).used) {
    player.sendError(`Could not use ${descriptor}.`);
    return;
  }
  player.sendSuccess(`Used ${descriptor}.`);
}));

Commands.item.add(new shared.Command('drop', {
  parameters: [
    ['amount', 'integer'],
    ['descriptor', 'text'],
  ],
}, (player, amount, descriptor) => {
  if (!player.storages.hasAny(descriptor)) {
    player.sendError(`You don't own any ${descriptor}.`);
    return;
  }

  const stacks = player.storages.getAnyStacks(descriptor);
  const dropStacks = [];
  let amountLeft = amount;
  for (let i = 0; i < stacks.length; i++) {
    if (stacks[i].quantity < amountLeft) {
      dropStacks.push({ amount: stacks[i].quantity, stack: stacks[i] });
      amountLeft -= stacks[i].quantity;
    } else if (stacks[i].quantity >= amountLeft) {
      dropStacks.push({ amount: amountLeft, stack: stacks[i] });
      amountLeft = 0;
      break;
    }
  }

  if (amountLeft > 0) {
    player.sendError(`You can't drop ${amount} ${descriptor} because you do not own so many.`);
    return;
  }


  let stacksDropped = 0;
  const fnFinish = () => {
    stacksDropped++;

    if (stacksDropped === dropStacks.length) {
      player.sendError(`Dropped ${amount} ${descriptor}.`);
    }
  };

  for (let i = 0; i < dropStacks.length; i++) {
    if (!dropStacks[i].stack.drop(player.position, fnFinish, dropStacks[i].amount)) {
      log.error(`Internal Error when dropping Stack ${dropStacks[i].stack.dbModel.id} from Player ${player.character.name}`);
    }
  }
}));

Commands.item.add(new shared.Command('pick', {
  parameters: [],
}, player => {
  // Get nearby storage
  let items = 0;
  global.package.globalStorage.content.forEach(stack => {
    if (stack.dropPosition === null) {
      return;
    }

    if (shared.utility.distance(player.position, stack.dropPosition) < 3.0) {
      items++;
      if (stack.pickup(player)) {
        player.sendSuccess(`You picked up ${stack.quantity}x ${stack.name}.`);
      } else {
        player.sendError(`Could not pick up ${stack.quantity}x ${stack.name}.`);
      }
    }
  });
  if (!items) {
    player.sendError('There are no items near you to pick up.');
  }
}));

Commands.item.add(new shared.Command('info', {
  parameters: [],
}, player => {
  player.sendInfo(`Total Weight: ${player.storages.weight / 1000.0}kg. Volume: ${player.storages.contentVolume}/${player.storages.capacity}l`);
  player.storages.forEach(storage => {
    player.sendInfo(`<strong>${storage.name}</strong><hr />`);
    storage.content.forEach(stack => {
      player.sendInfo(`(${stack.weight}g) ${stack.quantity}x ${stack.name}`);
    });
  });
}));

Commands.item.add(new shared.Command('removeComponent', {
  parameters: [
    ['component', 'text'],
  ],
  validator: player => player.vrpEquippedWeapon,
}, (player, component) => {
  const descriptor = global.package.manager.resolveDescriptor(component);
  if (typeof descriptor === 'undefined') {
    player.sendError(`Unknown Weapon Component ${component}.`);
    return;
  }
  if (player.vrpEquippedWeapon.removeComponent(player, descriptor)) {
    player.sendSuccess(`Removed Component ${component} from your ${player.vrpEquippedWeapon.stack.name}.`);
    return;
  }
  player.sendError(`Could note remove a ${component} from your ${player.vrpEquippedWeapon.stack.name}.`);
}));
