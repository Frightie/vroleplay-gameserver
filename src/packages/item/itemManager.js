/**
 * @title itemManager.js
 * @overview Item Manager Implementation
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

import BaseManager from '../../shared/classes/baseManager';
import ItemDescriptor from './classes/itemDescriptor';

import injectUseFunctions from './useFunctions';

/**
 * ItemManager class that holds both existing storages and item descriptors.
 */
class ItemManager extends BaseManager {
  /**
   * Creates the Player Manager
   *
   * @class PlayerManager
   * @this {PlayerManager}
   */
  constructor() {
    super('items');
    /** @type {boolean} */
    this.initialized = false;

    /** @type {Map<string, ItemDescriptor>} */
    this.descriptors = new Map();

    /** @type {Map<int, ItemDescriptor} */
    this.weaponDescriptors = new Map();

    /** @type {Map<int, DBStorage} */
    this.storages = new Map();

    /** @type {Symbol} */
    this.symSkip = Symbol('skip');

    /** @type {DBStorage} */
    this.globalStorage = undefined;
  }

  /**
   * Initializes the Faction manager
   *
   * @this {PlayerManager}
   */
  init() {
    vrp.database.ItemDescriptor.all(descriptors => {
      for (let i = 0; i < descriptors.length; i++) {
        this.descriptors.set(descriptors[i].name, new ItemDescriptor(descriptors[i]));
      }
      injectUseFunctions(this.resolveDescriptor.bind(this));

      this.descriptors.forEach(descriptor => {
        if (descriptor.type === global.package.Weapon) {
          this.weaponDescriptors.set(shared.utility.findWeapon(descriptor.other.identifier).hash, descriptor);
        }
      });

      // Load Global storage
      vrp.database.Storage.first(0, strg => {
        this.globalStorage = new vrp.item.DBStorage(strg);
        log.success('Global Storage has been loaded.');
      });
    });
    this.initialized = true;
  }

  /**
   * Finds the ItemDescriptor based on the id
   *
   * @param {integer} id database id
   * @returns {ItemDescriptor|undefined}
   */
  getDescriptorById(id) {
    for (const descriptor of this.descriptors.values()) {
      if (descriptor.dbModel.id === id) {
        return descriptor;
      }
    }
  }

  /**
   * Returns the valid hash for a weapon component
   *
   * @param {integer} weaponId weapon dbModel id
   * @param {integer} componentId the component id
   * @returns {integer} the hash
   */
  getComponentHashForWeapon(weaponId, componentId) {
    const component = this.getDescriptorById(componentId);

    for (let i = 0; i < component.other.hashes.length; i++) {
      for (let i2 = 0; i2 < component.other.hashes[i].weapons.length; i2++) {
        if (component.other.hashes[i].weapons[i2] === weaponId) {
          return component.other.hashes[i].hash;
        }
      }
    }
  }

  /**
   * Resolves a descriptor for easier usage in all functions.
   *
   * @param {string|ItemDescriptor} descriptor target
   * @returns {ItemDescriptor|undefined}
   */
  resolveDescriptor(descriptor) {
    if (typeof descriptor === 'string') {
      return this.descriptors.get(descriptor);
    }
    return descriptor;
  }

  /**
   * Resolves an item type from a string to its actual class
   *
   * @param {string} type type string
   * @returns {Object|undefined} the class or nothing
   */
  resolveType(type) {
    switch (type) {
    case 'item':
      return global.package.Item;
    case 'itemStack':
      return global.package.ItemStack;
    case 'consumable':
      return global.package.Consumable;
    case 'storage':
      return global.package.Storage;
    case 'weapon':
      return global.package.Weapon;
    default:
      break;
    }
  }
}

/**
 * ItemManager instance
 */
export default new ItemManager();
