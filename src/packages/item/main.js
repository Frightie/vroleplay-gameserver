/**
 * @title main.js
 * @overview Main Package File
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

// Predefined V:RP Header to install our own error types & handler
import error from '../../shared/error';
import errorHandler from '../../shared/errorHandler';
error.install(global);
process.on('uncaughtException', errorHandler);

import Package from '../../shared/classes/package';
import Item from './classes/item';
import ItemStack from './classes/itemStack';
import DBItemStack from './classes/dbItemStack';
import Consumable from './classes/consumable';
import Weapon from './classes/weapon';
import Storage from './classes/storage';
import DBStorage from './classes/dbStorage';
import StorageContainer from './classes/storageContainer';

import _itemManager from './itemManager';

class ItemSystem extends Package {
  /**
   * Constructs the Package
   * @constructor
   */
  constructor() {
    super('item');
    this.Item = Item;
    this.ItemStack = ItemStack;
    this.DBItemStack = DBItemStack;
    this.Consumable = Consumable;
    this.Weapon = Weapon;
    this.Storage = Storage;
    this.DBStorage = DBStorage;
    this.StorageContainer = StorageContainer;
    this.manager = _itemManager;
    this.descriptors = this.manager.descriptors;

    const magazineSaveTimeout = new Map();

    events.Add('PlayerShot', (player, weaponHash) => {
      let hash = weaponHash;
      if (hash === -0x40100093) {
        hash = 0xBFEFFF6D;
      } else if (hash === -1813897027) {
        hash = 0x93E220BD;
      } else if (hash === -1312131151) {
        hash = 0xB1CA77B1;
      }

      // Get the Weapon Descriptor by its Hash
      const descriptor = this.manager.weaponDescriptors.get(hash);
      if (typeof descriptor === 'undefined') {
        log.warn(`Unknown Weapon ${hash} shot by ${player.character.name}`);
        return;
      }

      if (!player.playing) {
        log.warn(`Player that is not playing is shooting: ${player.name}`);
        return;
      }

      if (!player.storages.has(descriptor, {equipped: true}, true)) {
        player.debug(`Shooting unregistered weapon ${descriptor.name}`);
        log.warn(`Possible Cheat: player ${player.name} shoots unregistered weapon ${descriptor.name}`);
        return;
      }
      const weapon = player.storages.get(descriptor, {equipped: true}, true);

      if (weapon.descriptor.other.noAmmo && !weapon.descriptor.other.throwable) {
        return;
      }

      if (weapon.descriptor.other.throwable) {
        const stack = weapon.stack;
        if (stack.quantity === 1) {
          weapon.unequip(player);
          stack.delete();
          return;
        }

        // We have to find a weapon that is different from ours
        let delWeapon;
        for (const _weapon of stack.items.values()) {
          if (_weapon !== weapon) {
            delWeapon = _weapon;
            break;
          }
        }
        if (typeof delWeapon === 'undefined') {
          throw new FatalError(`Throwable mismatch in item stack remove`);
        }
        stack.remove(delWeapon);
        return;
      }

      if (!player.storages.has(weapon.ammoDescriptor, {}, true)) {
        player.debug(`Shooting ${descriptor.name} without having ${weapon.ammoDescriptor.name}`);
        log.warn(`Possible Cheat: player ${player.name} shoots ${descriptor.name} without having magazines`);
        return;
      }

      const magazines = player.storages.getStacks(weapon.ammoDescriptor);
      magazines.sort((a, b) => a.attributes.bullets - b.attributes.bullets);

      const fnPoppedStack = () => {
        player.debug(`Using different magazine now.`);
      };
      for (let i = 0; i < magazines.length; i++) {
        if (magazines[i].attributes.bullets === 0) {
          continue;
        }

        if (magazines[i].quantity > 1) {
          const newStorage = player.storages.getFreeStorage(magazines[i].descriptor, 0);

          if (typeof newStorage === 'undefined') {
            log.error(`Could not find free storage ?`);
            break;
          }

          magazines[i].popStack(1, newStorage, fnPoppedStack, { bullets: magazines[i].attributes.bullets - 1 });
          break;
        } else {
          magazines[i].attributes.bullets -= 1;

          if (magazines[i].attributes.bullets === 0) {
            player.debug(`Magazine emptied.`);

            if (magazines[i].descriptor.other.deleteEmpty) {
              clearTimeout(magazineSaveTimeout.get(magazines[i].dbModel.id));
              magazines[i].delete();
            }
            magazines[i].dbModel.save();

            // Merge empty
            magazines[i].storage.merge();
          } else {
            clearTimeout(magazineSaveTimeout.get(magazines[i].dbModel.id));
            magazineSaveTimeout.set(magazines[i].dbModel.id, setTimeout(() => {//eslint-disable-line
              magazines[i].dbModel.save();
              magazineSaveTimeout.delete(magazines[i].dbModel.id);
            }, 500));
          }
        }
        break;
      }
    });
  }

  /**
   * Starts the Package
   */
  start() {
    super.start();
  }

  createStack(_descriptor, storage, quantity, attributes, done = () => {}) {
    shared.utility.concat(attributes, { _useDefaults: true });
    const descriptor = this.manager.resolveDescriptor(_descriptor);

    if (descriptor.type === this.Storage || descriptor.type === this.DBStorage) {
      return false;
    }

    if (storage.has(descriptor, attributes)) {
      const stacks = storage.getStacks(descriptor, attributes);
      for (let i = 0; i < stacks.length; i++) {
        if (stacks[i].full) {
          continue;
        }
        stacks[i].add(quantity);
        done(stacks[i]);
        return true;
      }
    }

    delete attributes._useDefaults;

    vrp.database.Item.create({
      attributes: attributes,
      storage: storage.dbModel.id,
      descriptor: descriptor.dbModel.id,
      quantity: quantity,
    }, item => {
      if (!item) {
        throw new DatabaseError('Could not create new ItemStack');
      }
      done(new DBItemStack(item));
    });
    return true;
  }

  get globalStorage() {
    return this.manager.globalStorage;
  }

  receiveDependencies(deps) { super.receiveDependencies(global, deps); }
  receiveVRPNamespace(namespace) { super.receiveVRPNamespace(global, namespace); }
}

// Register globals
global.package = new ItemSystem();
global[global.package.name] = global.package;
