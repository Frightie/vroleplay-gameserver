
/**
 * @title weaponSystem.js
 * @overview Weapon System functions and hooks
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

export default function weaponSystem(get) {
  const tints = ['Normal', 'Green', 'Gold', 'Pink', 'Army', 'LSPD', 'Orange', 'Platinum'];
  get('Weapon Tint').on('getName', stack => `${tints[stack.attributes.tintIndex]} Weapon Tint`);
  get('Weapon Tint').on('use', (player, tint) => {
    if (!player.vrpEquippedWeapon) {
      player.sendError(`You dont have any weapon equipped.`);
      return {used: false, shouldDelete: false};
    }

    if (player.vrpEquippedWeapon.attributes.tint === tint.attributes.tintIndex) {
      player.sendError(`Your Weapon already has that tint.`);
      return {used: false, shouldDelete: false};
    }

    if (player.vrpEquippedWeapon.setTint(tint.attributes.tintIndex, player)) {
      player.sendSuccess(`Applied ${tint.name} on your ${player.vrpEquippedWeapon.name}.`);
      return {used: true, shouldDelete: true};
    }
    player.sendError(`Could not apply ${tint.name} on your ${player.vrpEquippedWeapon.name}.`);
    return {used: false, shouldDelete: false};
  });
}
