/**
 * @title commandManager.js
 * @overview Command Manager Implementation
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

import BaseManager from '../../shared/classes/baseManager';

/**
 * Manages all Commands of V:RP.
 */
export class CommandManager extends BaseManager {
  /**
   * Creates the Command Manager.
   */
  constructor() {
    super('commands');

    /** @type {boolean} */
    this.initialized = false;

    events.Add('ChatCommand', this.handleCommand.bind(this));
  }

  /**
   * Initializes the Command manager
   *
   * @param {Object} packages - Object with all Packages.
   */
  init(packages) {
    log.print('Initializing Command Manager');

    // Register our Input handler to list all commands
    vrp.server.inputCommands.set('commands', () => {
      const printCommand = command => {
        log.print(command.usage);
        if (command instanceof shared.CommandGroup) {
          command.commands.forEach(cmd => {
            printCommand(cmd);
          });
        }
      };

      this.commands.forEach(command => {
        printCommand(command);
      });
    });

    let done = 0;
    let total = 0;
    packages.forEach((_, packageName) => {
      total++;
      if (this.registerPackage(packageName)) {
        done++;
      }
    });
    if (done !== total) {
      log.error('Could not register all package commands');
    } else {
      this.initialized = true;
    }
  }

  /**
   * Registers a command
   *
   * @param {Command} command the Command.
   * @returns {boolean} Whether the command was added.
   */
  register(command) {
    let bExists = false;
    for (const tempCommand of this.commands.values()) {
      for (const trigger of tempCommand.trigger) {
        if (command.trigger.has(trigger)) {
          bExists = true;
          break;
        }
      }
    }

    if (bExists) {
      log.warn(`Tried to add different handlers for same trigger (one of these: '${command.trigger.toString()}')`);
      return false;
    }

    this.commands.add(command);
    log.debug(`Command "${command.trigger.values().next().value}" added.`, 3);
    return true;
  }

  /**
   * Registers a package (all of its commands)
   *
   * @param {string} packageName the name of the package.
   * @returns {boolean} whether all package commands could be added
   */
  registerPackage(packageName) {
    if (!vrp.server.packages.has(packageName) || typeof vrp.server.packages.get(packageName).package.commands === 'undefined') {
      return true;
    }
    const pack = vrp.server.packages.get(packageName).package;

    let failed = 0;
    // Recursive function to walk through the object and register commands
    const fnWalkObject = obj => {
      if (typeof obj === 'object') {
        if (obj instanceof shared.Command) {
          obj.package = packageName;
          if (!this.register(obj)) {
            log.error(`Failed to register Command ${obj.trigger.toString()}"`);
            failed++;
          }
        } else {
          for (const key in obj) {
            if (typeof obj[key] === 'object') {
              fnWalkObject(obj[key]);
            }
          }
        }
      }
    };

    // Start the recursive function
    fnWalkObject(pack.commands);
    return failed === 0;
  }

  /**
   * Unregisters all package commands
   *
   * @param {string} packageName name of the package.
   */
  unregisterPackage(packageName) {
    this.commands.forEach(command => {
      this.commands.delete(command);
    });

    log.info('Unregistered Package Commands "' + packageName + '"');
  }

  /**
   * Command handler (lower level handler)
   *
   * @param {Player} _player The Player calling the command
   * @param {stirng} commandArguments Command Argumnts.
   */
  handleCommand(player, commandArguments) {
    console.time('HandleCommand');

    const args = commandArguments.split(' ');
    let commandName = args[0].toLowerCase();

    if (commandName.charAt(0) === '/') {
      commandName = commandName.substr(1);
    }
    args.splice(0, 1);

    let command;

    for (const tempCommand of this.commands) {
      if (tempCommand.trigger.has(commandName)) {
        command = tempCommand;
        break;
      }
    }

    if (typeof command === 'undefined') {
      player.sendError('Unknown Command.');
      return true;
    }

    log.debug(`Executing Command ${commandName}`);
    command.execute(player, args);
    console.timeEnd('HandleCommand');
    return true;
  }
}

/**
 * CommandManager instance
 */
export default new CommandManager();
