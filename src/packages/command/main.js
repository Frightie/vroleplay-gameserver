/**
 * @title main.js
 * @overview Main Package File
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

// Predefined V:RP Header to install our own error types & handler
import error from '../../shared/error';
import errorHandler from '../../shared/errorHandler';
error.install(global);
process.on('uncaughtException', errorHandler);

import Package from '../../shared/classes/package';
import _commandManager from './commandManager';

class CommandSystem extends Package {
  /**
   * Constructs the Package
   * @constructor
   */
  constructor() {
    super('command');
    this.manager = _commandManager;
  }

  /**
   * Starts the Package
   */
  start() {
    super.start();
  }

  /**
   * Returns the Dependencies
   * @returns {[string]}
   */
  get dependencies() {
    return ['player'];
  }

  receiveDependencies(deps) { super.receiveDependencies(global, deps); }
  receiveVRPNamespace(namespace) { super.receiveVRPNamespace(global, namespace); }
}

// Register globals
global.package = new CommandSystem();
global[global.package.name] = global.package;
