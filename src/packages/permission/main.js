/**
 * @title main.js
 * @overview Main Package File
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

// Predefined V:RP Header to install our own error types & handler
import error from '../../shared/error';
import errorHandler from '../../shared/errorHandler';
error.install(global);
process.on('uncaughtException', errorHandler);

import Package from '../../shared/classes/package';
import Permission from './permission';
import Container from './permissionContainer';

class PermissionSystem extends Package {
  /**
   * Constructs the Package
   * @constructor
   */
  constructor() {
    super('permission');

    /** @type {Set.<Permission>} */
    this._permissions = new Set();

    /** @type {Set.<PermissionContainer>} */
    this._containers = new Set();
  }

  /**
   * Starts the Package
   */
  start() {
    super.start();
  }

  /**
   * Initializes the Permissions. This function will make sure that every package can now
   * register their permissions.
   */
  init() {
    const permissions = events.Call('registerPermissions');
    for (let i = 0; i < permissions.length; i++) {
      if (typeof permissions[i] !== 'object') {
        continue;
      }

      for (let i2 = 0; i2 < permissions[i].data.length; i2++) {
        this.create(permissions[i].data[i2]);
      }
    }
  }

  /**
   * Creates a new Permission
   *
   * @param {string} name permission name / identifier
   * @param {Object} [options = {}] Options for the Permission
   * @returns {Permission} permission instance
   */
  create(name, options = {}) {
    const permission = new Permission(name, options);
    this._permissions.add(permission);
    return permission;
  }

  /**
   * Finds a Permission
   *
   * @param {string} name the permission's name
   * @returns {Permission|undefined} either the permission or undefined.
   */
  find(name) {
    for (const permission of this._permissions.values()) {
      if (name === permission.name) {
        return permission;
      }
    }
  }

  /**
   * Deletes a Permission from this Package and all PermissionContainers.
   *
   * @param {Permission|string} permission the Permission that should be deleted.
   * @returns {boolean} whether the permission could be deleted
   */
  delete(permission) {
    let perm = permission;
    if (typeof permission === 'string') {
      perm = this.find(permission);
    }

    if (!(perm instanceof this.Permission)) {
      log.warn(`Trying to delete Non-Permission (${typeof perm})`);
      return false;
    }
    // Remove it from all containers
    this._containers.forEach(container => {
      container.remove(perm);
    });
    return this._permissions.delete(perm);
  }

  /**
   * Creates a Container and adds it to the list of Containers
   *
   * @param {boolean} [iterable=false] whether the container has to be iterable
   * @param {PermissionContainer~defaultArgumentGetter} [evaluateArgFn] default evaluate arguments getter (used if no parameters
   *                                                                    are passed in evaluate)
   * @returns {PermissionContainer} the new container
   */
  createContainer(iterable = false, evaluateArgFn = () => { return []; }) {
    let container;
    if (iterable) {
      container = new Container(Set, evaluateArgFn);
    } else {
      container = new Container(WeakSet, evaluateArgFn);
    }
    this._containers.add(container);
    return container;
  }

  /**
   * Destroys a Container and removes it from the list of Containers
   *
   * @param {PermissionContainer} container
   */
  deleteContainer(container) {
    if (this._containers.has(container)) {
      this._containers.delete(container);
    }
  }

  receiveDependencies(deps) { super.receiveDependencies(global, deps); }
  receiveVRPNamespace(namespace) { super.receiveVRPNamespace(global, namespace); }
}

// Register globals
global.package = new PermissionSystem();
global[global.package.name] = global.package;
