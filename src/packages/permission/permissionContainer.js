/**
 * @title permissionContainer.js
 * @overview Permission Container Implementation
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

import Permission from './permission';

/**
 * The PermissionContainer is able to hold multiple permission and provides various
 * methods to retrieve, add, remove and evaluate Permissions.
 */
export default class PermissionContainer {
  /**
   * Holds Permissions
   *
   * @param {Set|WeakSet} ContainerType the container type. If it needs to be iterable, use Set.
   * @param {PermissionContainer~defaultArgumentGetter} defaultEvaluateArgs if no arguments are given in evaluate, this function
   *                        									                              will be called to get the arguments
   */
  constructor(ContainerType, defaultEvaluateArgs) {
    /** @type {Set.<Permission>} */
    this._permissions = new ContainerType();
    /** @type {PermissionContainer~defaultArgumentGetter} */
    this.defaultEvaluateArgs = defaultEvaluateArgs;

    if (ContainerType === Set) {
      /**
       * Returns the permission Container. Only present with ContainerType `Set`
       *
       * @returns {PermissionContainer<Set>}
       */
      this.get = () => this._permissions;
    }
    log.debug('Created new PermissionContainer', 3);
  }

  /**
   * Adds a permission to the container
   *
   * @param {Permission|string} permission to add
   * @returns {boolean} whether the add succeeded
   */
  add(permission) {
    let perm = permission;
    if (typeof permission === 'string') {
      perm = global.package.find(permission);
    }

    if (!(perm instanceof Permission)) {
      log.warn(`Not adding _something_ to a PermissionContainer, its definitely not a Permission (type: ${typeof perm})!`);
      return false;
    }
    return this._permissions.add(perm).has(perm);
  }

  /**
   * Assigns the Permissions to the given Container
   *
   * @param {PermissionContainer} container the container
   */
  applyPermissions(container) {
    if (!(this._permissions instanceof Set)) {
      log.warn('Tried to applyPermissions from a WeakSet PermissionContainer.');
      return;
    }

    this._permissions.forEach(permission => {
      container.add(permission);
    });
  }

  /**
   * Evaluates a Permission
   *
   * @param {Permission|string} the permission that has to be evaluated
   * @param {*...} any
   * @returns {boolean} evaluation result
   */
  evaluate(permission, ...args) {
    let perm = permission;
    if (typeof permission === 'string') {
      perm = global.package.find(permission);
    }

    if (!(perm instanceof Permission)) {
      log.warn(`Evaluate is asking for a Non-Permission (${typeof perm})`);
      return false;
    }

    let finalArgs = args;
    if (args.length === 0) {
      finalArgs = this.defaultEvaluateArgs();
    }

    if (perm.evaluate.apply(perm, finalArgs)) {
      if (!this.has(perm)) {
        this.add(perm);
      }
      return true;
    }

    if (this.has(perm)) {
      this.remove(perm);
    }
    return false;
  }

  /**
   * Checks whether the container holds the given Permission.
   *
   * @param {Permission|string} permission the permission that you ask for
   * @returns {boolean} whether the container holds the permission
   */
  has(permission) {
    let perm = permission;
    if (typeof permission === 'string') {
      perm = global.package.find(permission);
    }

    if (!(perm instanceof Permission)) {
      log.warn(`Not asking for _something_ of a PermissionContainer, its definitely not a Permission (type: ${typeof perm})!`);
      return false;
    }
    return this._permissions.has(perm);
  }

  /**
   * Removes a Permission.
   *
   * @param {Permission|string} permission the permission that should be removed
   * @returns {boolean} whether the permission could be removed or not
   */
  remove(permission) {
    let perm = permission;
    if (typeof permission === 'string') {
      perm = global.package.find(permission);
    }

    if (!(perm instanceof Permission)) {
      log.warn(`Not removing _something_ from a PermissionContainer, its definitely not a Permission (type: ${typeof perm})!`);
      return false;
    }
    this._permissions.delete(perm);
    return !this._permissions.has(perm);
  }

  /**
   * Unapplies the applied permissions from the PermissionContainer
   *
   * @param {PermissionContainer} container the permission container
   */
  unapplyPermissions(container) {
    if (!(this._permissions instanceof Set)) {
      log.warn('Tried to unapplyPermissions from a WeakSet PermissionContainer.');
      return;
    }

    this._permissions.forEach(permission => {
      container.remove(permission);
    });
  }

  /**
   * Default Argument getter. If no evaluation function is given in Permission.evaluate, this function will be called
   * to retrieve the arguments.
   *
   * @callback {PermissionContainer~defaultArgumentGetter}
   * @returns {Array<*>} an Array of arguments of any type.
   */
}
