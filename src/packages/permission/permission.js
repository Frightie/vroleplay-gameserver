/**
 * @title permission.js
 * @overview Permission Class Implementation
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

export default class Permission {
  /**
   * Creates a new Permission
   *
   * @param {string} name the Permission name
   * @param {Object} options options for the Permission
   * @param {string} option.friendlyName printable representation of the Permission (e.g. to show it to a user)
   */
  constructor(name, options) {
    /** @type string */
    this.name = name;

    const defaultOptions = {
      friendlyName: '',
    };

    /** @type {Object} */
    this.options = shared.utility.concat(options, defaultOptions);
    log.debug(`Permission "${name}" created.`, 3);
  }

  /**
   * Evaluates the Permission
   *
   * @param {...*} args evaluation arguments
   * @returns {boolean}
   */
  evaluate(...args) {
    if (typeof this.options.evaluator !== 'undefined') {
      return this.options.evaluator.apply(this, args);
    }
    return false;
  }
}
