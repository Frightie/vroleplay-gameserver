/**
 * @title account.js
 * @overview Account Database Model
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

import moment from 'moment';

/* global database */

/**
 * Account Model
 */
export default {
  structure: {
    id: 'integer',
    username: 'string',
    password: 'string',
    email: 'string',
    access_level: 'integer',
  },
  options: {
    primaryKey: 'id',
  },
  methods: {
    ban(issuer, duration, reason, callback) {
      // [n]m = n minutes (e.g. /ban somePlayer 30m ...)
      // [n]h = n hours
      // [n]d = n days
      const expirationDate = moment();
      const dur = parseInt(duration.slice(0, duration.length - 1), 10);

      if (isNaN(dur)) {
        callback();
        return;
      }

      let type;
      switch (duration.charAt(duration.length - 1)) {
      case 'h':
        type = 'hours';
        break;
      case 'd':
        type = 'days';
        break;
      default:
        type = 'minutes';
        break;
      }

      database.Ban.create({
        reason,
        account: this.id,
        issuer: issuer.account.id,
        expiration: expirationDate.add(dur, type).toDate(),
      }, callback);
    },
    getActiveBans(callback) {
      database.Ban.find({account: this.id, expiration: '>(SELECT CURRENT_TIMESTAMP())'}, bans => {
        callback(bans);
      });
    },
    getCharacters(callback) {
      this.hasMany(database.Character, characters => {
        callback(characters);
      });
    },
    getLoginAttempts(player, done) {
      database.LoginAttempt.find({
        date: '>(SELECT CURRENT_DATE()) - INTERVAL 60 MINUTE',
        ip_address: player.client.ipAddress,
        result: 'fail',
        orderBy: {
          field: 'id',
          ascending: false,
        },
      }, done);
    },
    loginAttempt(player, result) {
      database.LoginAttempt.create({account: this.id, ip_address: player.client.ipAddress, date: new Date().toISOString(), result: result});
    },
  },
};
