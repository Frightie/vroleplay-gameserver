/**
 * @title loginAttempt.js
 * @overview Account Database Model
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

/**
 * LoginAttempt Model
 */
export default {
  structure: {
    id: 'integer',
    account: 'integer',
    ip_address: 'string',
    date: 'timestamp',
    result: 'string',
  },
  options: {
    primaryKey: 'id',
    tableName: 'gs_login_attempts',
  },
};
