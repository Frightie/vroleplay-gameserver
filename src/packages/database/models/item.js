/**
 * @title item.js
 * @overview Item Database Model
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

/**
 * Item Model
 */
export default {
  structure: {
    id: 'integer',
    descriptor: 'integer',
    storage: 'integer',
    quantity: 'integer',
    attributes: 'json',
  },
  options: {
    primaryKey: 'id',
  },
};
