/**
 * @title vehicle.js
 * @overview Vehicle Database Model
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

/**
 * Vehicle Model
 */
export default {
  structure: {
    id: 'integer',
    owner: 'integer',
    owner_type: 'string',
    model_name: 'string',
    primary_color: 'RGBA',
    secondary_color: 'RGBA',
    position: 'Vector3f',
    rotation: 'Vector3f',
    customization: 'json',
  },
  options: {
    primaryKey: 'id',
  },
};
