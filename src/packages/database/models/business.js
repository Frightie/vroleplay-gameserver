/**
 * @title business.js
 * @overview Business Database Model
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

/**
 * Business Model
 */
export default {
  structure: {
    id: 'integer',
    owner: 'integer',
    name: 'string',
    active: 'integer',
    entrance: 'json',
    level: 'integer',
    price: 'integer',
    fee: 'integer',
  },
  options: {
    primaryKey: 'id',
    tableName: 'businesses',
  },
};
