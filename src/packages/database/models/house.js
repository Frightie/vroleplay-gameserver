/**
 * @title house.js
 * @overview House Database Model
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

/**
 * House Model
 */
export default {
  structure: {
    id: 'integer',
    owner: 'integer',
    entrance: 'json',
    price: 'integer',
    level: 'integer',
  },
  options: {
    primaryKey: 'id',
  },
};
