/**
 * @title storage.js
 * @overview Storage Database Model
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

/**
 * Storage Model
 */
export default {
  structure: {
    id: 'integer',
    descriptor: 'integer',
    owner: 'integer',
    owner_type: 'string',
    attributes: 'json',
  },
  options: {
    primaryKey: 'id',
  },
};
