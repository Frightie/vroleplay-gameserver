/**
 * @title boost.js
 * @overview Boost Database Model
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

/**
 * Boost Model
 */
export default {
  structure: {
    id: 'integer',
    applied_to: 'integer',
    stat: 'string',
    remaining: 'integer',
    value: 'integer',
  },
  options: {
    primaryKey: 'id',
  },
};
