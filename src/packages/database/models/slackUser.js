/**
 * @title slackUser.js
 * @overview Model for the slack_auth table (associative accountid<->slack_name table)
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

/**
 * SlackUser Model
 */
export default {
  structure: {
    id: 'integer',
    account: 'integer',
    slack_name: 'string',
  },
  options: {
    primaryKey: 'id',
    tableName: 'slack_auth',
  },
};
