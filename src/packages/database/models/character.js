/**
 * @title character.js
 * @overview Character Database Model
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

/**
 * Character Model
 */
export default {
  structure: {
    id: 'integer',
    account: 'integer',
    name: 'string',
    password: 'string',
    last_pos: 'Vector3f',
    playtime: 'integer',
    duty_time: 'integer',
    last_paycheck: 'integer',
    experience: 'integer',
    skin: 'integer',
    cash: 'integer',
    bank: 'integer',
    phone: 'string',
    faction: 'integer',
    faction_rank: 'string',
    stats: 'json',
    last_online: 'timestamp',
  },
  options: {
    primaryKey: 'id',
  },
  methods: {
    getPlayer() {
      // FIXME: Accessing this and calling GTA:MP Functions afterwards crashes the server. Temp. workaround: use .refresh() of CPlayer
      for (const player of vrp.player.manager.players.values()) {
        if (player.character === this) {
          return player;
        }
      }
    },
  },
};
