/**
 * @title faction.js
 * @overview Faction Database Model
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

/**
 * Faction Model
 */
export default {
  structure: {
    id: 'integer',
    name: 'string',
    acronym: 'string',
    leader: 'integer',
    db_ranks: 'json',
  },
  options: {
    primaryKey: 'id',
  },
};
