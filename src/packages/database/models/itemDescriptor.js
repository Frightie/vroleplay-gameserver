/**
 * @title itemDescriptor.js
 * @overview ItemDescriptor Database Model
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

/**
 * ItemDescriptor Model
 */
export default {
  structure: {
    id: 'integer',
    name: 'string',
    description: 'string',
    type: 'string',
    attributes: 'json',
    weight: 'float',
    volume: 'float',
    other: 'json',
  },
  options: {
    primaryKey: 'id',
    tableName: 'item_descriptors',
  },
};
