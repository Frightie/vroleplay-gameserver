/**
 * @title ban.js
 * @overview Ban Database Model
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

/**
 * Ban Model
 */
export default {
  structure: {
    id: 'integer',
    account: 'integer',
    ip: 'string',
    issuer: 'integer',
    issued: 'timestamp',
    expiration: 'timestamp',
    reason: 'string',
  },
  options: {
    primaryKey: 'id',
    hooks: {
      afterCreate(ban) {
        ban.permanent = false;
        if (ban.expiration.getFullYear() === 2038) {
          ban.permanent = true;
        }
      },
    },
  },
};
