/**
 * @title main.js
 * @overview Main Package File
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

// Predefined V:RP Header to install our own error types & handler
import error from '../../shared/error';
import errorHandler from '../../shared/errorHandler';
error.install(global);
process.on('uncaughtException', errorHandler);

import fs from 'fs';
import path from 'path';
import Package from '../../shared/classes/package';
import ORM from './orm/orm';

class Database extends Package {
  /**
   * Constructs the Package
   * @constructor
   */
  constructor() {
    super('database');
  }

  /**
   * Starts the Package
   */
  start() {
    super.start();
  }

  /**
   * Connects to the database
   *
   * @param {function (orm: ORM)} callback callback when connected to the databse
   */
  connect(callback) {
    log.debug('Establishing database connection.', 2);

    const _orm = new ORM({
      host: 'localhost',
      user: 'root',
      password: '',
      database: 'vrp',
    }, err => {
      if (err) {
        throw new DatabaseError('Error connecting to the database.');
      }

      log.print('Connection to Database established.');

      this.connection = _orm;

      // Register custom types
      _orm.customTypes.set('Vector3f', {
        check(data) {
          return typeof data === 'object' && data.hasOwnProperty('x') && data.hasOwnProperty('y') && data.hasOwnProperty('z');
        },
        encode(data) {
          if (data.hasOwnProperty('x') && data.hasOwnProperty('y') && data.hasOwnProperty('z')) {
            const {x, y, z} = data;
            return JSON.stringify({x, y, z});
          }
          log.warn(`Could not convert Data to Vector3f, data: ${data}`);
          return JSON.stringify({x: 0, y: 0, z: 0});
        },
        decode(data) {
          let parsedData;
          try {
            parsedData = JSON.parse(data);
          } catch(e) {
            log.warn(`Error decoding Vector3f: ${e}, data: '${data}'`);
            return new Vector3f(0.0, 0.0, 0.0);
          }
          if (parsedData.hasOwnProperty('x') && parsedData.hasOwnProperty('y') && parsedData.hasOwnProperty('z')) {
            const {x, y, z} = parsedData;
            return new Vector3f(x, y, z);
          }
          log.warn(`Could not decode data to Vector3f, data: ${parsedData}`);
          return new Vector3f(0.0, 0.0, 0.0);
        },
      });
      _orm.customTypes.set('RGBA', {
        check(data) {
          return typeof data === 'object' && data.hasOwnProperty('r') && data.hasOwnProperty('g') && data.hasOwnProperty('b') && data.hasOwnProperty('a');
        },
        encode(data) {
          if (data.hasOwnProperty('r') && data.hasOwnProperty('g') && data.hasOwnProperty('b') && data.hasOwnProperty('a')) {
            const {r, g, b, a} = data;
            return JSON.stringify({r, g, b, a});
          }
          log.warn(`Could not convert Data to RGBA, data: ${data}`);
          return JSON.stringify({r: 0, g: 0, b: 0, a: 255});
        },
        decode(data) {
          let parsedData;
          try {
            parsedData = JSON.parse(data);
          } catch(e) {
            log.warn(`Error decoding RGBA: ${e}, data: '${data}'`);
            return new RGBA(0, 0, 0, 255);
          }
          if (parsedData.hasOwnProperty('r') && parsedData.hasOwnProperty('g') && parsedData.hasOwnProperty('b') && parsedData.hasOwnProperty('a')) {
            const {r, g, b, a} = parsedData;
            return new RGBA(r, g, b, a);
          }
          log.warn(`Could not decode data to RGBA, data: ${parsedData}`);
          return new RGBA(0, 0, 0, 255);
        },
      });

      // Autoloading models

      // We'll do everything synchroniously here because this is mandatory anyway.
      log.debug('Loading Models', 4);
      if (!fs.existsSync(path.join(__dirname, '/models'))) {
        throw new FatalError('Models Directory does not exist');
      }

      const files = fs.readdirSync(path.join(__dirname, '/models'));

      for (let i = 0; i < files.length; i++) {
        if (path.extname(files[i]) !== '.js') {
          continue;
        }

        // Load Models
        let model;
        try {
          model = require(path.join(__dirname, '/models/', files[i]));
        } catch (e) {
          throw new FatalError(e);
        }

        let modelName;
        if (typeof model.name !== 'undefined') {
          modelName = model.name;
        } else {
          modelName = path.basename(files[i], '.js');
          modelName = modelName[0].toUpperCase() + modelName.slice(1);
        }
        if (typeof model.structure === 'undefined') {
          throw new FatalError(`Model '${modelName}' has no structure.`);
        }

        this[modelName] = _orm.defineModel(modelName, model.structure, model.options, model.methods);

        log.debug(`Model '${modelName}' loaded.`, 3);
      }
      callback(_orm);
    });
  }

  receiveDependencies(deps) { super.receiveDependencies(global, deps); }
  receiveVRPNamespace(namespace) { super.receiveVRPNamespace(global, namespace); }
}

// Register globals
global.package = new Database();
global[global.package.name] = global.package;
