/**
 * @title instance.js
 * @overview ORM Model Instance
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

import sql from 'squel';

/**
 * Instance of a Database Model.
 */
export default class Instance {
  /**
   * Creates a new Instance of a Model.
   *
   * @this {Instance}
   * @param {Model} model the database model this is based on.
   * @param {Object} data the loading data of the Instance.
   * @param {boolean} [addToCollection=true] whether the instance should be added to the instance collection
   */
  constructor(model, data, addToCollection = true) {
    /** @type {orm.Model} */
    this.model = model;

    /** @type {Array<string>} */
    this.modified = [];

    /** @type {boolean} */
    this.dontSave = false;

    /** @type {Object} */
    this.values = data;

    /** @type {Object} */
    this.lastSaved = shared.utility.concat({}, data);

    /** @type {Object} */
    /** @private */ this._objects = {};

    const registerProp = prop => {
      Object.defineProperty(this, prop, {
        get: function get() { return this.values[prop]; },
        set: function set(value) { if (value !== this.values[prop]) { this.modified[prop] = true; } if (this.lastSaved[prop] === value) { delete this.modified[prop]; } this.values[prop] = value; },
      });
    };

    for (const idx in data) {
      if (data.hasOwnProperty(idx)) {
        // Create setters and getters so we can track changes to the values
        registerProp(idx);
      }
    }
    this.refresh(() => {}, data);

    // Apply methods
    for (const idx in this.model.methods) {
      if (this.model.methods.hasOwnProperty(idx)) {
        this[idx] = this.model.methods[idx].bind(this);
      }
    }

    if (typeof this.model.options.hooks.afterCreate !== 'undefined') {
      this.model.options.hooks.afterCreate(this);
    }

    if (addToCollection) {
      this.model._instances.add(this);
    }
    log.debug(`Instance of Model "${this.model.name}" created.`, 3);
  }

  /**
   * Deletes the instance and invalidates it (to prevent further access)
   *
   * @param {function ()} done callback
   */
  destroy(done = () => {}) {
    this.deleting = true;
    this.model.destroy(this, done);
  }

  /**
   * Encodes the Instance data
   *
   * @returns {Object} the data
   */
  encode() {
    const obj = {};
    for (const column in this.model.columns) {
      if (!this.model.columns.hasOwnProperty(column)) {
        continue;
      }
      // Type conversion
      const desiredType = this.model.columns[column].toLowerCase();

      switch (desiredType) {
      case 'json':
        obj[column] = shared.utility.mysqlEscape(JSON.stringify(this.values[column]));
        break;
      case 'string':
        obj[column] = shared.utility.mysqlEscape(this.values[column]);
        break;
      case 'timestamp':
        try {
          obj[column] = this.values[column].toISOString();
        } catch(e) {
          log.warn(`could not encode column ${column} in ${this.model.name}. Value: ${this.values[column]}`);
          obj[column] = new Date().toISOString();
        }
        break;
      default:
        obj[column] = this.values[column];
        break;
      }
    }
    return obj;
  }

  /**
   * Relationship: A Instance can own another single Instance/Model. This function returns it.
   *
   * @param {Model} other the related model
   * @param {function (instance: Instance)} done callback
   * @param {string} [column = ''] optional column name
   */
  hasOne(other, done, column = '') {
    let col = column;
    col = column === '' ? this.model.name.toLowerCase() : column;

    const pattern = {};
    pattern[col] = this[this.model.options.primaryKey];
    other.first(pattern, instance => {
      done(instance);
    });
  }

  /**
   * Relationship: A Instance can be the owner of other Instances/Models. This function returns them
   *
   * @param {Model} other the related model
   * @param {function (instances: Array<Instance>)} done callback
   * @param {string} [column = ''] optional column name
   */
  hasMany(other, done, column = '') {
    let col = column;
    col = column === '' ? this.model.name.toLowerCase() : column;

    const pattern = {};
    pattern[col] = this[this.model.options.primaryKey];
    other.find(pattern, instances => {
      done(instances);
    });
  }

  /**
   * Merges the current data with remote data. Saves it afterwards.
   *
   * @param {Object} data remote data
   * @param {function ()} done callback
   */
  merge(data, done) {
    const changes = {};
    for (const column in this.model.columns) {
      // Local changes prioritized
      if (typeof this.modified[column] === 'undefined') {
        changes[column] = data[column];
      }
    }
    this.refresh(() => {
      done();
      this.save(() => {});
    }, changes);
  }

  /**
   * Refreshs the data of an Instance
   *
   * @param {function ()} done callback
   * @param {Object} [optData] refresh data
   */
  refresh(done, optData) {
    const data = optData;

    const fnApply = newData => {
      // Convert data
      for (const column in this.model.columns) {
        if (typeof newData[column] === 'undefined') {
          continue;
        }

        if (typeof this.model.columns[column] === 'undefined') {
          throw new DatabaseError(`Model "${this.model.name}" is misconfigured. Columns do not match.`);
        }

        switch (this.model.columns[column].toLowerCase()) {
        case 'string':
          newData[column] = newData[column].toString();
          break;
        case 'integer':
        case 'number':
          try {
            newData[column] = parseInt(newData[column], 10);
          } catch (err) {
            log.warn(`Invalid Integer in Model "${this.model.name}", field "${column}"`);
            newData[column] = 0;
          }
          break;
        case 'float':
          try {
            newData[column] = parseFloat(newData[column]);
          } catch (err) {
            log.warn(`Invalid Float in Model "${this.model.name}", field "${column}"`);
            newData[column] = 0.0;
          }
          break;
        case 'json':
          try {
            this._objects[column] = newData[column];
            newData[column] = JSON.parse(newData[column]);
          } catch (err) {
            log.warn(`Invalid JSON in Model "${this.model.name}", field "${column}"`);
            newData[column] = {};
          }
          break;
        case 'timestamp':
          newData[column] = new Date(newData[column]);
          break;
        default:
          if (this.model.orm.customTypes.has(this.model.columns[column])) {
            newData[column] = this.model.orm.customTypes.get(this.model.columns[column]).decode(newData[column]);
          }
          break;
        }
        this.values[column] = newData[column];
      }
    };

    if (typeof data !== 'undefined') {
      fnApply(data);
      done();
      return;
    }

    const _sql = sql.select().from(this.model.options.tableName);

    for (const column in this.model.columns) {
      if (this.model.columns.hasOwnProperty(column)) {
        _sql.field(column);
      }
    }

    _sql.where(`${this.model.options.primaryKey} = '${this[this.model.options.primaryKey]}'`);
    this.model.orm.query(_sql.toString(), (err, result) => {
      if (err) {
        throw new DatabaseError(err);
      }

      if (result.length === 0) {
        throw new DatabaseError('Refresh Model Data: invalid Instance?');
      }

      for (let _ in this.modified) { // eslint-disable-line
        this.merge(result[0], done);
        return;
      }
      fnApply(result[0]);
      done();
    });
  }

  /**
   * Saves all changes of this instance to the database.
   *
   * @param {function (success: bool)} [callback] the function to call when save finished.
   */
  save(callback = () => {}) {
    if (this.deleting || this.deleted || this.dontSave) {
      callback(false);
      return;
    }

    if (typeof this.model.options.hooks.beforeSave !== 'undefined') {
      this.model.options.hooks.beforeSave(this);
    }

    const saveData = {};

    for (const idx in this._objects) {
      if (JSON.stringify(this.values[idx]) !== this._objects[idx]) {
        this.modified[idx] = true;
      }
    }

    let numChanges = 0;

    for (const key in this.modified) {
      if (this.modified.hasOwnProperty(key)) {
        numChanges++;
      }
    }

    // If there is no callback, we want to wait for the return and return (that sounds stupid)
    if (numChanges === 0) {
      callback(false);
      return;
    }

    const _sql = sql.update().table(this.model.options.tableName);

    // Let's make our query
    for (const idx in this.modified) {
      // Type conversion
      if (typeof this.model.columns[idx] === 'undefined') {
        log.warn(`Undefined Column "${idx}" in database model "${this.model.name}"`);
      }

      const desiredType = this.model.columns[idx].toLowerCase();

      if (typeof this.values[idx] !== desiredType && !(typeof this.values[idx] === 'object' && desiredType === 'json') &&
         !(typeof this.values[idx] === 'number' && (desiredType === 'integer' || desiredType === 'float')) && !(desiredType === 'timestamp' && typeof this.values[idx].getDate === 'function') &&
         !(this.model.orm.customTypes.has(this.model.columns[idx]) && this.model.orm.customTypes.get(this.model.columns[idx]).check(this.values[idx]))) {
        log.warn(`SKIPPING -- Model: "${this.model.name}": Unmatched type: field "${idx}", desired type: "${desiredType}", actual type: "${typeof this.values[idx]}"`);
        if (numChanges === 1) {
          callback(false);
          return;
        }
        continue;
      }

      switch (desiredType) {
      case 'integer':
        saveData[idx] = parseInt(this.values[idx], 10);
        if (isNaN(saveData[idx])) {
          throw new DatabaseError(`Could not convert field '${idx}' to integer`);
        }
        break;
      case 'float':
        saveData[idx] = parseFloat(this.values[idx]);
        if (isNaN(saveData[idx])) {
          throw new DatabaseError(`Could not convert field '${idx}' to float`);
        }
        break;
      case 'json':
        saveData[idx] = shared.utility.mysqlEscape(JSON.stringify(this.values[idx]));
        break;
      case 'string':
        saveData[idx] = shared.utility.mysqlEscape(this.values[idx]);
        break;
      case 'timestamp':
        saveData[idx] = this.values[idx].toISOString();
        break;
      default:
        if (this.model.orm.customTypes.has(this.model.columns[idx])) {
          saveData[idx] = this.model.orm.customTypes.get(this.model.columns[idx]).encode(this.values[idx]);
        } else {
          saveData[idx] = this.values[idx];
        }
        break;
      }
      _sql.set(idx, saveData[idx]);
    }

    this.lastSaved = shared.utility.concat({}, this.values);
    _sql.where(`${this.model.options.primaryKey} = '${this.values[this.model.options.primaryKey]}'`);
    this.modified = [];
    this.model.orm.query(_sql.toString(), (err, res) => {
      if (err) {
        throw new DatabaseError(err);
      }

      if (!this.deleted && res.affectedRows !== 1) {
        throw new DatabaseError('Error saving Model Instance!');
      }

      callback(res);
    });
  }
}
