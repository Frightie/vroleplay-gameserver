/**
 * @title orm.js
 * @overview ORM
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

import Model from './model';
import mysql from 'mysql';

/**
 * ORM Class.
 */
export default class ORM {
  /**
   * Connects to the database with the given configuration
   *
   * @param {Object} data the login data (host, user, password, database).
   * @param {function (success: bool)} [callback] callback when an error happened.
   */
  constructor(data, callback = () => {}) {
    /** @type {mysql.Connection} */
    this.connection = mysql.createConnection(data);

    this.connection.connect(callback);

    /** @type {Map<string, Object>} */
    this.customTypes = new Map();

    /** @type {Object} */
    this.models = {};
  }

  /**
   * Defines a new Model.
   *
   * @param {string} name the name of the model.
   * @param {Object} structure the structure of the models (columns with respective types).
   * @param {Object} options model options.
   * @param {Object} [methods=Object] methods for model instances
   * @returns {undefined}
   */
  defineModel(name, structure, options, methods = {}) {
    if (typeof this.models[name] !== 'undefined') {
      return false;
    }

    this.models[name] = new Model(name, this, structure, options, methods);

    return this.models[name];
  }

  /**
   * Removes a model from the orm.
   *
   * @this {orm}
   * @param {string} name the name of the Model.
   * @returns {undefined}
   */
  undefineModel(name) {
    if (typeof this.models[name] !== 'undefined') {
      return true;
    }

    delete this.models[name];
  }

  /**
   * Custom query.
   *
   * @param {string} query query.
   * @returns {object} result.
   */
  query(query, callback) {
    log.debug(`SQL Query: '${query}'`, 2);
    this.connection.query(query, callback);
  }
}
