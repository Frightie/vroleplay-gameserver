/**
 * @title model.js
 * @overview ORM Database Model
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

import Instance from './instance';
import sql from 'squel';

/**
 * Model Class.
 */
export default class Model {
  /**
   * Model Constructor. A Model is a script representation of a database table.
   *
   * @param {string} name the name of the model.
   * @param {orm} orm the orm instance.
   * @param {Object} structure the structure of the model.
   * @param {Object} options options like primaryKey, tableName.
   * @param {Object} [methods = {}] methods for the model instances
   */
  constructor(name, orm, structure, options, methods = {}) {
    /** @type {string} */
    this.name = name;

    /** @type {orm} */
    /* @protected */ this.orm = orm;

    /** @type {[function ()]} */
    this.destroyHandlers = [];

    /** @type {string[]} */
    this.columns = structure;
    /** @type {Object} */
    this.options = options;

    /** @type {Object} */
    this.methods = methods;

    /** @type {Collection} */
    /** @private */ this._instances = new Set();

    if (typeof this.options.tableName === 'undefined') {
      this.options.tableName = this.name.toLowerCase() + 's';
    }

    if (typeof this.options.hooks === 'undefined') {
      this.options.hooks = {};
    }

    if (this.columns.hasOwnProperty('model') || this.columns.hasOwnProperty('modified') || this.columns.hasOwnProperty('values')) {
      throw new DatabaseError(`Misconfiguration: unallowed column name (model, modified or values) in model ${this.name}`);
    }

    orm.query(`SHOW TABLES LIKE '${this.options.tableName}';`, (err, rows) => {
      if (err || rows.length === 0) {
        throw new DatabaseError(`Invalid tableName '${this.options.tableName}' for Model '${this.name}'`);
      }
      log.debug(`Model Initialization for Model '${this.name}' done`, 3);
    });
  }

  /**
   * Finds all records in the Model.
   *
   * @this {Model}
   * @param {function (instances: Array<Instance>)} callback function to call when the function finished.
   */
  all(callback) {
    const _sql = sql.select().from(this.options.tableName);

    for (const column in this.columns) {
      if (this.columns.hasOwnProperty(column)) {
        _sql.field(column);
      }
    }

    this.orm.query(_sql.toString(), (err, queryResult) => {
      if (err) {
        throw new DatabaseError(err);
      }

      const result = [];
      // Parse results
      let finished = 0;
      const recvInstance = instance => {
        finished++;
        result.push(instance);
        if (finished === queryResult.length) {
          callback(result);
        }
      };

      for (const row in queryResult) {
        if (queryResult.hasOwnProperty(row)) {
          this.getInstance(queryResult[row], recvInstance);
        }
      }
    });
  }

  /**
   * Destroys (deletes) an instance
   *
   * @param {Instance} instance the instance
   * @param {function ()} done callback
   */
  destroy(instance, done) {
    if (instance.deleted) {
      return;
    }
    instance.deleted = true;

    for (let i = 0; i < this.destroyHandlers.length; i++) {
      this.destroyHandlers[i](instance);
    }

    const sql_ = sql.delete().from(this.options.tableName).where(`${this.options.primaryKey} = '${instance[this.options.primaryKey]}'`);

    this.orm.query(sql_.toString(), err => {
      if (err) {
        throw new DatabaseError(err);
      }
      done();
    });
  }

  /**
   * Finds records in the Model by matching it with a query.
   *
   * @this {Model}
   * @param {string|number|Object} query the search pattern. (String: custom query, Number: find by primary key, Object: find by matching columns)
   * @param {function (instances: Array<Instance>)} callback function to call when the function finished.
   */
  find(query, callback) {
    const _sql = sql.select().from(this.options.tableName);

    // Escape
    for (const key in query) {
      if (typeof query[key] === 'string' && key !== 'queryStr') {
        query[key] = shared.utility.mysqlEscape(query[key]);
      }
    }

    this.makeQuery_(_sql, query);

    console.log(_sql.toString());
    this.orm.query(_sql.toString(), (err, queryResult) => {
      if (err) {
        throw new DatabaseError(err);
      }

      const result = [];
      if (queryResult.length === 0) {
        callback(result);
      }

      // Parse results
      let finished = 0;
      const recvInstance = instance => {
        finished++;
        result.push(instance);

        if (finished === queryResult.length) {
          callback(result);
        }
      };

      for (const row in queryResult) {
        if (queryResult.hasOwnProperty(row)) {
          this.getInstance(queryResult[row], recvInstance);
        }
      }
    });
  }

  /**
   * Alias to 'first'
   *
   * @this {Model}
   * @param {string|number|Object} query the search pattern. (String: custom query, Number: find by primary key, Object: find by matching columns)
   * @param {function (instance: Instance)} callback function to call when the function finished.
   * @returns {?Instance}
   */
  one(query, callback) {
    return this.first(query, callback);
  }

  /**
   * Finds one record with a matching query.
   *
   * @this {Model}
   * @param {string|number|Object} query the search pattern. (String: custom query, Number: find by primary key, Object: find by matching columns)
   * @param {function (instance: Instance)} callback function to call when the function finished.
   * @returns {?Instance}
   */
  first(query, callback) {
    if (typeof query.limit !== 'undefined') {
      throw new DatabaseError(`"limit" defined in Model.first. Query >> ${query}`);
    }

    // Escape
    for (const key in query) {
      if (typeof query[key] === 'string' && key !== 'queryStr') {
        query[key] = shared.utility.mysqlEscape(query[key]);
      }
    }

    const _sql = sql.select().from(this.options.tableName);

    // Prepare query
    this.makeQuery_(_sql, query);
    _sql.limit(1);

    this.orm.query(_sql.toString(), (err, result) => {
      if (err) {
        throw new DatabaseError(err);
      }

      if (result.length >= 2) {
        throw new DatabaseError(`result length ${result.length} in Model.first`);
      }

      if (typeof result[0] !== 'undefined') {
        this.getInstance(result[0], instance => {
          callback(instance);
        });
      } else {
        callback(undefined);
      }
    });
  }

  /**
   * Gets the Instance or creates a new one (making sure we have singleton instances)
   *
   * @param {Array} queryResult the result of the mysql query
   * @param {function (instance: Instance)} done callback
   */
  getInstance(queryResult, done) {
    let result;
    const fnAfterRefresh = () => {
      done(result);
    };

    for (const instance of this._instances.values()) {
      if (instance[this.options.primaryKey] === queryResult[this.options.primaryKey]) {
        for (const idx in instance.modified) {
          if (instance.modified.hasOwnProperty(idx)) {
            result = instance;
            instance.merge(queryResult, fnAfterRefresh);
            return true;
          }
        }

        result = instance;
        const encoded = instance.encode();

        for (const column in this.columns) {
          if (this.columns[column] === 'timestamp' && +queryResult[column] !== +encoded[column] || this.columns[column] !== 'timestamp' && queryResult[column] !== encoded[column]) {
            instance.refresh(fnAfterRefresh, queryResult);
            result = instance;
            return true;
          }
        }
        done(result);
        return true;
      }
    }

    if (typeof result === 'undefined') {
      result = new Instance(this, queryResult);
      done(result);
    }
  }

  /**
   * Parses a query.
   *
   * @this {Model}
   * @private
   * @param {squel} sqobj 'squel' object.
   * @param {string|number|Object} query the search pattern. (String: custom query, Number: find by primary key, Object: find by matching columns)
   * @returns {squel} the squel object.
   */
  makeQuery_(sqobj, query) {
    for (const column in this.columns) {
      if (this.columns.hasOwnProperty(column)) {
        sqobj.field(column);
      }
    }

    // How should a query object look like?
    // It is either a string (custom query)
    // Or an object with the columns and the value
    if (typeof query === 'string') {
      sqobj.where(query);
    } else if (typeof query === 'number') {
      sqobj.where(this.options.primaryKey + " = '" + query + "'");
    } else {
      if (typeof query !== 'object') {
        throw new DatabaseError('Query is neither an Object nor a string. And its not a number(primary key)');
      }

      for (const field in query) {
        if ((/^[<>=]{1,}/ig).test(query[field])) {
          if (this.columns[field] === 'timestamp') {
            sqobj.where(`${field} ${query[field].charAt(0)} ${query[field].substr(1)}`);
          } else {
            sqobj.where(`${field} ${query[field].charAt(0)} '${query[field].substr(1)}'`);
          }
        } else if (field === 'queryStr') {
          sqobj.where(query[field]);
        } else if (field === 'limit') {
          sqobj.limit(query[field]);
        } else if (field === 'orderBy') {
          sqobj.order(query[field].field, query[field].ascending === true);
        } else {
          sqobj.where(field + " = '" + query[field] + "'");
        }
      }
    }

    return sqobj;
  }

  /**
   * Creates a new model based on its data.
   *
   * @param {Object} data the instance data
   * @param {function (instance: Instance)} [callback] callback
   */
  create(data, callback = () => {}) {
    const _sql = sql.insert().into(this.options.tableName);

    for (const column in this.columns) {
      if (typeof data[column] !== 'undefined') {
        switch (this.columns[column].toLowerCase()) {
        case 'json':
          if (typeof data[column] !== 'object') {
            throw new DatabaseError('Type "json" required.');
          }
          data[column] = shared.utility.mysqlEscape(JSON.stringify(data[column]));
          break;
        case 'string':
          data[column] = shared.utility.mysqlEscape(data[column]);
          break;
        case 'timestamp':
          if (typeof data[column] === 'object') {
            data[column] = data[column].toISOString();
          }
          break;
        default:
          if (this.orm.customTypes.has(this.columns[column])) {
            data[column] = this.orm.customTypes.get(this.columns[column]).encode(data[column]);
          }
          break;
        }

        _sql.set(column, data[column]);
      }
    }

    this.orm.query(_sql.toString(), (err, result) => {
      if (err) {
        throw new DatabaseError(err);
      }
      if (result.affectedRows !== 1) {
        throw new DatabaseError(`No affected row in ${this.name}.create`);
      }

      const options = {};
      options[this.options.primaryKey] = result.insertId;
      this.first(options, instance => {
        return callback(instance);
      });
    });
  }
}
