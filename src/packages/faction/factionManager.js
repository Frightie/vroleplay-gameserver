/**
 * @title factionManager.js
 * @overview Faction Manager Implementation
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

import fs from 'fs';
import BaseManager from '../../shared/classes/baseManager';

/**
 * The FactionManager manages all factions. Factions get loaded
 * on startup by the FactionManager.
 *
 * @see {@link FactionManager#loadFactions}
 */
class FactionManager extends BaseManager {
  /**
   * Creates the Faction Manager
   *
   * @this {FactionManager}
   */
  constructor() {
    super('factions');
    /** @type {boolean} */
    this.initialized = false;
  }

  /**
   * Initializes the Faction manager
   *
   * @this {FactionManager}
   * @param {function ()} done callback
   */
  init(done) {
    log.print('Initializing Faction Manager');

    // Create the Hook for the Database Model
    vrp.database.Faction.options.hooks.beforeSave = faction => {
      const ranks = [];
      this.findById(faction.id).ranks.forEach(rank => {
        ranks.push(rank.object);
      });
      if (!shared.utility.isSameObject(faction.db_ranks, ranks)) {
        faction.db_ranks = ranks;
      }
    };

    this.loadFactions(succeeded => {
      if (succeeded === false) {
        log.error('Failed to load Factions');
      } else {
        this.initialized = true;
      }
      done();
    });
  }

  /**
   * Loads all Factions from the database
   *
   * @this {FactionManager}
   * @param {function (success: bool)} done callback
   */
  loadFactions(done) {
    const dir = './packages/faction/factions';
    fs.readdir(dir, (err, files) => {
      if (err) {
        done(false);
      }

      // Loop through all files and stat them
      for (let i = 0; i < files.length; i++) {
        const stat = fs.statSync(`${dir}/${files[i]}`);

        if (stat.isFile() && files[i].indexOf('.js') === files[i].length - 3 &&
            files[i].indexOf('.commands.') === -1) {
          const faction = new (require(`./factions/${files[i]}`))(files[i].slice(0, files[i].length - 3));
          global[faction.fname] = faction;
          this.factions.add(faction);
        }
      }

      done(true);
    });
  }

  /**
   * Returns a faction based on its database id
   *
   * @param {integer} id the database id
   * @returns {Faction|undefined}
   */
  findById(id) {
    for (const faction of this.factions.values()) {
      if (faction.id === id) {
        return faction;
      }
    }
  }
  /**
   * Registers all Permissions. Static Permissions are returned as string array,
   * dynamic permissions can be created using vrp.permission.create.
   */
  registerPermissions() {
    const arr = [];
    this.factions.forEach(faction => {
      arr.push(...faction.registerPermissions());
    });
    return arr;
  }
}

/**
 * FactionManager instance
 */
export default new FactionManager();
