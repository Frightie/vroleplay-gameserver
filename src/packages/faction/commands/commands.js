/**
 * @title commands.js
 * @overview Implements Package Commands
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

/**
  * @namespace
  */
const Commands = {};
export default Commands;

const defaultValidator = player => {
  return player.account !== null && player.character !== null && player.faction !== null;
};

Commands.faction = new shared.CommandGroup(['faction', 'f'], {});

Commands.faction.add(new shared.Command('invite', {
  parameters: [
    ['player', 'target'],
  ],
  validator: player => {
    if (!defaultValidator(player)) {
      return false;
    }
    return player.permissions.has('CAN_INVITE_PLAYER_TO_FACTION');
  },
}, (player, target) => {
  if (target.faction !== null) {
    return player.sendError('The targeted player already is part of a faction.');
  }

  if (player.faction.pendingInvites.has(target.character.name)) {
    return player.sendError('The targeted player already has a pending invitation.');
  }

  if (!target.permissions.has('CAN_JOIN_FACTION')) {
    return player.sendError('The targeted player is not allowed to join a faction.');
  }

  player.faction.invite(target, player);
  player.sendInfo(`Faction invitation was sent to ${target.character.name}.`);
}));

Commands.faction.add(new shared.Command('join', {
  parameters: [
    ['faction acronym', 'string'],
  ],
  validator: defaultValidator,
}, (player, acronym) => {
  for (const faction of global.package.manager.factions.values()) {
    if (faction.acronym === acronym) {
      player.acceptFactionInvitation(faction);
      return;
    }
  }
  player.sendError(`Could not find any faction with the acronym "${acronym}".`);
}));

Commands.faction.add(new shared.Command('kick', {
  parameters: [
    ['player', 'target'],
  ],
  validator: player => {
    if (!defaultValidator(player)) {
      return false;
    }
    return player.permissions.has('CAN_KICK_PLAYER_FROM_FACTION');
  },
}, (player, target) => {
  if (target.permission.has('IS_FACTION_LEADER')) {
    return player.sendError('You cannot kick a faction leader.');
  }
  if (player.faction.removeMember(target)) {
    target.sendInfo(`You were kicked out of the faction by ${player.character.name}.`);
    player.sendSuccess(`Kicked ${target.character.name} from the faction.`);
    return undefined;
  }
  return player.sendError(`Could not kick ${target.character.name}.`);
}));

Commands.faction.add(new shared.Command('leave', {
  parameters: [],
  validator: defaultValidator,
}, player => {
  if (player.permissions.has('IS_FACTION_LEADER')) {
    return player.sendError('You are the leader of the faction.');
  }

  if (player.faction.removeMember(player)) {
    return player.sendSuccess('You left the faction.');
  }
  return player.sendError('Could not leave the faction.');
}));

Commands.faction.add(new shared.Command('members', {
  parameters: [],
  validator: defaultValidator,
}, player => {
  let msg = '<table><tr><td>Name</td><td>Rank</td></tr>';
  player.faction.onlineMembers.forEach(member => {
    if (member.character === null) {
      return;
    }

    msg += `<tr><td>${member.character.name}</td><td>${member.factionRank.name}</td></tr>`;
  });
  msg += '</table>';

  player.sendMessage(msg);
}));

Commands.faction.add(new shared.Command('say', {
  parameters: [
    ['message', 'text'],
  ],
  validator: defaultValidator,
}, (player, message) => {
  player.faction.onlineMembers.forEach(member => {
    member.sendMessage(`(( [FACTION] ${player.character.name}: ${message} ))`, vrp.config.game.color.message.factionSayCommand);
  });
}));

Commands.faction.add(new shared.Command('setRank', {
  parameters: [
    ['player', 'target'],
    ['rank', 'text'],
  ],
  validator: player => {
    if (!defaultValidator(player)) {
      return false;
    }
    return player.permissions.has('IS_FACTION_LEADER');
  },
}, (player, target, rank) => {
  if (!player.faction.onlineMembers.has(target.character)) {
    return player.sendError(`${target.character.name} is not part of the faction.`);
  }

  if (!player.faction.ranks.has(rank)) {
    return player.sendError('Invalid rank.');
  }
  target.factionRank.unapplyPermissions(target.permissions);
  target.factionRank = player.faction.ranks.get(rank);
  target.factionRank.applyPermissions(target.permissions);
  target.character.faction_rank = rank;
  target.character.save();
  target.sendInfo(`Your new Faction Rank is "${rank}".`);
  return player.sendSuccess(`Set the rank of "${target.character.name}" to "${rank}"`);
}));
