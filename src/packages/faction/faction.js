/**
 * @title faction.js
 * @overview Faction Class Implementation
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

import fs from 'fs';

/**
 * A Faction is a organization of multiple Members. It has
 * certain ranks (user-defined). Players can get invited to factions,
 * get promoted in their ranks or get kicked from the Faction.
 */
export default class Faction {
  /**
   * Creates a new Faction
   */
  constructor(fname, acronym) {
    /** @type {string} */
    this.fname = fname;

    /** @type {Map.<string, Rank>} */
    this.ranks = new Map();

    /** @type {Set.<database.Character>} */
    this.types = new Set();

    /** @type {Map<database.Character, Player>} */
    this.onlineMembers = new Map();

    /** @type {Map<database.Character, Player>} */
    this.pendingInvites = new Map();

    /** @type {database.Faction} */
    this.dbModel = undefined;

    /** @type {Set<database.Character>} */
    this.members = new Set();

    /** @type {Set<VRPVehicle} */
    this.vehicles = new Set();


    vrp.database.Faction.first({acronym}, dbModel => {
      if (typeof dbModel === 'undefined') {
        throw new FatalError(`Could not find Faction Database Entry (${this.name})`);
      }
      this.dbModel = dbModel;
      this.loadRanks();

      // We want to access dbModel directly, so let's do it
      const fnRegisterProperty = prop => {
        Object.defineProperty(this, prop, {
          get() { return this.dbModel[prop]; },
          set(value) { this.dbModel[prop] = value; },
        });
      };
      for (const property in this.dbModel.values) {
        if (this.dbModel.values.hasOwnProperty(property)) {
          fnRegisterProperty(property);
        }
      }
      log.debug(`Faction '${acronym}' loaded.`, 3);

      vrp.database.Character.find({faction: dbModel.id}, characters => {
        for (let i = 0; i < characters.length; i++) {
          this.members.add(characters[i]);
        }
      });
      this.init();
    });


    // Try to register the commands
    if (fs.existsSync(`./packages/faction/factions/${fname}.commands.js`)) {
      const commands = require(`./factions/${fname}.commands.js`);

      // Register the commands
      for (const commandName in commands) {
        if (commands.hasOwnProperty(commandName)) {
          vrp.command.manager.register(commands[commandName]);
        }
      }
    }
  }

  /**
   * Initializes the Faction
   */
  init() {
    // stub
  }

  /**
   * Invite a Player to the faction
   *
   * @param {Player} target the targeted player
   * @param {Player} invitingPlayer the inviting player
   */
  invite(target, invitingPlayer) {
    if (this.pendingInvites.has(target.character)) {
      return;
    } else if (invitingPlayer.faction !== this || !invitingPlayer.permissions.has('CAN_INVITE_PLAYER_TO_FACTION')) {
      return;
    }
    this.pendingInvites.set(target.character, target);
    target.receiveFactionInvitation(invitingPlayer.faction, invitingPlayer);
  }

  /**
   * A player accepted an invitation
   *
   * @param {Player} player the player
   */
  invitationAccepted(player) {
    if (!this.pendingInvites.has(player.character)) {
      return null;
    }

    this.onlineMembers.forEach(member => {
      member.sendInfo(`${player.character.name} joined the faction.`);
    });
    player.permissions.remove('IS_FACTION_LEADER');
    player.factionRank = this.ranks.get('Member');
    player.factionRank.applyPermissions(player.permissions);
    player.character.faction = this.id;
    player.character.faction_rank = player.factionRank.name;
    player.character.save();
    this.members.add(player.character);
    this.onlineMembers.set(player.character, player);
    return this;
  }

  /**
   * Loads the Ranks from the database Model
   */
  loadRanks() {
    for (let i = 0; i < this.dbModel.db_ranks.length; i++) {
      const dbRank = this.dbModel.db_ranks[i];
      const rank = new global.package.Rank(dbRank.name, dbRank.destroyable);
      for (let i2 = 0; i2 < dbRank.permissions.length; i2++) {
        if (!rank.addPermission(dbRank.permissions[i2])) {
          log.warn(`Could not add Permission ${dbRank.permissions[i2]} to faction ${this.dbModel.name}`);
        }
      }
      this.ranks.set(rank.name, rank);
    }
  }

  /**
   * Registers all Permissions. Static Permissions are returned as string array,
   * dynamic permissions can be created using vrp.permission.create.
   */
  registerPermissions() {
    return [];
  }

  /**
   * Remove a Member from the faction
   *
   * @param {Player|database.Character} target the player or character to remove
   * @returns {boolean} whether the character was removed from the faction
   */
  removeMember(target) {
    const fnLeave = character => {
      if (!this.members.has(character)) {
        return false;
      }
      this.members.delete(character);
      this.onlineMembers.delete(character);
      character.faction = 0;
      character.faction_rank = '';
      character.save();

      this.onlineMembers.forEach(player => {
        player.sendInfo(`${character.character.name} left the faction.`);
      });
      return true;
    };

    if (typeof target.character !== 'undefined') {
      if (target.permissions.has('IS_FACTION_LEADER')) {
        return false;
      }

      target.factionRank.unapplyPermissions(target.permissions);
      target.factionRank = null;
      if (fnLeave(target.character)) {
        target.faction = null;
        return true;
      }
      return false;
    }
    return fnLeave(target);
  }
}
