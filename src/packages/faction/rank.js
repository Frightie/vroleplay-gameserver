/**
 * @title rank.js
 * @overview Faction Rank Implementation
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

/**
 * Ranks are part of a Faction. A Rank can be associated
 * with permissions to modify the Faction or use certain Faction
 * commands. A Rank always has a name.
 */
export default class Rank {
  /**
   * Creates a new Rank
   *
   * @param {string} name the Rank's name
   * @param {boolean} [destroyable=true] whether the Rank can be deleted
   */
  constructor(name, destroyable = true) {
    /** @type {string} */
    this.name = name;
    /** @type {boolean} */
    this.destroyable = destroyable;
    /** @type {Set.<Permission>} */
    this._permissions = vrp.permission.createContainer(true);
  }

  /**
   * Adds a Permission to the Rank
   *
   * @param {Permission|string} permission the permission to add
   * @returns {boolean} whether the permission has been added or not
   */
  addPermission(permission) {
    // If you ask yourself why these are functions: We have to notify
    // all active rank holders when a Permission has been added or removed
    return this._permissions.add(permission);
  }

  /**
   * Assigns the Permissions to the given Container
   *
   * @param {PermissionContainer} container the container
   */
  applyPermissions(container) {
    this.permissions.get().forEach(permission => {
      container.add(permission);
    });
  }

  /**
   * Returns the Object Representation of the current rank
   *
   * @type {Object} the Rank as a normal object
   */
  get object() {
    const obj = {
      name: this.name,
      destroyable: this.destroyable,
      permissions: [],
    };
    this.permissions.get().forEach(permission => {
      obj.permissions.push(permission.name);
    });
    return obj;
  }

  /**
   * Returns the PermissionContainer
   *
   * @type {PermissionContainer} the Rank's permissions.
   */
  get permissions() {
    return this._permissions;
  }

  /**
   * Removes a Permission from the Rank
   *
   * @param {Permission|string} permission the permission to remove
   * @returns {boolean} whether the permission has been removed or not
   */
  removePermission(permission) {
    return this._permissions.remove(permission);
  }

  /**
   * Unapplies the applied permissions from the PermissionContainer
   *
   * @param {PermissionContainer} container the permission container
   */
  unapplyPermissions(container) {
    this.permissions.get().forEach(permission => {
      container.remove(permission);
    });
  }
}
