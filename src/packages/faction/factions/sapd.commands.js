/**
 * @title sapd.commands.js
 * @overview SAPD Commands
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

const Commands = {};

/**
 * SAPD Commands
 */
export default Commands;

function validator(player) {
  return player.faction === global.sapd;
}

Commands.SayHello = new shared.Command('something', {
  validator,
  parameters: [
    ['Message', 'text'],
  ],
}, player => {
  player.debug('yup');
});
