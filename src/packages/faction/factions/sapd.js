/**
 * @title sapd.js
 * @overview SAPD Implementation
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

/**
 * San Andreas Police Department
 */
export default class SAPD extends global.package.Faction {
  /**
   * Creates the SAPD Faction
   *
   * @param {string} fname - Faction Name (internal)
   */
  constructor(fname) {
    super(fname, 'SAPD');
  }

  /**
   * initializes the SAPD
   */
  init() {
  }

  /**
   * Registers all Permissions. Static Permissions are returned as string array,
   * dynamic permissions can be created using vrp.permission.create.
   */
  registerPermissions() {
    return [];
  }
}
