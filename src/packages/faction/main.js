/**
 * @title main.js
 * @overview Main Package File
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

// Predefined V:RP Header to install our own error types & handler
import error from '../../shared/error';
import errorHandler from '../../shared/errorHandler';
error.install(global);
process.on('uncaughtException', errorHandler);

import Package from '../../shared/classes/package';
import Rank from './rank';
import Faction from './faction';

import _factionManager from './factionManager';

class FactionSystem extends Package {
  /**
   * Constructs the Package
   * @constructor
   */
  constructor() {
    super('faction');
  }

  /**
   * Starts the Package
   */
  start() {
    super.start();
    this.Rank = Rank;
    this.Faction = Faction;
    this.manager = _factionManager;
  }

  /**
   * Returns the Dependencies
   * @returns {[string]}
   */
  get dependencies() {
    return ['database', 'permission'];
  }

  /**
   * Registers all Permissions. Static Permissions are returned as string array,
   * dynamic permissions can be created using vrp.permission.create.
   */
  registerPermissions() {
    vrp.permission.create('CAN_JOIN_FACTION', {
      evaluator: player => player.faction === null,
    });
    return ['CAN_INVITE_PLAYER_TO_FACTION', 'CAN_KICK_PLAYER_FROM_FACTION',
            'IS_FACTION_LEADER', ...this.manager.registerPermissions()];
  }

  receiveDependencies(deps) { super.receiveDependencies(global, deps); }
  receiveVRPNamespace(namespace) { super.receiveVRPNamespace(global, namespace); }
}

// Register globals
global.package = new FactionSystem();
global[global.package.name] = global.package;
