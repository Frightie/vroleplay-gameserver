/**
 * @title main.js
 * @overview Main Package File
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

// Predefined V:RP Header to install our own error types & handler
import error from '../../shared/error';
import errorHandler from '../../shared/errorHandler';
error.install(global);
process.on('uncaughtException', errorHandler);

import Package from '../../shared/classes/package';
import AccessLevel from './accessLevel';

/**
 * AdminSystem Package
 */
class AdminSystem extends Package {
  /**
   * Constructs the Package
   * @constructor
   */
  constructor() {
    super('admin');

    this.levels = {};
  }

  get dependencies() {
    return ['permission'];
  }

  /**
   * Starts the Package
   */
  start() {
    super.start();
  }

  /**
   * Initializes the Package
   */
  init() {
    const modPermissions = [
      'ENFORCE_2FA', 'CAN_KICK', 'CAN_LIST_PLAYERS', 'CAN_TELEPORT',
      'CAN_USE_ADMIN_CHAT', 'CAN_MUTE_PLAYER',
    ];
    const adminPermissions = [
      ...modPermissions, 'CAN_MANIPULATE_WEATHER', 'CAN_BAN', 'CAN_SEND_GLOBAL_ADMIN_MESSAGE',
      'CAN_TOGGLE_OOC',
    ];
    const hadminPermissions = [
      ...adminPermissions, 'CAN_CREATE_VEHICLE',
    ];
    const managerPermissions = [
      ...hadminPermissions,
    ];
    const devPermissions = managerPermissions;

    this.levels = {
      Player: new AccessLevel('Player', 0),
      Moderator: new AccessLevel('Moderator', 1, modPermissions),
      Administrator: new AccessLevel('Administrator', 2, adminPermissions),
      HeadAdministrator: new AccessLevel('Head Administrator', 3, hadminPermissions),
      Manager: new AccessLevel('Manager', 4, managerPermissions),
      Developer: new AccessLevel('Developer', 42, devPermissions),
    };
  }

  /**
   * Converts the numeric representation of an access level to the object
   *
   * @param {integer} number numeric access level representation
   * @returns {AccessLevel|undefined}
   */
  numberToLevel(number) {
    for (const idx in this.levels) {
      if (this.levels[idx].levelNumber === number) {
        return this.levels[idx];
      }
    }
  }

  /**
   * Registers all Permissions. Static Permissions are returned as string array,
   * dynamic permissions can be created using vrp.permission.create.
   */
  registerPermissions() {
    return ['ENFORCE_2FA', 'CAN_KICK', 'CAN_BAN', 'CAN_TELEPORT', 'CAN_LIST_PLAYERS',
            'CAN_MANIPULATE_WEATHER', 'CAN_USE_ADMIN_CHAT', 'CAN_SEND_GLOBAL_ADMIN_MESSAGE',
            'CAN_TOGGLE_OOC', 'CAN_MUTE_PLAYER', 'CAN_CREATE_VEHICLE'];
  }


  // Do not remove this. Otherwise Jan will slowly sneak up to you and rip your anus apart.
  receiveDependencies(deps) { super.receiveDependencies(global, deps); }
  receiveVRPNamespace(namespace) { super.receiveVRPNamespace(global, namespace); }
}

// Register globals
global.package = new AdminSystem();
global[global.package.name] = global.package;
