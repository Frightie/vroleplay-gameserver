/**
 * @title commands.js
 * @overview Implements Commands
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

import debugCommands from './debugCommands';
import playerCommands from './playerCommands';
import worldCommands from './worldCommands';
import ChatCommands from './chatCommands';

const adminCommands = new shared.CommandGroup(['admin', 'a'], {});

// Register the sub-commands
debugCommands(adminCommands);
playerCommands(adminCommands);
worldCommands(adminCommands);
ChatCommands.register(adminCommands);

/**
 * Admin Package Commands
 */
export default {
  adminCommands,
  ChatCommands,
};

function validator(player) {
  return player.playing;
}

function toggleTarget(target, toggle) {
  const fnGetValue = current => {
    if (typeof toggle === undefined) {
      return !current;
    }
    return toggle;
  };

  const targets = {
    'ooc': () => {
      vrp.config.game.enableOOC = fnGetValue(vrp.config.game.enableOOC);

      let state = 'enabled';
      if (!vrp.config.game.enableOOC) {
        state = 'disabled';
      }

      vrp.player.manager.broadcast(`Global OOC chat is now ${state}.`);
    },
  };

  if (!targets.hasOwnProperty(target)) {
    return false;
  }

  targets[target](toggle);
  return true;
}

function checkTarget(player, target) {
  const targetPermissions = {
    ooc: ['CAN_TOGGLE_OOC'],
  };

  if (targetPermissions.hasOwnProperty(target)) {
    for (let i = 0; i < targetPermissions[target].length; i++) {
      if (!player.permissions.has(targetPermissions[target][i])) {
        player.sendError(`You are not allowed to lock '${target}'`);
        return false;
      }
    }
  }
  return true;
}

adminCommands.add(new shared.Command('lock', {
  validator,
  parameters: [
    ['target', 'string'],
  ],
}, (player, target) => {
  if (!checkTarget(player, target)) {
    return false;
  }

  if (!toggleTarget(target, false)) {
    return player.sendError(`Could not lock '${target}', internal error.`);
  }
  return player.sendSuccess(`Locked '${target}'.`);
}));

adminCommands.add(new shared.Command('toggle', {
  validator,
  parameters: [
    ['target', 'string'],
  ],
}, (player, target) => {
  if (!checkTarget(player, target)) {
    return false;
  }

  if (!toggleTarget(target)) {
    return player.sendError(`Could not unlock '${target}', internal error.`);
  }
  return player.sendSuccess(`Unlocked '${target}'.`);
}));

adminCommands.add(new shared.Command('unlock', {
  validator,
  parameters: [
    ['target', 'string'],
  ],
}, (player, target) => {
  if (!checkTarget(player, target)) {
    return false;
  }

  if (!toggleTarget(target, true)) {
    return player.sendError(`Could not unlock '${target}', internal error.`);
  }
  return player.sendSuccess(`Unlocked '${target}'.`);
}));

adminCommands.add(new shared.Command('vehicle', {
  validator,
  parameters: [
    ['name', 'text'],
  ],
  permissions: ['CAN_CREATE_VEHICLE'],
}, (player, name) => {
  const vehicle = new Vehicle(new Vector3f(player.position.x, player.position.y, player.position.z + 0.51), GTAHash(name));

  if (vehicle) {
    setTimeout(() => {
      // vehicle.SetOccupant(0, player);
      log.implement('Set Player in Vehicle in /a vehicle');
    }, 500);
    player.sendSuccess(`Created Vehicle '${name}'.`);
  } else {
    player.sendError(`Could not create the vehicle.`);
  }
}));
