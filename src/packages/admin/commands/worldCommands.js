/**
 * @title worldCommands.js
 * @overview Implements World related Admin Commands
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

const defaultValidator = player => {
  return player.account !== null && player.permissions.has('CAN_MANIPULATE_WEATHER') && player.character !== null;
};

export default adminCommands => {
  adminCommands.add(new shared.Command(['savePos', 'sp'], {
    parameters: [
      ['additional comment', 'text'],
    ],
    validator: defaultValidator,
  }, (player, comment) => {
    const msg = `{ x: ${player.position.x}, y: ${player.position.y}, z: ${player.position.z} }; // ${comment}`;
    player.sendInfo(msg);
    shared.integration.slack.sendMessage(msg, {
      title: `SavePos from "${player.account.name}"`,
      channel: '#interesting-positions',
      symbol: 'checkered_flag',
    });
  }));

  adminCommands.add(new shared.Command('weather', {
    parameters: [
      ['id', 'string'],
    ],
    validator: defaultValidator,
  }, (player, weather) => {
    vrp.server.world.transition(vrp.server.world.transitions[weather]);
    log.debug(`Player ${player.character.name} started transition ${weather}`);
  }));
};
