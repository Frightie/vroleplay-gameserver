/**
 * @title chatCommands.js
 * @overview Implements Chat related Commands
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

const validator = player => {
  return player.character !== null;
};

const ChatCommands = {};

/**
 * ChatCommands for the Admin Package
 */
export default ChatCommands;

function adminChat(player, message) {
  vrp.player.manager.players.forEach(tempPlayer => {
    if (player.accessLevel !== vrp.admin.levels.Player) {
      tempPlayer.sendMessage(`(Admin Chat) ${player.character.name}: ${message}`, vrp.config.game.color.message.adminChatCommand);
    }
  });
  shared.integration.slack.sendMessage(`(Admin Chat) ${player.character.name}[${player.account.username}]: ${message}`, { channel: '#administrators' });
}

ChatCommands.adminChat = new shared.Command('/', {
  validator,
  parameters: [
    ['message', 'text'],
  ],
  permissions: ['CAN_USE_ADMIN_CHAT'],
}, adminChat);

ChatCommands.register = adminCommands => {
  adminCommands.add(new shared.Command('chat', {
    validator,
    parameters: [
      ['message', 'text'],
    ],
    permissions: ['CAN_USE_ADMIN_CHAT'],
  }, adminChat));

  adminCommands.add(new shared.Command('say', {
    validator,
    parameters: [
      ['message', 'text'],
    ],
    permissions: ['CAN_SEND_GLOBAL_ADMIN_MESSAGE'],
  }, (player, message) => {
    // Display as anonymous message for normal players. Everyone who is not a player should see the admin's name.
    vrp.player.manager.players.forEach(tempPlayer => {
      if (player.accessLevel === vrp.admin.levels.Player) {
        tempPlayer.sendMessage(`ADMIN: ${message}`, vrp.config.game.color.message.adminSayCommand);
      } else {
        tempPlayer.sendMessage(`${player.character.name}: ${message}`, vrp.config.game.color.message.adminSayCommand);
      }
    });
  }));
};
