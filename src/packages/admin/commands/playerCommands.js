/**
 * @title playerCommands.js
 * @overview Implements Player related Admin Commands
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

const defaultValidator = player => {
  return player.account !== null && player.character !== null;
};

export default adminCommands => {
  adminCommands.add(new shared.Command('ban', {
    parameters: [
      ['Target Player', 'target'],
      ['Duration', 'string'],
      ['Reason', 'text'],
    ],
    validator: defaultValidator,
    permissions: ['CAN_BAN'],
  }, (player, target, duration, reason) => {
    if (target.account === null) {
      return player.sendError('Invalid Target (no account?)');
    }
    if (isNaN(parseInt(duration.slice(0, duration.length - 1), 10))) {
      return player.sendError('Invalid Format for the duration. (e.g. "50m", "1h", "1d")');
    }

    // Duration can have these formats:
    // [n]m = n minutes (e.g. /ban somePlayer 30m ...)
    // [n]h = n hours
    // [n]d = n days
    target.account.ban(player, duration, reason, ban => {
      if (typeof ban === 'undefined') {
        return player.sendError('Could not ban player.');
      }
      target.client.Kick(`You have been banned from the server until ${ban.expiration.toString()} by ${player.character.name}: ${reason}`);
    });
  }));


  /**
   * Teleports the source player to its destination
   *
   * @param {Player} destination teleportation destination (lol)
   * @param {Player} target player to be teleported
   */
  function teleport(destination, target) {
    target.position = destination;
  }

  adminCommands.add(new shared.Command('goto', {
    parameters: [
      ['Target Player', 'target'],
    ],
    validator: defaultValidator,
    permissions: ['CAN_TELEPORT'],
  }, (player, target) => {
    if (target.character === null) {
      return player.sendError('Bad target.');
    }

    teleport(target, player);
    player.sendSuccess(`Teleported to ${target.character.name}`);
    target.sendInfo(`${player.character.name} teleported to you.`);
  }));

  adminCommands.add(new shared.Command('gethere', {
    parameters: [
      ['Target Player', 'target'],
    ],
    validator: defaultValidator,
    permissions: ['CAN_TELEPORT'],
  }, (player, target) => {
    if (target.character === null) {
      return player.sendError('Bad target.');
    }

    teleport(player, target);
    player.sendSuccess(`Teleported ${target.character.name} to you`);
    target.sendInfo(`${player.character.name} teleported you to his position.`);
  }));

  adminCommands.add(new shared.Command('mute', {
    parameters: [
      ['Target Player', 'player'],
      ['Duration (minutes)', 'integer'],
      ['Reason', 'text'],
    ],
    validator: defaultValidator,
    permissions: ['CAN_MUTE_PLAYER'],
  }, (player, target, duration, reason) => {
    if (!target.character) {
      return player.sendError('Invalid target.');
    }

    target.mute(duration * 60);
    target.sendInfo(`You were muted by ${player.character.name} for ${duration} minutes: ${reason}`);
    player.sendSuccess(`You muted ${target.character.name} for ${duration} minutes.`);
  }));

  adminCommands.add(new shared.Command(['kick', 'k'], {
    parameters: [
      ['Target Player', 'target'],
      ['Reason', 'text'],
    ],
    validator: defaultValidator,
    permissions: ['CAN_KICK'],
  }, (player, target, reason) => {
    shared.integration.slack.sendInfo(`${player.character.name} kicked ${target.character.name}: ${reason}`);
    target.client.Kick(`${reason} (${player.character.name})`);
  }));

  adminCommands.add(new shared.Command('players', {
    parameters: [],
    validator: defaultValidator,
    permissions: ['CAN_LIST_PLAYERS'],
  }, player => {
    let msg = '<table><tr><td><strong>Name</strong></td><td><strong>Character</strong></td><td><strong>Network ID</strong></td></tr>';
    vrp.player.manager.players.forEach(tempPlayer => {
      msg += `<tr><td>${tempPlayer.name}</td><td>${(tempPlayer.character !== null ? tempPlayer.character.name : 'no char selected')}</td><td>${tempPlayer.client.networkId}</td></tr>`;
    });
    msg += '</table>';
    player.sendRaw(msg);
  }));

  adminCommands.add(new shared.Command(['teleport', 'tele'], {
    parameters: [
      ['Destination Player', 'target'],
      ['Target Player', 'target'],
    ],
    validator: defaultValidator,
    permissions: ['CAN_TELEPORT'],
  }, (player, target, target2) => {
    if (target.character === null || target2.character === null) {
      return player.sendError('Bad target(s).');
    }

    teleport(target, target2);
    player.sendSuccess(`Teleported ${target2.character.name} to ${target.character.name}.`);
    target.sendInfo(`${player.character.name} teleported ${target2.character.name} to you.`);
    target2.sendInfo(`${player.character.name} teleported you to ${target2.character.name}.`);
  }));
};
