/**
 * @title debugCommands.js
 * @overview Implements Debug Commands (for Devs)
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

const validator = player => {
  return player.accessLevel !== null && player.accessLevel === vrp.admin.levels.Developer;
};

export default adminCommands => {
  adminCommands.add(new shared.Command('perm', {
    validator,
    parameters: [
      ['permission', 'string'],
      ['value', 'integer', undefined],
    ],
  }, (player, permission, value) => {
    if (typeof value === 'undefined') {
      player.debug(`${permission} => ${player.permissions.has(permission)}`);
      return;
    }

    if (value) {
      if (player.permissions.add(permission)) {
        player.debug(`Added permission '${permission}'`);
      }
    } else {
      if (player.permissions.remove(permission)) {
        player.debug(`Removed permission '${permission}'`);
      }
    }
  }));

  adminCommands.add(new shared.Command('getDriver', {
    validator,
    parameters: [],
  }, player => {
    for (let i = 0; i < g_vehicles.length; i++) {
      const driver = g_vehicles[i].driver;
      if (typeof _driver === 'undefined') {
        player.debug(`Vehicle ${i}: no driver`);
        continue;
      }

      if (!driver.character) {
        log.warn(`Unknown driver for vehicle ${i}: ${driver.name}`);
        continue;
      }

      player.debug(`Vehicle ${i}: ${driver.character.name}`);
    }
  }));

  adminCommands.add(new shared.Command('showGlobalStorage', {
    validator,
    parameters: [],
  }, player => {
    const storage = vrp.item.globalStorage;
    player.sendInfo(`<strong>${storage.name}. Weight: ${storage.weight}. Volume: ${storage.contentVolume}/${storage.capacity}</strong><hr />`);
    storage.content.forEach(stack => {
      if (stack.dropPosition === null) {
        player.sendInfo(`(${stack.weight}g) ${stack.quantity}x ${stack.name} at [#FF0000]<em>null</em>`);
      } else {
        player.sendInfo(`(${stack.weight}g) ${stack.quantity}x ${stack.name} at [#FFFF00]<em>x: ${stack.dropPosition.x}, y: ${stack.dropPosition.y}, z: ${stack.dropPosition.z}</em>`);
      }
    });
  }));
};
