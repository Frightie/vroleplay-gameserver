/**
 * @title accessLevel.js
 * @overview Implements Commands
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

/**
 * AccessLevel represents a certain set of permissions granted
 * to an Entity. This might be needed for example to execute certain commands.
 */
export default class AccessLevel {
  /**
   * Creates a new Access Level
   *
   * @param {string} name the name for the level
   * @param {integer} levelNumber number representation of the level (used for e.g. database actions)
   * @param {Array<string>} [permissions = []] the level's permissions
   */
  constructor(name, levelNumber, permissions = []) {
    /** @type {string} */
    this.name = name;

    /** @type {int} */
    this.levelNumber = levelNumber;

    /** @type {PermissionContainer} */
    this.permissions = vrp.permission.createContainer(true);

    for (let i = 0; i < permissions.length; i++) {
      if (!this.permissions.add(permissions[i])) {
        log.warn(`Could not add unknown Permission '${permissions[i]}' to AccessLevel '${this.name}'`);
      }
    }
  }
}
