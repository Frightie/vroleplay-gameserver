/**
 * @title main.js
 * @overview Main Package File
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

// Predefined V:RP Header to install our own error types & handler
import error from '../../shared/error';
import errorHandler from '../../shared/errorHandler';
error.install(global);
process.on('uncaughtException', errorHandler);

import Package from '../../shared/classes/package';

/** @typedef {string} EventName */

/**
 * The EventSystem provides functions similar to the GTA:MP Events but contains only
 * patched players. This makes it easy to use the events in other packages.
 */
class EventSystem extends Package {
  /**
   * Constructs the Package
   * @constructor
   */
  constructor() {
    super('events');

    /** @type {Map<EventName, Set<EventSystem~EventHandler>>} */
    this.handlers = new Map();
  }

  /**
   * Starts the Package
   */
  start() {
    super.start();
  }

  /**
   * Invokes an event with the given name and arguments.
   *
   * @param {string} name - The name of the event.
   * @param {...*} args - Arguments.
   * @returns {Array<*>} combined output of all handlers
   */
  invoke(name, ...args) {
    const r = [];
    if (!this.handlers.has(name)) {
      return r;
    }

    this.handlers.get(name).forEach(handler => {
      r.push(handler(...args));
    });
    return r;
  }

  receiveDependencies(deps) { super.receiveDependencies(global, deps); }
  receiveVRPNamespace(namespace) { super.receiveVRPNamespace(global, namespace); }


  /**
   * Event Handler for any type of events.
   *
   * @callback {EventSystem~EventHandler}
   * @param {...*} args - Any arguments.
   * @returns {*} anything you want to return.
   */
}

// Register globals
global.package = new EventSystem();
