/**
 * @title commands.js
 * @overview Implements Package Commands
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

const Commands = {};
export default Commands;

/* In Character (IC) */

Commands.do = new shared.Command('do', {
  parameters: [
    ['Message', 'text'],
  ],
}, (player, target, message) => {
  vrp.player.manager.sendRangedMessage(`<strong>* ${message} ((${player.name}))</strong>`, player.position, 18.0, {
    color: vrp.config.game.color.message.doCommand,
  });
});

Commands.local = new shared.Command(['l', 'local'], {
  parameters: [
    ['Message', 'text'],
  ],
}, (player, message) => {
  vrp.player.manager.sendRangedMessage(`${player.name} says: ${message}`, player.position, 10.0, {
    dropStart: 2.0,
  });
});

Commands.low = new shared.Command('low', {
  parameters: [
    ['Message', 'text'],
  ],
}, (player, message) => {
  vrp.player.manager.sendRangedMessage(`${player.name} quietly: ${message}`, player.position, 5.0, {
    dropStart: 2.0,
  });
});

Commands.me = new shared.Command('me', {
  parameters: [
    ['Message', 'text'],
  ],
}, (player, message) => {
  vrp.player.manager.sendRangedMessage(`<strong>* ${player.name} ${message}.</strong>`, player.position, 18.0, {
    color: vrp.config.game.color.message.meCommand,
  });
});

Commands.shout = new shared.Command(['s', 'shout'], {
  parameters: [
    ['Message', 'text'],
  ],
}, (player, message) => {
  vrp.player.manager.sendRangedMessage(`${player.name} shouts: ${message}`, player.position, 25.0, {
    dropStart: 10.0,
  });
});

Commands.radio = new shared.CommandGroup(['r', 'radio'], {})
  .add(new shared.Command('join', {
    parameters: [
      ['frequency', 'float'],
      ['key', 'text', ''],
    ],
    validator: player => player.playing && player.storages.has('Radio'),
  }, (player, frequency, key) => {
    let finalFrequency = frequency;
    let str = finalFrequency.toString();
    if (str.indexOf('.') === -1) {
      str += '.0';
    }
    if (str.length - str.indexOf('.') > 2) {
      str = str.slice(0, str.indexOf('.') + 2);
      finalFrequency = parseFloat(str);
    }

    if (frequency < 0.0 || frequency > 999.9) {
      return player.sendError(`Invalid frequency ${str}MHz.`);
    }

    if (frequency === player.radioChannel.frequency) {
      return player.sendError(`You already set the frequency of your radio to this frequency.`);
    }

    player.radioChannel = {
      finalFrequency,
      key,
    };
    player.sendSuccess(`Set radio frequency to ${str}MHz.`);
  }))
  .add(new shared.Command('key', {
    parameters: [
      ['key', 'text'],
    ],
    validator: player => player.playing && player.storages.has('Radio'),
  }, (player, key) => {
    if (key === player.radioChannel.key) {
      return player.sendError(`Key is already set to '${key}.'`);
    }
    player.radioChannel.key = key;
    player.sendSuccess(`Set channel key to '${key}'.`);
  }))
  .add(new shared.Command('say', {
    parameters: [
      ['message', 'text'],
    ],
    validator: player => player.playing && player.storages.has('Radio'),
  }, (player, message) => {
    if (player.radioChannel.frequency === null) {
      return player.sendError(`No frequency has been set.`);
    }

    const encryptedMessage = `${player.character.name}: ${message}`.split('');

    for (let i = 0; i < encryptedMessage.length; i++) {
      let random = Math.floor(Math.random() * (122 - 65)) + 65;
      while (random < 96 && random > 90) {
        random = Math.floor(Math.random() * (122 - 65)) + 65;
      }
      if (random % 4 === 0) {
        encryptedMessage[i] = ' ';
      } else {
        encryptedMessage[i] = String.fromCharCode(random);
      }
    }

    vrp.player.manager.players.forEach(receiver => {
      if (!receiver.playing || !receiver.storages.has('Radio')) {
        return;
      }

      if (player.radioChannel.frequency === receiver.radioChannel.frequency) {
        if (player.radioChannel.key === receiver.radioChannel.key) {
          receiver.sendMessage(`[RADIO] ${player.character.name}: ${message}`, vrp.config.game.color.message.radio);
        } else {
          receiver.sendMessage(`[RADIO] ${encryptedMessage.join('')}`, vrp.config.game.color.message.radio);
        }
      }
    });
  }));

/* Out of Character (OOC) */

Commands.globalOOC = new shared.Command(['o', 'ooc'], {
  parameters: [
    ['OOC Message', 'text'],
  ],
  validator: player => vrp.config.game.enableOOC || player.accessLevel !== vrp.admin.levels.Player,
  permissions: ['CAN_WRITE_IN_OOC'],
}, (player, message) => {
  vrp.player.manager.players.forEach(tempPlayer => {
    if (tempPlayer.permissions.has('CAN_SEE_OOC_CHAT')) {
      tempPlayer.sendMessage(`(( [Global OOC] ${player.name} (ID: ${player.networkId}): ${message} ))`);
    }
  });
});

Commands.localOOC = new shared.Command('b', {
  parameters: [
    ['OOC Message', 'text'],
  ],
  permissions: ['CAN_WRITE_IN_OOC'],
}, (player, message) => {
  vrp.player.manager.sendRangedMessage(`(( [OOC] ${player.name} (ID: ${player.networkId}): ${message} ))`, player.position, 10.0, {
    dropStart: 2.0,
  });
});

Commands.pm = new shared.Command('pm', {
  parameters: [
    ['Target Player', 'target'],
    ['OOC Message', 'text'],
  ],
  permissions: ['CAN_SEND_PMs'],
}, (player, target, message) => {
  if (!target.permissions.has('CAN_SEE_PMs')) {
    player.sendError(`${target.character.name} does not receive PMs at the moment.`);
    return;
  }
  if (target.blocked.has('pm') && target.blocked.get('pm').has(player.character.id)) {
    player.sendError(`${target.character.name} does not want to receive PMs from you right now.`);
    return;
  }

  target.sendMessage(`<strong>((</strong> PM from <strong>${player.character.name}</strong> [${player.networkId}]: ${message}<strong>))</strong>`, vrp.config.game.color.message.pmCommand);
  player.sendMessage(`<strong>((</strong> PM to <strong>${target.character.name}</strong> [${target.networkId}]: ${message}<strong>))</strong>`, vrp.config.game.color.message.pmCommandSender);
});
