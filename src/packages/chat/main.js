/**
 * @title main.js
 * @overview Main Package File
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

// Predefined V:RP Header to install our own error types & handler
import error from '../../shared/error';
import errorHandler from '../../shared/errorHandler';
error.install(global);
process.on('uncaughtException', errorHandler);

import Package from '../../shared/classes/package';

class Chat extends Package {
  /**
   * Constructs the Package
   * @constructor
   */
  constructor() {
    super('chat');
  }

  /**
   * Starts the Package
   */
  start() {
    super.start();
  }

  /**
   * Registers all Permissions. Static Permissions are returned as string array,
   * dynamic permissions can be created using vrp.permission.create.
   */
  registerPermissions() {
    vrp.permission.create('CAN_CHAT', {
      evaluator: player => player.character !== null,
    });
    return ['CAN_SEE_OOC_CHAT', 'CAN_WRITE_IN_OOC', 'CAN_SEE_PMs', 'CAN_SEND_PMs'];
  }

  // Do not remove this. Otherwise Jan will slowly sneak up to you and rip your anus apart.
  receiveDependencies(deps) { super.receiveDependencies(global, deps); }
  receiveVRPNamespace(namespace) { super.receiveVRPNamespace(global, namespace); }
}

// Register globals
global.package = new Chat();
global[global.package.name] = global.package;
