/**
 * @title events.js
 * @overview Package Events File
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */

export default class Events {
  /**
   * Registers all Events
   *
   */
  static register() {
    events.Add('ChatMessage', Events.onChatMessage);
  }

  /**
   * Called when a player sends a text message
   *
   * @param {Player} player the new player.
   * @param {string} message the message.
   * @returns {boolean} whether the message will appear in the chat or not.
   */
  static onChatMessage(player, message) {
    console.log(player);
    log.debug(`player.events.onChatMessage(name: "${player.name}", message: "${message}")`, 4);

    if (!player.permissions.has('CAN_CHAT')) {
      player.sendError('You are not allowed to send chat messages.');
      return true;
    }

    if (message.charAt(0) === '#' && player.permissions.has('CAN_USE_ADMIN_CHAT')) {
      vrp.command.manager.handleCommand(player, `a chat ${message.substr(1)}`);
      return true;
    }

    if (player.playing) {
      vrp.command.manager.handleCommand(player, `local ${message}`);
    }

    return true;
  }
}
