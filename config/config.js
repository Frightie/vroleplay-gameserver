/**
 * @title config.js
 * @overview Config File
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */
"use strict";

/**
 * This Config file is loaded at startup. Changes applied while the server
 * is running are not registered by the server.
 */

module.exports = {
	game: require('./config.game'),
	environment: require('./config.environment'),
  ui: require('./config.ui'),
};
