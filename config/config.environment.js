/**
 * @title config.environment.js
 * @overview Environment Configuration
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */
"use strict";

let environment = module.exports;

// Add yourself here, if you dont want Slack to be spammed. (2015/01/07 NEVER 5GET)
environment.localHosts = [
  "User-PC",          // Ariel
  "WIN-F1UCSHMDPD0",  // Jan
  "jan.archlinux",    // Jan on linux
  "jan-elementary",   // Jan on Mac
  "WIN-Q3V979MPFNP",  // Daniel
];

// Specify the Log Level. This will make the output the more verbose the higher the value is.
// Range 0 - 5. silent >>>> verbose

// Level 0: no debug output
// Level 1: +General debug messages
// Level 2: +Database Queries, Initialization messages
// Level 3: +Verbose Initialization messages (registering commands, etc)
// Level 4: +Breakpoints, "I am here"
// Level 5: +Loop debug messages
environment.logLevel = 5;
