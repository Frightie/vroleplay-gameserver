/**
 * @title game.js
 * @overview Game Configuration
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */
"use strict";

const enums = require('../shared/enums.js');

let game = module.exports;

// Default login restriction after the server started
game.loginRestriction = 'None';

// Enable OOC Chat by default
game.enableOOC = true;

// Save Interval for Players in Milliseconds
game.saveInterval = 300000;

// Color codes
game.color = {
    red: {
        light: new RGB(231, 76, 60),
        dark: new RGB(192, 57, 43),
    },
    green: {
        light: new RGB(46, 204, 113),
        dark: new RGB(39, 174, 96),
    },
    blue: {
        light: new RGB(52, 73, 94),
        dark: new RGB(44, 62, 80),
    },
    cyan: {
        light: new RGB(52, 152, 219),
        dark: new RGB(41, 128, 185),
    },
    purple: {
        light: new RGB(155, 89, 182),
        dark: new RGB(142, 68, 173),
    },
    white: {
        light: new RGB(236, 240, 241),
        dark: new RGB(189, 195, 199),
    },
};

// Colors for Messages (Feature-based colors)
game.color.message = {
    debug: game.color.purple.dark,
    info: game.color.white.dark,
    success: game.color.green.light,
    error: game.color.red.dark,
    unavailableCommand: new RGB(241, 196, 15),

    adminSayCommand: new RGB(255, 166, 0),
    adminChatCommand: new RGB(255, 166, 0),

    meCommand: new RGB(194, 162, 218),
    doCommand: new RGB(194, 162, 218),
    pmCommand: new RGB(241, 196, 15),
    pmCommandSender: new RGB(241, 196, 15),
    factionSayCommand: new RGB(100, 95, 184),
    radio: new RGB(255, 229, 150),

    vitalStat: new RGB(241, 196, 15),
    vitalStatCritical: game.color.red.light,
};

game.position = {
  loginScreen: [
    { // View on the long bridge at the river near the military base
      position: new Vector3f(-2122.85, 2755.77, 26.13),
      lookAt: new Vector3f(-2699.8, 2596.91, 52.1),
    },
    { // View on the wind park
      position: new Vector3f(1452.99, 2024.95, 129.75),
      lookAt: new Vector3f(1548.32, 2025.84, 105.37),
    },
    { // View on a bridge + river
      position: new Vector3f(-1584.66, 4444.74, 9.68),
      lookAt: new Vector3f(-1795.93, 4480.1, 14.56),
    },
    { // View on the City (~Vinewood Shield)
      position: new Vector3f(692.0, 1229.0, 363.0),
      lookAt: new Vector3f(-5.0, -571.0, 253.0),
    }
  ]
}

// Default Weahter, IPLs, interiors
game.world = {
  interiors: [],
  IPLs: [],
  timeScale: 1.0,
  weather: 1
};
