/**
 * @title config.ui.js
 * @overview UI Configuration
 * @author Ariel Weinberger
 * @copyright (c) 2015 V:Role Play Team [v-rp.com]
 */
"use strict";

// This File specifies all the UIs and events that are allowed to be sent from UIs to the server.

module.exports = {
  login: {
    name: 'vrp-login',
    receives: ['loginResult'],
    sends: ['submit'],
  },
  characterSelection: {
    name: 'vrp-character-selection',
    receives: ['availableCharacters', 'selectResult'],
    sends: ['submit'],
  },
};
