# V:Role Play
[V:Role Play](http://v-rp.com) is a strict roleplay gamemode for the upcoming [GTA:Multiplayer modification](http://gta-mp.net).

## Environment Setup
### Javascript
- Make sure to check out `development` on git.
- `npm install`
- Build the src dir once by typing `babel src --out-dir ./ --optional runtime` in the root dir of the project.
- Copy all package.json files from the src packages over to the `packages` and `shared` folders.
- Go into each folder and type `npm install`
- If you are using VSCode, the build task can be found below

### Database
- Start your MySQL Server
- Execute the `vrp.sql` into a database named `vrp`

### Add your hostname to the Config
- In order to not get spammed on Slack, add your hostname to `config/config.environment.js` under `environment.localHosts.
- You can find out your hostname by opening the Command Line and typing `hostname`.

## Authors
- Jan Christophersen
- Ariel Weinberger

## tasks.json (VSCode)
```javascript
{
	"version": "0.1.0",
	"command": "${workspaceRoot}/node_modules/.bin/babel",
	"isShellCommand": true,
	"tasks": [
		{
			"args": ["--experimental", "--stage", "1", "src", "--out-dir", "${workspaceRoot}", "-w", "--optional", "runtime", "--source-maps"],
			"taskName": "build",
			"suppressTaskName": true,
			"isBuildCommand": true, // make this the F1 > Task: Run Build Task gesture
			"isWatching": true // tell VS Code not wait for this task to finish
		}
	]
}
```