/**
 * @title main.js
 * @overview UI Controller
 * @author Ariel Weinberger
 */

var uis = {};
var mainUiCommands = ['toggle_chat_visibility'];

var loginui;

var regUIEvent = /^ui-[a-z\-]{1,}$/i;

/**
 * Handles CEF Commands (commands from any UI to the Clientscript)
 */
events.Add('CEFCommand', function(command, data) {
  if (regUIEvent.test(command)) {
    debug('CEFCommand ' + command + ': ' + data);
  }

  for (var i = 0; i < mainUiCommands.length; i++) {
    // Directly relay Commands from an UI to the MainUI (e.g. toggle chat visibility)
    if (mainUiCommands[i] === command) {
      invokeCEF(command, data);
      return true;
    }
  }

  if (regUIEvent.test(command)) {
    // Command from a ui-* UI.
    var ui = uis[command];
    if (typeof ui === 'undefined') {
      debug('invalid UI ' + command);
      return true;
    }

    if (ui.destroyed) {
      return;
    }

    var info = JSON.parse(data);

    // Check if the event is something the server wants to know (e.g. ui-login submit)
    for (var i = 0; i < ui.sends.length; i++) {
      if (ui.sends[i] === info.event) {
        invokeServerEvent(command, data);
        break;
      }
    }

    if (info.event === 'finishedSetup') {
      invokeServerEvent('ui_created', command);
      return true;
    }
    debug('invalid UI->serverEvent ' + info.event);
  } else if (command === 'mainui-control') {
    // Internal command from a ui-* UI to the Clientscript to show/hide itself etc.
    var info = JSON.parse(data);

    if (!uis.hasOwnProperty(info.name)) {
      debug('Invalid name in mainui-control: ' + data);
      return true;
    }

    uis[info.name][info.command].apply(uis[info.name], info.args);
  }
});

/**
 * Called from the GTA:MP Server to any clientscripts
 */
events.Add('ClientEvent', function(command, data) {
  if (command === 'create_ui') {
    // Create an UI.
    debug('create_ui invoked: ' + data);
    var info = JSON.parse(data);

    if (uis.hasOwnProperty(info.name)) {
      debug('Cannot recreate UI ' + data);
      return;
    }
    var ui = new UI(info.name, new Vector2(2560, 1440));
    uis[info.name] = ui;

    // sends are events that will be passed directly to the server when called from the UI (see CEFCommand handler)
    ui.sends = info.sends;

    ui.create(1).show();
  } else if (command === 'destroy_ui') {
    debug('destroy_ui invoked: ' + data);

    if (!uis.hasOwnProperty(data)) {
      debug('unknown ui ' + data);
      return;
    }

    uis[data].destroy();
    invokeServerEvent('ui_destroyed', info.name);
  }

  if (regUIEvent.test(command)) {
    // Pass ui-* events directly to the UI
    debug('UI EVENT ' + command + ': ' + data);
    invokeCEF(command, data);
  }

  return true;
});

/**
 * UI Class
 */
function UI(name, size, info) {
  this.name = name;
  this.path = 'file://' + GetWorkingDirectory() + '/' + name + '/index.html';
  this.size = size;
  this.sends = [];
  this.destroyed = false;
}

UI.prototype.create = function(zIndex) {
  var index = zIndex || 1;
  this.overlay = new UIOverlay(this.name, this.path, 0, 0, index);
  this.overlay.size = this.size;
  this.overlay.Create();
  return this;
};

UI.prototype.destroy = function() {
  if (!this.overlay) {
    return;
  }

  debug('TODO: ui.destroy');
  this.hide();
  this.destroyed = true;
  //this.overlay.Destroy();
}

UI.prototype.hide = function() {
  if (!this.overlay) {
    return;
  }

  this.overlay.Hide();
};

UI.prototype.invoke = function(event, info) {
  if (this.destroyed) {
    return;
  }

  invokeCEF(this.name, JSON.stringify({ event: event, data: info }));
}

UI.prototype.show = function() {
  if (!this.overlay) {
    return;
  }

  this.overlay.BringToFront();
  this.overlay.Show();
};

function debug(message) {
  invokeCEF('ui_debug', message);
}